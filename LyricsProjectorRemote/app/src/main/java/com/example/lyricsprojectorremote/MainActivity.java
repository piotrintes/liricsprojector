package com.example.lyricsprojectorremote;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class MainActivity extends Activity {

    private String IpAddr = "192.168.2.106";
    private Integer Port = 5400;
    private boolean settingsVisible = false;

    DatagramSocket udpSocket = new DatagramSocket();

    public MainActivity() throws SocketException, UnknownHostException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
        IpAddr = sharedPreferences.getString("SvrIP", "192.168.2.106").toString();
        Port = sharedPreferences.getInt("SvrPort", 5400);

        TextView ip = (TextView) findViewById(R.id.IPAddr);
        TextView port = (TextView) findViewById(R.id.PortNr);

        ip.setText(IpAddr);
        port.setText(Integer.toString(Port));

        hideSettings();
    }


    private void sent(String datagram){
        byte[] buf = (datagram).getBytes();

        InetAddress serverAddr = null;
        try {
            serverAddr = InetAddress.getByName(IpAddr);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        DatagramPacket packet = new DatagramPacket(buf, buf.length,serverAddr, Port);

        new Thread(new Runnable() {
            public void run() {
                try {
                    udpSocket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    public void updateIPPort(View view){
        TextView ip = (TextView) findViewById(R.id.IPAddr);
        TextView port = (TextView) findViewById(R.id.PortNr);
        IpAddr = ip.getText().toString();
        Port =  Integer.parseInt(port.getText().toString());

        SharedPreferences sharedPreferences = getSharedPreferences("UserInfo", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("SvrIP",IpAddr);
        editor.putInt("SvrPort",Port.intValue());
        editor.commit();

    }


    public void sentPrewSlide(View view){
        sent("-1");
    }

    public void sentNextSlide(View view){
        sent("1");
    }

    public void sentBlack(View view){
        sent("0");
    }

    public void togleSettings(View view){
        if(settingsVisible)
            hideSettings();
        else
            showSettings();
    }


    public void showSettings(){
        TextView ip = (TextView) findViewById(R.id.IPAddr);
        TextView port = (TextView) findViewById(R.id.PortNr);
        Button updateButton = (Button) findViewById(R.id.updateButton);

        ip.setVisibility(View.VISIBLE);
        port.setVisibility(View.VISIBLE);
        updateButton.setVisibility(View.VISIBLE);

        settingsVisible = true;
    }

    public void hideSettings(){
        TextView ip = (TextView) findViewById(R.id.IPAddr);
        TextView port = (TextView) findViewById(R.id.PortNr);
        Button updateButton = (Button) findViewById(R.id.updateButton);

        ip.setVisibility(View.GONE);
        port.setVisibility(View.GONE);
        updateButton.setVisibility(View.GONE);

        settingsVisible = false;
    }
}