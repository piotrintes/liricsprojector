const { homedir } = require('os');
const asyncHandler = require('express-async-handler')
const fs = require('fs');
const exec = require('child_process').exec;

// create pdf cache directory
const currentPath = process.cwd();
const cachePath = currentPath + "/front/pdf";
if (!fs.existsSync(cachePath)) fs.mkdirSync(cachePath);
// clean up pdf cache at startup
fs.readdir(cachePath, (err, files) => {
    for (const file of files) {
        fs.unlinkSync(cachePath + "/" + file);
    }
})


function resolvePath(path) {
    if (path.at(-1) == '/') path = path.slice(0, -1);

    // if path is not absolute, use user home as a base
    if (path.at(0) != '/') {
        if (path.startsWith('uploads/')) path = currentPath + '/' + path;
        else path = homedir + path;
    }
    return path;
}

exports.ls = asyncHandler(async (req, res, next) => {
    let files = [];
    let dir = req.query.dir;

    try {
        dir = resolvePath(dir);

        let filenames = await fs.readdir(dir);

        for (const filename of filenames) {
            if (filename.at(0) == '.') continue;
            let stats = await fs.stat(dir + '/' + filename);
            files.push(
                {
                    name: filename,
                    type: stats.isDirectory() ? "dir" : "file"
                }
            )
        }
    } catch (error) {
        console.log(error);
    }
    res.send(files);
})

exports.mkpdf = asyncHandler(async (req, res, next) => {
    let pdfFile = req.query.file;

    try {
        pdfFile = resolvePath(pdfFile);

        // create id for pdf (hex from timestamp)
        const id = Math.floor(Date.now()).toString(16);

        // run mutool to render pdf
        const command = "mutool draw -F png -r 180 -o \"" + cachePath + "/" + id + ".%d.png\" \"" + pdfFile + "\"";
        exec(command, (err, stdout, stderr) => {
            if (err) {
                console.log(err);
                res.status(500).send(err);
                return;
            }

            let pageCount = 0;

            // count number of pages from stderr (mutool output)
            for (const line of stderr.split('\n')) {
                console.log(line);
                if (line.startsWith('page ')) pageCount++;
            }

            // send response
            res.status(200).send(JSON.stringify({
                filename: pdfFile.split('/').at(-1),
                id: id,
                pages: pageCount,
                src: 'pdf/'
            }));
        });
    } catch (error) {
        res.status(500).send(error);
    }
})