
class Settings {
     constructor(cookieName) {
          this.cookieName = cookieName;
          this.settings = new Map();
          this.defaults = new Map();
     }

     registerSettings(setting, defaultValue, callback) {
          if (defaultValue != undefined) this.defaults.set(setting, defaultValue);
          if (callback != undefined) this.callbacks.set(setting, callback);
     }

     set(setting, value) {
          this.settings.set(setting, value);
          if (this.callbacks.has(setting)) this.callbacks.get(setting)();
     }

     get(setting) {
          return this.settings.has(setting) ? this.settings.get(setting) : this.defaults.get(setting);
     }

     getJson() {
          let json;
          this.settings.forEach((value, setting) => { json[setting] = value; })
          return json;
     }

     loadJson(json) {
          for (var key in json) { //data being the object
               this.settings.set(key, json[key]);
          }
     }


}

export default Settings;