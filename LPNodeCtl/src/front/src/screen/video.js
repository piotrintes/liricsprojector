import Screen from "./screen.js";

class Video extends Screen {
     constructor(screenArea, server, settings) {
          super(screenArea);

          this.server = server;
          this.settings = settings;
          this.playerList = new Map();
          this.currentPlayer;

          this.videoArea = document.createElement('div');
          this.videoArea.style.width = "inherit";
          this.videoArea.style.height = "inherit";
          this.screen.appendChild(this.videoArea);

          setInterval(() => {
               try {
                    this.server.get("videolist").then(json => {
                         json.forEach(async element => {
                              if (!this.playerList.has(element.id)) {
                                   let player = document.createElement('video');
                                   player.src = await this.proloadVideo(this.server.getVideoUrl(element.id));
                                   //player.controls = true;
                                   player.preload = 'auto'
                                   player.autoplay = false
                                   player.style.display = "none";
                                   this.playerList.set(element.id, player);
                                   this.videoArea.appendChild(player);
                              }
                         });
                    }).catch(() => { });
               } catch (e) { }
          }, 1000);
     }

     async proloadVideo(src) {
          const res = await fetch(src);
          const blob = await res.blob();
          return URL.createObjectURL(blob);
     }

     update(message) {
          message = message.split("#");

          console.log(message);
          if (message[0] == "videostop") {
               console.log('stopping video')
               this.currentPlayer.pause();
          } else {
               if (this.currentPlayer) this.currentPlayer.style.display = "none";
               let player = this.playerList.get(message[1]);
               if (message[2] != '0') {
                    player.currentTime = String(Number(message[2]) + 0.7);
                    setTimeout(() => {
                         player.play();
                    }, 10);
               } else {
                    player.play();
               }

               player.style.display = "block";
               this.currentPlayer = player;
          }
     }

     hide() {
          Screen.prototype.hide.call(this);
          if (this.currentPlayer) {
               let player = this.currentPlayer;
               player.pause();
               setTimeout(() => {
                    if (!this.isVisible) player.currentTime = 0;
               }, 1000);
          }
     }
}

export default Video;
