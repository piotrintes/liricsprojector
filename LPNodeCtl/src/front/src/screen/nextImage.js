import Screen from "./screen.js";

/** This screen displays a miniature of the next image slide, send by LP */
class NextImage extends Screen {
    /**
     * @param screenArea container in which this screen should be displayed
     * @param server Endpoint for Lyrics Projector web server
     * @param settings App settings
     */
    constructor(screenArea, server, settings) {
        super(screenArea);
        this.server = server;
        this.settings = settings;

        this.image = document.createElement('img');
        this.image.className = 'image-content';
        this.screen.appendChild(this.image);
    }

    /** Update the content of this screen */
    update() {
        this.image.src = this.server.getUrl() + '/img/next&' + Date.now();
    }
}

export default NextImage;