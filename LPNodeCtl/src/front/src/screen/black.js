import Screen from "./screen.js";

class Black extends Screen {
     constructor(screenArea) {
          super(screenArea);

          this.screen.style.background = "#000";
     }
}

export default Black;