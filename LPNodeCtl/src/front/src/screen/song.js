import Screen from "./screen.js";

/** This screen displays song text currently displayed by LP */
class Song extends Screen {
     /**
      * @param screenArea container in which this screen should be displayed
      * @param server Endpoint for Lyrics Projector web server
      * @param settings App settings
      */
     constructor(screenArea, server, settings) {
          super(screenArea);
          this.server = server;
          this.settings = settings;
          this.currentText;

          this.screen.classList.add("song-container");

          this.song = document.createElement('song');
          this.screen.appendChild(this.song);
     }

     /** Set text content to provided text
      * @param text text to be displayed on screen
     */
     setText(text) {
          this.currentText = text;
          this.song.innerHTML = text;
     }

     /** Update the content of this screen */
     update() {
          this.server.get('song/current').then(json => {
               if (json.display == 'true' && this.currentText != json.text) this.setText(json.text);
          });
     }
}

export default Song;