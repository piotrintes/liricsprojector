import Screen from "./screen.js";
import logoSVG from "../ui/logo.js"
import Status from "../ui/status.js"

class WelcomeScreen extends Screen {
     constructor(screenArea, server) {
          super(screenArea);

          this.screen.style.background = "#111";
          this.screen.style.display = "flex";
          this.screen.style.alignItems = "center";
          this.screen.style.justifyContent = "center";
          this.screen.style.containerType = "inline-size";

          let logo = document.createElement("div");
          logo.innerHTML = logoSVG;
          logo = logo.children[0];
          this.screen.appendChild(logo);

          logo.style.width = "40cqw"
          logo.style.setProperty("--circle-fill", "100")
          logo.style.setProperty("--opacity", "0")
          logo.style.setProperty("--scale2", "0.5")

          setTimeout(() => {
               logo.style.setProperty("--scale", "1")
               logo.style.setProperty("--scale2", "1")
          }, 100)
          setTimeout(() => {
               logo.style.setProperty("--opacity", "1")
          }, 300)
          setTimeout(() => {
               logo.style.setProperty("--circle-fill", "18")
          }, 1200)

          setTimeout(() => {
               this.status = new Status(server);
               this.screen.appendChild(this.status.getHTML());
          }, 3000)

          this.show();
     }

     update() {
          this.status.update();
     }
}


export default WelcomeScreen;