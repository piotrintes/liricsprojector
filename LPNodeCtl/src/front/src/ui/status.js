export class Status {
     constructor(server) {
          this.server = server;

          this.html = document.createElement('div');
          this.html.className = "status-display-floating";

          this.ipList = document.createElement('div');
          this.ipList.className = "list-container";
          this.html.appendChild(this.ipList);

          let sep = document.createElement('span');
          sep.className = "separatorV";
          this.html.appendChild(sep);

          this.cnList = document.createElement('div');
          this.cnList.className = "list-container";
          this.html.appendChild(this.cnList);

          this.update();

          setInterval(() => this.update(), 5000);
     }

     update() {
          { // IP address list
               fetch('ip-list').then(res => res.json()).then(json => {
                    this.ipList.innerHTML = '';

                    let header = document.createElement('span');
                    header.className = "list-header";
                    header.appendChild(document.createTextNode('IP address list:'));
                    this.ipList.appendChild(header);

                    json.forEach(address => {
                         let addressLabel = document.createElement('span');
                         addressLabel.appendChild(document.createTextNode(address));
                         this.ipList.appendChild(addressLabel);
                    });
               });
          }

          { // Connection status
               let connected = this.server.getConnected();

               this.cnList.innerHTML = ''

               let header = document.createElement('span');
               header.className = "list-header";
               header.appendChild(document.createTextNode('Connection status:'));
               this.cnList.appendChild(header);

               let statusLabel = document.createElement('span');
               statusLabel.style.fontWeight = 'bold';
               statusLabel.style.color = connected ? "var(--color-green)" : "var(--color-red)";
               statusLabel.appendChild(document.createTextNode(connected ? "CONNECTED" : "DISCONNECTED"));
               this.cnList.appendChild(statusLabel);

               if (connected) {
                    let addressLabel = document.createElement('span');
                    addressLabel.appendChild(document.createTextNode("Server IP: " + this.server.address));
                    this.cnList.appendChild(addressLabel);
               }
          }
     }

     getHTML() {
          return this.html;
     }
}

export default Status;