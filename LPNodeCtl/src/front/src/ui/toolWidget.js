
class ToolWidget {
    constructor(appMain) {
        this.appMain = appMain;
        this.widgets = [];
        this.currentWidget = 0;

        this.container = document.createElement('div');
        this.container.className = "tool-widget";
        this.appMain.appendChild(this.container);

        this.content = document.createElement('div');
        this.content.className = "tool-widget-content";
        this.container.appendChild(this.content);

        this.switchButton = document.createElement('button');
        this.switchButton.className = "tool-widget-switch";
        this.switchButton.onclick = () => {
            this.currentWidget = (this.currentWidget + 1) % this.widgets.length;
            this.content.innerHTML = '';
            this.content.appendChild(this.widgets[this.currentWidget]);
        }
        this.container.appendChild(this.switchButton);

        this.addClock();
        this.addBattery();

        this.content.appendChild(this.widgets[0]);
    }

    addClock() {
        let time = document.createElement('div');
        time.className = "tool-widget-clock";
        this.widgets.push(time);

        setInterval(() => {
            let date = new Date();
            let s = date.toLocaleTimeString();
            time.innerHTML = `${s.split(':')[0]}:${s.split(':')[1]}`;
        }, 1000);
    }

    addBattery() {
        let widget = document.createElement('div');
        widget.className = "tool-widget-battery-container";
        widget.style.display = "flex";
        widget.style.gap = "8px";
        this.widgets.push(widget);

        let battery = document.createElement('div');
        battery.className = "tool-widget-battery";
        widget.appendChild(battery);

        let fill = document.createElement('span');
        fill.className = "tool-widget-battery-fill";
        battery.appendChild(fill);

        let textArea = document.createElement('div');
        textArea.className = "tool-widget-battery-text";
        widget.appendChild(textArea);

        let percent = document.createElement('span');
        percent.className = "tool-widget-battery-time";
        textArea.appendChild(percent);

        let time = document.createElement('span');
        time.className = "tool-widget-battery-time";
        textArea.appendChild(time);

        setInterval(() => {
            navigator.getBattery().then(battery => {
                fill.style.width = `${battery.level * 100}%`;
                fill.classList.toggle('low', battery.level < 0.15);
                fill.classList.toggle('charge', battery.charging);
                percent.innerHTML = `${Math.floor(battery.level * 100)}%`;

                function sec2str(seconds) {
                    if (seconds == Infinity) return ''
                    let h = Math.floor(seconds / 3600);
                    let m = Math.floor(seconds / 60);
                    if (h > 0) return `${h}h`
                    if (m > 0) return `${m}m`
                    else return `< 1m`
                }

                time.innerHTML = battery.charging ? sec2str(battery.chargingTime) : sec2str(battery.dischargingTime);
            });
        }, 1000);
    }
}


export default ToolWidget