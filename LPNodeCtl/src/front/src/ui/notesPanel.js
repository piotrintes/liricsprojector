import pdfLoader from './pdfLoader.js'

export class NotesPanel {
    constructor(screenArea, prewiewArea, server) {
        this.screenArea = screenArea;
        this.prewiewArea = prewiewArea;
        this.server = server;
        this.mode = 'list';
        this.pdfList = [];

        // Create the panel
        this.html = document.createElement('div');
        this.html.className = "notes-panel dock-left vp0";

        // Create the content
        this.content = document.createElement('div');
        this.content.className = "notes-panel-content";
        this.html.appendChild(this.content);

        // set up contents scroll control - to keep the scroll % when resizing the panel
        let scrollPosition = 0;
        let lockPosition = false;
        this.content.onscroll = () => {
            if (lockPosition) return;
            scrollPosition = this.content.scrollTop / (this.content.scrollHeight)
        }
        new ResizeObserver(() => {
            if (!lockPosition) {
                lockPosition = true;
                setTimeout(() => {
                    lockPosition = false;
                }, 500)
            }
            this.content.scrollTop = scrollPosition * (this.content.scrollHeight)
        }).observe(this.content);

        // create open/close button
        this.closeButton = document.createElement('button');
        this.closeButton.className = "notes-open-button";
        document.getElementById("app-main").appendChild(this.closeButton);
        this.closeButton.onclick = () => {
            const hidden = this.html.classList.contains("vp0");
            this.setPanelMode(hidden ? 'small' : 'hidden');
        }

        // back to list button
        this.backButton = document.createElement('button');
        this.backButton.className = "notes-back-button";
        document.getElementById("app-main").appendChild(this.backButton);
        this.backButton.onclick = () => {
            this.displayNoteList();
        }

        // open pdf button
        this.addButton = document.createElement('button');
        this.addButton.className = "notes-add-button";
        document.getElementById("app-main").appendChild(this.addButton);
        this.addButton.onclick = () => {
            pdfLoader.openDialog(async json => {
                if (json.path)
                    await this.setupNotes(json.path)
                else if (json.id)
                    this.addPdf(json)
            }, this.server)
        }

        // dark mode switcher
        this.darkModeSwitcher = document.createElement('button');
        this.darkModeSwitcher.className = "notes-dark-button";
        document.getElementById("app-main").appendChild(this.darkModeSwitcher);
        this.darkModeSwitcher.onclick = () => {
            this.html.classList.toggle("dark-mode");
            this.darkMode = this.html.classList.contains("dark-mode");
        }

        // ratio switcher
        this.ratioSwitcher = document.createElement('button');
        this.ratioSwitcher.className = "notes-ratio-switcher vp0";
        document.getElementById("app-main").appendChild(this.ratioSwitcher);
        this.ratioSwitcher.onclick = () => {
            const small = this.html.classList.contains("vp40");
            this.setPanelMode(small ? 'large' : 'small');
        }

        this.setPanelMode('hidden');
        this.displayNoteList();
    }

    getHTML() {
        return this.html;
    }

    addPdf(pdfData) {
        this.pdfList.push(pdfData);
        if (this.mode == 'list') this.displayNoteList();
    }

    async setupNotes(filename) {
        return await fetch(`/mkpdf?file=${filename}`).then(res => res.json()).then(json => {
            this.addPdf(json)
            return json
        })
    }

    displayNotes(pdfData) {
        console.log(pdfData)
        this.content.innerHTML = ''

        for (let i = 0; i < pdfData.pages; i++) {
            let page = document.createElement('img');
            page.className = "notes-page";
            page.src = `${pdfData.src}/${pdfData.id}.${i + 1}.png`;
            page.onabort = () => {
                console.log("aborted")
            }
            this.content.appendChild(page);
        }

        this.mode = 'notes';
        this.backButton.style.display = 'block';
        this.darkModeSwitcher.style.display = 'block';
        this.addButton.style.display = 'none';
    }

    displayNoteList() {
        this.content.innerHTML = ''

        this.pdfList.forEach(pdfData => {
            let item = document.createElement('button');
            item.className = "option notes-item";
            item.innerHTML = pdfData.filename;
            item.onclick = () => {
                this.displayNotes(pdfData);
            }
            this.content.appendChild(item);
        })

        this.mode = 'list';
        this.backButton.style.display = 'none';
        this.darkModeSwitcher.style.display = 'none';
        this.addButton.style.display = 'block';
    }

    setPanelMode(mode) {
        if (mode == 'hidden') {
            this.html.className = "notes-panel dock-left vp0";
            this.screenArea.className = "screen-area dock-right vp0";
            this.ratioSwitcher.className = "notes-ratio-switcher vp0";
            this.prewiewArea.className = "next-screen vp0";
            this.closeButton.className = "notes-open-button";
            this.backButton.style.translate = '-100px';
            this.addButton.style.translate = '-100px';
            this.darkModeSwitcher.style.translate = '-100px';
        } else if (mode == 'small') {
            this.html.className = "notes-panel dock-left vp40" + (this.darkMode ? " dark-mode" : "");
            this.screenArea.className = "screen-area dock-right vp40";
            this.ratioSwitcher.className = "notes-ratio-switcher vp40";
            this.prewiewArea.className = "next-screen vp40";
            this.closeButton.className = "notes-close-button";
            this.backButton.style.translate = '0';
            this.addButton.style.translate = '0';
            this.darkModeSwitcher.style.translate = '0';
        } else if (mode == 'large') {
            this.html.className = "notes-panel dock-left vp75" + (this.darkMode ? " dark-mode" : "");
            this.screenArea.className = "screen-area dock-right vp75";
            this.ratioSwitcher.className = "notes-ratio-switcher vp75";
            this.prewiewArea.className = "next-screen vp75";
            this.closeButton.className = "notes-close-button";
            this.backButton.style.translate = '0';
            this.addButton.style.translate = '0';
            this.darkModeSwitcher.style.translate = '0';
        }
    }

}

export default NotesPanel;
