export class SlideControls {
    constructor(server) {

        // next button
        this.nextButton = document.createElement('button');
        this.nextButton.className = "controls-next-button";
        document.getElementById("app-main").appendChild(this.nextButton);
        this.nextButton.onclick = () => {
            server.send('next');
        }

        // previous button
        this.previousButton = document.createElement('button');
        this.previousButton.className = "controls-previous-button";
        document.getElementById("app-main").appendChild(this.previousButton);
        this.previousButton.onclick = () => {
            server.send('prev');
        }

        // black switcher
        this.blackSwitcher = document.createElement('button');
        this.blackSwitcher.className = "controls-black-button";
        document.getElementById("app-main").appendChild(this.blackSwitcher);
        this.blackSwitcher.onclick = () => {
            server.send('black');
        }
    }
}

export default SlideControls;