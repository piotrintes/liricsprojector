

function createWaitingIndicator() {
    let waiting = document.createElement('div');
    waiting.className = 'waiting';

    for (let i = 0; i < 4; i++) {
        let dot = document.createElement('span');
        dot.className = 'waiting-dot';
        dot.style.translate = `0 ${i % 2 * 10}px`;
        waiting.appendChild(dot);
        setTimeout(() => {
            setInterval(() => {
                dot.style.scale = dot.style.scale == '1' ? '0' : '1';
            }, 500)
        }, 250 * i)
    }

    return waiting;
}

export default createWaitingIndicator;