import createWaitingIndicator from "./waitingIndicator.js";

let mask = document.createElement('div');
mask.className = 'dialog-mask';

let pdfLoader = new class PdfLoader {
    constructor() {
        this.callback = null;
        this.server = null;
        this.window = document.createElement('div');
        this.window.className = 'dialog-window';

        this.header = document.createElement('h1');
        this.header.className = 'dialog-header';
        this.header.appendChild(document.createTextNode('Add notes'));

        // source: file
        this.srcFile = document.createElement('button');
        this.srcFile.className = 'option notes-source-file';
        this.srcFile.innerHTML = 'Load PDF from file';
        this.srcFile.onclick = () => {
            this.loadPdfFromFile(this.callback)
        }

        // source: LP server
        this.srcServer = document.createElement('button');
        this.srcServer.className = 'option notes-source-lp';
        this.srcServer.innerHTML = 'Load PDF from LP server';
        this.srcServer.onclick = () => {
            this.loadPdfFromLPServer(this.callback, this.server)
        }

        // cancel button
        this.cancel = document.createElement('button');
        this.cancel.className = 'option cancel';
        this.cancel.innerHTML = 'Cancel';
        this.cancel.onclick = () => {
            this.closeDialog();
        }

        this.waiting = createWaitingIndicator()
    }

    loadPdfFromFile(callback) {
        // open file selector dialog
        let input = document.createElement('input');
        input.type = 'file';
        input.accept = 'application/pdf';
        input.onchange = () => {
            console.log(input.files[0])
            let url = 'uploadpdf';

            let formData = new FormData();
            formData.append('file', input.files[0]);
            fetch(url, {
                method: 'POST',
                body: formData
            }).then(res => res.json()).then(json => {

                this.window.innerHTML = '';
                this.window.appendChild(this.header);
                this.window.appendChild(this.waiting);
                this.window.appendChild(document.createElement('br'));

                callback(json).then(() => {
                    this.closeDialog();
                }).catch(err => {
                    this.window.innerHTML = '';
                    this.window.appendChild(this.header);
                    this.window.appendChild(document.createElement('br'));
                    this.window.appendChild(document.createTextNode(err));
                    this.window.appendChild(this.cancel);
                })
            })
        }
        document.body.appendChild(input);
        input.click();
        document.body.removeChild(input);
    }

    loadPdfFromLPServer(callback, server) {
        this.window.innerHTML = '';
        this.window.appendChild(this.header);

        let listContainer = document.createElement('div');
        listContainer.className = 'dialog-list-container';
        listContainer.innerHTML = '';
        listContainer.appendChild(this.waiting);
        this.window.appendChild(listContainer);
        this.window.appendChild(document.createElement('br'));

        console.log(server)
        let connected = server.getConnected();

        if (!connected) {
            let i = setInterval(() => {
                connected = server.getConnected();
                if (connected) {
                    clearInterval(i);
                    this.loadPdfFromLPServer(callback, server);
                }
            }, 1000);
            return;
        }

        server.get('pdflist').then(json => {
            listContainer.innerHTML = '';
            console.log(json)
            json.forEach(pdf => {
                pdf.filename = pdf.name.split('/').at(-1);
                pdf.src = server.getUrl() + '/pdf';

                let pdfLabel = document.createElement('button');
                pdfLabel.className = "option notes-source-file";
                // pdfLabel.style.translate = '-8px';
                pdfLabel.style.marginTop = '0';
                pdfLabel.appendChild(document.createTextNode(pdf.filename));
                pdfLabel.onclick = () => {
                    callback(pdf).then(() => {
                        this.closeDialog();
                    })
                }
                listContainer.appendChild(pdfLabel);
            })
        })


        this.window.appendChild(this.cancel);
    }

    openDialog(callback, server) {
        this.window.innerHTML = '';
        this.window.appendChild(this.header);
        this.window.appendChild(this.srcFile);
        this.window.appendChild(this.srcServer);
        this.window.appendChild(this.cancel);

        this.callback = callback;
        this.server = server;

        mask.style.opacity = 1;
        document.body.appendChild(mask);
        document.body.appendChild(this.window);
    }

    closeDialog() {
        mask.style.opacity = 0;
        this.window.remove();
        setTimeout(() => {
            mask.remove();
        }, 300);
    }
}


export default pdfLoader