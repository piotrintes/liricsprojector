import Server from "./endpoints/server.js";
import Settings from "./misc/settings.js";
import ContentManager from "./screen/contentManager.js";
import Song from "./screen/song.js";
import Image from "./screen/image.js";
import NextImage from "./screen/nextImage.js";
// import Video from "./screen/video.js";
import Black from "./screen/black.js";
import WelcomeScreen from "./screen/welcome.js"
import NotesPanel from "./ui/notesPanel.js";
import SlideControls from "./ui/slideControls.js";
import ToolWidget from "./ui/toolWidget.js";

/** This is a app main class, used to set up and instantiate application. */
class App {
     constructor() {
          // handle mouse cursor (only visible on movement + 3s)
          document.body.style.cursor = 'none';
          let mouseHideTimeout = null;
          document.body.onmousemove = () => {
               document.body.style.cursor = 'auto';
               if (mouseHideTimeout) clearTimeout(mouseHideTimeout);
               mouseHideTimeout = setTimeout(() => { document.body.style.cursor = 'none'; }, 3000);
          }

          document.addEventListener('contextmenu', event => event.preventDefault());

          this.settings = new Settings('LP.Node.Settings');
          this.server = new Server();

          this.appMain = document.createElement('div');
          this.appMain.id = 'app-main';
          this.appMain.className = 'app-main';
          document.body.appendChild(this.appMain);


          this.screenArea = document.createElement('div');
          this.screenArea.className = 'screen-area dock-right vp0';
          this.appMain.appendChild(this.screenArea);

          this.nextPrewArea = document.createElement('div');
          this.nextPrewArea.className = 'screen-area next-screen';
          this.appMain.appendChild(this.nextPrewArea);

          this.background = document.createElement('div');
          this.background.className = 'fill';
          this.screenArea.appendChild(this.background);

          this.content = document.createElement('div');
          this.content.className = 'fill';
          this.screenArea.appendChild(this.content);

          this.nextPrewContent = document.createElement('div');
          this.nextPrewContent.className = 'fill';
          this.nextPrewArea.appendChild(this.nextPrewContent);

          let welcomeScreen = new WelcomeScreen(this.content, this.server);
          let songScreen = new Song(this.content, this.server, this.settings);
          let imageScreen = new Image(this.content, this.server, this.settings);
          let nextScreen = new NextImage(this.nextPrewContent, this.server, this.settings);
          // let videoScreen = new Video(this.content, this.server, this.settings);
          let blackScreen = new Black(this.content);

          let contentManager = new ContentManager(
               this.server,
               songScreen,
               imageScreen,
               // videoScreen,
               undefined,
               blackScreen,
               welcomeScreen,
               nextScreen
          );
          setTimeout(() => contentManager.connect(), 4000);

          let notesPanel = new NotesPanel(this.screenArea, this.nextPrewArea, this.server);
          this.appMain.insertBefore(notesPanel.getHTML(), this.screenArea,);

          new SlideControls(this.server);
          new ToolWidget(this.appMain);

          let screenAreaLabel = document.createElement('span');
          screenAreaLabel.className = 'screen-area-label';
          screenAreaLabel.appendChild(document.createTextNode('Teraz'));
          this.screenArea.appendChild(screenAreaLabel);

          let nextPrewAreaLabel = document.createElement('span');
          nextPrewAreaLabel.className = 'screen-area-label';
          nextPrewAreaLabel.appendChild(document.createTextNode('Nastepny'));
          this.nextPrewArea.appendChild(nextPrewAreaLabel);
     }


}


export default App;