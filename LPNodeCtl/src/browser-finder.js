const fs = require('fs');
const { execSync } = require('child_process');

function parseDesktopFileExec(filePath) {
    const data = fs.readFileSync(filePath, 'utf-8');
    const lines = data.split('\n');

    for (const line of lines) {
        if (line.startsWith('Exec=')) {
            // remove 'Exec=' from line
            const charsToSkip = line.indexOf('=') + 1;
            // return the first part of the line (skip the potential garbage)
            return line.substring(charsToSkip).split('@')[0].split('%')[0].trim();
        }
    }

    // if nothing works, return null
    return null;
}

exports.getDefaultBrowserExec = () => {
    // get default browser from xdg-mime
    const defaultBrowserFile = execSync('xdg-mime query default x-scheme-handler/http').toString().trim();

    const searchPaths = [
        '/usr/share/applications/',
        `${process.env.HOME}/.local/share/applications/`,
        '/var/lib/flatpak/exports/share/applications/'
    ];

    // search in default paths for desktop file
    for (const path of searchPaths) {
        const filePath = `${path}${defaultBrowserFile}`;
        if (fs.existsSync(filePath)) {
            return parseDesktopFileExec(filePath);
        }
    }

    // check for special case with snap: app_app.desktop should return app
    let name = defaultBrowserFile.split('_')[0];
    if (defaultBrowserFile.split('_')[1].split('.')[0] == name) {
        return name;
    }

    // if nothing works, return null
    return null;
}