const networkControler = require("./controller/network");
const filesystemControler = require("./controller/filesystem");
const getDefaultBrowserExec = require("./browser-finder").getDefaultBrowserExec;

const hostname = '127.0.0.1';

var express = require('express')
var app = express()
var fs = require('fs');
var multer = require('multer');
const currentPath = process.cwd();

const opener = require('opener');
const { exit } = require("process");

let flags = {
     'port': {
          short: 'p',
          long: 'port',
          description: 'Port to listen on',
          value: 3000,
          arguemnt: true,
     },
     'browser': {
          short: 'b',
          long: 'browser',
          description: 'Browser to use',
          value: null,
          arguemnt: true,
     },
};


/** Mark a flag as true if it is present in the command line arguments
 * @param {string} string Flag to mark
 */
function markFlag(string) {
     if (string.startsWith('--')) {
          string = string.substring(2);
          for (let flag in flags) {
               if (flags[flag].long == string)
                    flags[flag].arguemnt
                         ? (flags[flag].value = args[args.indexOf('--' + string) + 2])
                         : (flags[flag].value = true);
          }
     } else if (string.startsWith('-')) {
          for (let flag in flags) {
               if (string.includes(flags[flag].short)) {
                    flags[flag].arguemnt
                         ? (flags[flag].value = args[args.indexOf('-' + string) + 2])
                         : (flags[flag].value = true);
               }
          }
     }
}

/** Get the value of a flag
 * @param {string} string Flag to get
 * @returns {boolean} Value of the flag
 */
function getFlag(string) {
     return flags[string].value;
}

// parse command line arguments
let args = process.argv.slice(2);
if (args.length != 0) {
     args.forEach(arg => markFlag(arg));
}


function removeDiacritics(str) {
     return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

var storage = multer.diskStorage({
     destination: function (req, file, cb) {
          if (!fs.existsSync(currentPath + "/uploads")) fs.mkdirSync(currentPath + "/uploads");
          cb(null, "uploads");
     },
     filename: function (req, file, cb) {
          cb(null, removeDiacritics(file.originalname));
     },
});

if (!fs.existsSync('uploads')) fs.mkdirSync('uploads');
// clean up pdf cache at startup
fs.readdir('uploads', (err, files) => {
     for (const file of files) {
          fs.unlinkSync("uploads/" + file);
     }
})

const maxSize = 100 * 1000 * 1000;

var upload = multer({
     storage: storage,
     limits: { fileSize: maxSize },
}).single("file");



app.get('/ver', function (req, res) {
     res.send(JSON.stringify({ version: '1.0.0' }));
})
app.get('/ip-list', (req, res) => networkControler.getIpList(req, res));
app.get('/svr-ip', (req, res) => networkControler.findServer(req, res));
app.get('/ls', (req, res) => filesystemControler.ls(req, res));
app.get('/mkpdf', (req, res) => filesystemControler.mkpdf(req, res));
app.get('/file', (req, res) => filesystemControler.findFile(req, res));
app.post('/uploadpdf', (req, res, next) => {
     upload(req, res, (err) => {
          if (err) {
               res.status(500).send(err);
          } else {
               res.send({
                    path: req.file.path
               });
          }
     })
});

app.use('/', express.static(currentPath + "/front"));

const spawn = require('child_process').spawn;

const port = getFlag('port');

app.listen(port, hostname, () => {

     const addr = `http://${hostname}:${port}/`;
     console.log(`Server running at ${addr}`);

     let browserExec = "chromium";
     if (getFlag('browser')) {
          browserExec = getFlag('browser');

          console.log(`Browser: ${browserExec}`);
     } else {
          try {
               browserExec = getDefaultBrowserExec();
               if (browserExec) {
                    console.log(`Default browser: ${browserExec}`);
               } else {
                    console.log("No default browser found");
               }
          } catch (err) {
               console.log(`Error: ${err}`);
               browserExec = "chromium";

               console.log(`Falling back to ${browserExec}`);
          }
     }

     if (browserExec == "none") {
          console.log("Running in headless mode");
     } else {
          let exec = browserExec.split(' ')[0];
          let args = browserExec.split(' ').slice(1);
          args.push(addr);
          args.push('--kiosk');
          args.push(`--app=${addr}`);

          // try to run, if it fails, fallback to open
          let proc = spawn(exec, args, { detached: true })

          proc.on('close', (code) => {
               console.log(`Browser closed with code ${code}`);
               exit(0);
          })

          proc.on('error', (err) => {
               if (err) {
                    console.log(`Error: ${err}`);
                    opener(addr);
               }
          })
     }
});