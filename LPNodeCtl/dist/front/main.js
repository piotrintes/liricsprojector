(()=>{var u=class{constructor(){this.address=null,this.port=50400,this.wsport=50500,this.connected=!1,this.websocket=void 0,this.updateConnectionState()}async updateServerAddress(){if(this.address==null){let e=await fetch("svr-ip");this.address=await e.text()}}async updateConnectionState(){if(await this.updateServerAddress(),this.address==null)return;let e=await fetch(`http://${this.address}:${this.port}/ver`).then(t=>t.json()).catch(()=>{this.connected=!1,console.log(`No connection to ${this.address}:${this.port}`),this.address=null});this.connected||e&&(console.log(e),this.connected=!0,console.log("Connected to Lyrics Projector HTTP server"),this.app=e.app,this.version=e.version,this.websocket=new WebSocket(`ws://${this.address}:${this.wsport}`),this.websocket.addEventListener("message",t=>{console.log("Message from server ",t.data)}))}getConnected(){return this.connected}getVersion(){return this.version}getUrl(){return`http://${this.address}:${this.port}`}get(e){if(this.getConnected())return fetch(`http://${this.address}:${this.port}/${e}`).then(t=>t.json()).catch(()=>{})}getVideoUrl(e){return"videosrc/"+e}addWebSocketCallback(e){this.websocket.addEventListener("message",e)}send(e){this.websocket.send(e)}},S=u;var g=class{constructor(e){this.cookieName=e,this.settings=new Map,this.defaults=new Map}registerSettings(e,t,s){t!=null&&this.defaults.set(e,t),s!=null&&this.callbacks.set(e,s)}set(e,t){this.settings.set(e,t),this.callbacks.has(e)&&this.callbacks.get(e)()}get(e){return this.settings.has(e)?this.settings.get(e):this.defaults.get(e)}getJson(){let e;return this.settings.forEach((t,s)=>{e[s]=t}),e}loadJson(e){for(var t in e)this.settings.set(t,e[t])}},M=g;var w=class{constructor(e,t,s,i,a,n,o){this.server=e,this.song=t,this.image=s,this.video=i,this.black=a,this.welcome=n,this.next=o,this.lastUpdate,this.update_=this.update.bind(this)}setScreen(e,t){this[e]=t}async connect(){await this.server.updateConnectionState(),this.server.getConnected()?(this.server.addWebSocketCallback(e=>{this.update(e.data)}),this.welcome.update()):setTimeout(()=>this.connect(),5e3)}async disconnect(){await this.server.updateConnectionState(),this.welcome.update(),this.welcome.show(),setTimeout(()=>this.connect(),5e3)}update(e){try{e=="videostop"&&this.video.update(e),this.server.get("content").then(t=>{if(!t){this.disconnect();return}this.lastUpdate!=t.update&&(this.lastUpdate=t.update,t.display=="empty"?(this.song?.hide(),this.image?.hide(),this.video?.hide(),this.black?.hide()):t.display=="black"?this.black?.show():t.display=="image"&&t.content=="song"?(this.song?.update(),this.song?.show(),this.next?.hide()):t.display=="image"&&t.content=="image"?(this.image?.update(),this.next?.update(),this.image?.show(),this.next?.show()):t.display=="image"&&t.content=="video"&&(this.video?.update(e),this.video?.show(),this.next?.hide()))})}catch{this.disconnect()}}},B=w;var f=class{constructor(e){this.screen=document.createElement("div"),this.screen.className="screen",this.screen.show=this.show.bind(this),this.screen.hide=this.hide.bind(this),e.appendChild(this.screen),this.screenArea=e,this.isVisible=!1}getCurrentScreen(){for(let e of this.screenArea.children)if(e.style.opacity=="1")return e}show(){this.getCurrentScreen()?.hide(),this.screen.style.opacity="1",this.isVisible=!0}hide(){this.screen.style.opacity="0",this.isVisible=!1}},l=f;var y=class extends l{constructor(e,t,s){super(e),this.server=t,this.settings=s,this.currentText,this.screen.classList.add("song-container"),this.song=document.createElement("song"),this.screen.appendChild(this.song)}setText(e){this.currentText=e,this.song.innerHTML=e}update(){this.server.get("song/current").then(e=>{e.display=="true"&&this.currentText!=e.text&&this.setText(e.text)})}},P=y;var k=class extends l{constructor(e,t,s){super(e),this.server=t,this.settings=s,this.image=document.createElement("img"),this.image.className="image-content",this.screen.appendChild(this.image)}update(){this.image.src=this.server.getUrl()+"/img&"+Date.now()}},G=k;var v=class extends l{constructor(e,t,s){super(e),this.server=t,this.settings=s,this.image=document.createElement("img"),this.image.className="image-content",this.screen.appendChild(this.image)}update(){this.image.src=this.server.getUrl()+"/img/next&"+Date.now()}},A=v;var x=class extends l{constructor(e){super(e),this.screen.style.background="#000"}},H=x;var W=`
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->
<svg
   style="
      --circle-fill: 18;
      --opacity: 1;
      --scale: 1;
      --scale2: 1;
   "
   width="512"
   height="128"
   viewBox="0 0 135.46665 33.866668"
   version="1.1"
   id="logo-animated-svg"
   inkscape:version="1.1.2 (0a00cf5339, 2022-02-04)"
   sodipodi:docname="logo.svg"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview7"
     pagecolor="#353535"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageshadow="2"
     inkscape:pageopacity="0"
     inkscape:pagecheckerboard="true"
     inkscape:document-units="mm"
     showgrid="false"
     units="px"
     width="128px"
     inkscape:zoom="1.5287037"
     inkscape:cx="191.33858"
     inkscape:cy="52.986069"
     inkscape:window-width="1920"
     inkscape:window-height="1044"
     inkscape:window-x="0"
     inkscape:window-y="36"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs2">
    <rect
       x="149.28464"
       y="18.029891"
       width="303.95685"
       height="96.575286"
       id="rect20304" />
    <linearGradient
       inkscape:collect="always"
       id="linearGradient15434">
      <stop
         style="stop-color:#ff5a4a;stop-opacity:var(--opacity);transition: 3s;"
         offset="0"
         id="stop15430" />
      <stop
         style="stop-color:#ff8a4c;stop-opacity:var(--opacity);transition: 1s;"
         offset="1"
         id="stop15432" />
    </linearGradient>
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient11174"
       x1="26.506628"
       y1="27.655256"
       x2="4.2216492"
       y2="5.1131549"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(31.183785,-29.68128)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient12961"
       gradientUnits="userSpaceOnUse"
       x1="3.7467117"
       y1="7.3907089"
       x2="10.83133"
       y2="4.0632777"
       gradientTransform="matrix(3.1814637,0,0,3.1814637,26.741646,-3.0733481)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient12963"
       gradientUnits="userSpaceOnUse"
       x1="3.7467117"
       y1="7.3907089"
       x2="10.83133"
       y2="4.0632777"
       gradientTransform="matrix(3.1814637,0,0,3.1814637,26.741646,-3.0733481)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient15662"
       x1="31.30434"
       y1="31.483503"
       x2="31.318514"
       y2="27.976347"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(18.899411,9.789295)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient56632"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906"
       gradientUnits="userSpaceOnUse" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient839"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient841"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient843"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient845"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
  </defs>
  <g
     inkscape:label="Warstwa 1"
     inkscape:groupmode="layer"
     id="layer1">
    <circle
       style="fill:none;stroke:url(#linearGradient11174);stroke-width:1.5875;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:100;stroke-dashoffset: var(--circle-fill); transition: 1s;stroke-opacity:1"
       id="path846"
       transform="rotate(34.72762)"
       cx="48.117115"
       cy="-12.747945"
       r="14.282" />
    <path
       d="m 38.033643,22.231159 c -0.388779,0 -0.725223,-0.118106 -1.009331,-0.354309 -0.269155,-0.249328 -0.403731,-0.544582 -0.403731,-0.885775 v -8.364942 c 0,-0.269651 0.09719,-0.496725 0.291584,-0.681223 0.194391,-0.198695 0.433639,-0.298036 0.717748,-0.298036 0.284107,0 0.523357,0.09934 0.717746,0.298036 0.209344,0.184499 0.314014,0.411572 0.314014,0.681223 v 7.813791 h 6.148781 c 0.238181,0 0.438752,0.09186 0.601716,0.275575 0.175499,0.170593 0.263247,0.380555 0.263247,0.629885 0,0.249328 -0.08775,0.459291 -0.263247,0.629885 -0.162964,0.170593 -0.363536,0.255891 -0.601716,0.255891 z"
       style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;font-size:5.21147px;line-height:1.25;font-family:'Varela Round';-inkscape-font-specification:'Varela Round Bold';letter-spacing:0px;word-spacing:0px;fill:url(#linearGradient12961);fill-opacity:1;stroke:none;stroke-width:0.415;stroke-miterlimit:4;stroke-dasharray:none;scale:var(--scale);transition:0.7s;transform-origin:center;"
       id="path922"
       sodipodi:nodetypes="scsscscscscscss" />
    <path
       d="m 48.453793,22.270235 c -0.286026,0 -0.526891,-0.101355 -0.722593,-0.304059 -0.195701,-0.202705 -0.293553,-0.45219 -0.293553,-0.748454 v -8.947147 c 0,-0.18518 0.09785,-0.341122 0.293553,-0.467825 0.195702,-0.13645 0.436567,-0.204673 0.722594,-0.204673 h 4.460986 c 1.190895,0 2.162415,0.161828 2.91456,0.48549 0.752144,0.323661 1.291181,0.73835 1.617111,1.244069 0.325928,0.50572 0.488894,1.051896 0.488894,1.63853 0,0.586634 -0.162965,1.132811 -0.488894,1.63853 -0.325931,0.505719 -0.864968,0.920409 -1.617112,1.244069 -0.752144,0.323661 -1.723653,0.485491 -2.91456,0.485491 h -3.422257 v 2.883466 c 0,0.296264 -0.10538,0.545748 -0.316136,0.748454 -0.195702,0.202707 -0.436566,0.304059 -0.722593,0.304059 z M 52.745547,17.0295 c 1.278645,0 2.174951,-0.197228 2.688916,-0.591691 0.526501,-0.394461 0.789752,-0.885009 0.789752,-1.471643 0,-0.586634 -0.26325,-1.077182 -0.789752,-1.471642 -0.513965,-0.394461 -1.410271,-0.591692 -2.688916,-0.591692 H 49.492522 V 17.0295 Z"
       style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;font-size:5.21147px;line-height:1.25;font-family:'Varela Round';-inkscape-font-specification:'Varela Round Bold';letter-spacing:0px;word-spacing:0px;fill:url(#linearGradient12963);fill-opacity:1;stroke:none;stroke-width:0.415;stroke-miterlimit:4;stroke-dasharray:none;scale:var(--scale);transition:0.7s;transform-origin:center;"
       id="path924"
       sodipodi:nodetypes="sssscsssssssscscsscscsccs" />
    <circle
       style="fill:url(#linearGradient15662);fill-opacity:1;stroke:none;stroke-width:0.414999;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       id="path15538"
       cx="50.202858"
       cy="39.701466"
       r="1.6562569"
       transform="rotate(-18.527532)" />
    <g
       aria-label="node"
       transform="matrix(0.300912,0,0,0.300912,20.729436,2.2878499)"
       id="text20302">
      <path
         d="m 155.89849,65.627054 q -1.01334,0 -1.70667,-0.693333 -0.69333,-0.693333 -0.69333,-1.706666 V 40.347069 q 0,-1.013333 0.69333,-1.706666 0.69333,-0.746666 1.70667,-0.746666 1.01333,0 1.70666,0.746666 0.74667,0.693333 0.74667,1.706666 v 2.186665 q 2.02666,-2.239999 4.37333,-3.573331 2.34666,-1.333333 5.54666,-1.333333 2.93333,0 5.06667,1.386666 2.18666,1.386666 3.30666,3.786665 1.17333,2.399998 1.17333,5.279996 v 15.146658 q 0,1.013333 -0.74666,1.706666 -0.69334,0.693333 -1.70667,0.693333 -1.01333,0 -1.70666,-0.693333 -0.69334,-0.693333 -0.69334,-1.706666 V 48.61373 q 0,-6.506662 -5.86666,-6.506662 -2.66667,0 -4.8,1.386666 -2.08,1.386665 -3.94666,3.733331 v 15.99999 q 0,1.013333 -0.74667,1.706666 -0.69333,0.693333 -1.70666,0.693333 z"
         id="path836"
         style="fill:url(#linearGradient839);scale:var(--scale2);transition:1.5s;transform-origin:100% 150%;" />
      <path
         d="m 197.97842,65.89372 q -4.10666,0 -7.19999,-1.759999 -3.09333,-1.759999 -4.8,-4.959997 -1.65333,-3.253331 -1.65333,-7.413329 0,-4.159997 1.65333,-7.359995 1.70667,-3.253332 4.8,-5.013331 3.09333,-1.759999 7.19999,-1.759999 4.05333,0 7.14666,1.759999 3.09334,1.759999 4.8,5.013331 1.70667,3.199998 1.70667,7.359995 0,4.159998 -1.70667,7.413329 -1.70666,3.199998 -4.8,4.959997 -3.09333,1.759999 -7.14666,1.759999 z m 0,-4.426664 q 2.18667,0 4.16,-1.013333 2.02667,-1.013332 3.30666,-3.199998 1.33334,-2.186665 1.33334,-5.546663 0,-3.306665 -1.33334,-5.49333 -1.27999,-2.186665 -3.30666,-3.146665 -1.97333,-1.013333 -4.16,-1.013333 -2.24,0 -4.26666,1.013333 -1.97333,0.96 -3.25333,3.146665 -1.28,2.186665 -1.28,5.49333 0,3.359998 1.28,5.546663 1.28,2.186666 3.25333,3.199998 2.02666,1.013333 4.26666,1.013333 z"
         id="path838"
         style="fill:url(#linearGradient841);scale:var(--scale2);transition:1.8s;transform-origin:100% 150%;" />
      <path
         d="m 229.65839,65.89372 q -3.30666,0 -6.29333,-1.653332 -2.93333,-1.706666 -4.79999,-4.906664 -1.81334,-3.253331 -1.81334,-7.573329 0,-4.319997 1.81334,-7.519995 1.86666,-3.253332 4.79999,-4.906664 2.98667,-1.706666 6.29333,-1.706666 5.22667,0 9.12,4.053331 V 28.613742 q 0,-1.066666 0.69333,-1.759999 0.74667,-0.693332 1.76,-0.693332 1.01333,0 1.70666,0.746666 0.69334,0.693333 0.69334,1.706665 v 34.613313 q 0,1.013333 -0.69334,1.706666 -0.69333,0.693333 -1.70666,0.693333 -1.01333,0 -1.76,-0.693333 -0.69333,-0.693333 -0.69333,-1.706666 v -1.386666 q -3.89333,4.053331 -9.12,4.053331 z m 0.69333,-4.53333 q 2.45334,0 4.58667,-1.066666 2.18666,-1.12 3.84,-2.933332 V 46.160399 q -1.65334,-1.813333 -3.84,-2.879999 -2.13333,-1.119999 -4.58667,-1.119999 -3.94666,0 -6.34666,2.506665 -2.4,2.506665 -2.4,7.093329 0,4.586664 2.4,7.093329 2.4,2.506666 6.34666,2.506666 z"
         id="path840"
         style="fill:url(#linearGradient843);scale:var(--scale2);transition:2.1s;transform-origin:100% 150%;" />
      <path
         d="m 264.21829,65.89372 q -6.50666,0 -10.18666,-3.679998 -3.62666,-3.733331 -3.62666,-10.453327 0,-3.786664 1.38667,-6.986662 1.38666,-3.199998 4.26666,-5.17333 2.88,-1.973333 7.09333,-1.973333 3.94666,0 6.82666,1.866666 2.88,1.813332 4.37333,4.906664 1.49333,3.039998 1.49333,6.666662 0,1.013333 -0.69333,1.706666 -0.64,0.693333 -1.76,0.693333 h -18.07999 q 0.42667,3.733331 2.77333,5.81333 2.34667,2.079999 6.56,2.079999 2.24,0 3.89333,-0.426667 1.70667,-0.426666 3.30667,-1.226666 0.42666,-0.213333 0.96,-0.213333 0.85333,0 1.49333,0.586666 0.64,0.586667 0.64,1.493333 0,1.173332 -1.38667,1.973332 -2.08,1.173333 -4.10666,1.759999 -2.02667,0.586666 -5.22667,0.586666 z m 6.82667,-16.373323 q -0.16,-2.613332 -1.38667,-4.373331 -1.17333,-1.759999 -2.93333,-2.559999 -1.76,-0.853332 -3.57333,-0.853332 -1.81333,0 -3.57333,0.853332 -1.76,0.8 -2.93333,2.559999 -1.17334,1.759999 -1.33334,4.373331 z"
         id="path842"
         style="fill:url(#linearGradient845);scale:var(--scale2);transition:2.4s;transform-origin:100% 150%;" />
    </g>
  </g>
</svg>
`,U=W;var b=class{constructor(e){this.server=e,this.html=document.createElement("div"),this.html.className="status-display-floating",this.ipList=document.createElement("div"),this.ipList.className="list-container",this.html.appendChild(this.ipList);let t=document.createElement("span");t.className="separatorV",this.html.appendChild(t),this.cnList=document.createElement("div"),this.cnList.className="list-container",this.html.appendChild(this.cnList),this.update(),setInterval(()=>this.update(),5e3)}update(){fetch("ip-list").then(e=>e.json()).then(e=>{this.ipList.innerHTML="";let t=document.createElement("span");t.className="list-header",t.appendChild(document.createTextNode("IP address list:")),this.ipList.appendChild(t),e.forEach(s=>{let i=document.createElement("span");i.appendChild(document.createTextNode(s)),this.ipList.appendChild(i)})});{let e=this.server.getConnected();this.cnList.innerHTML="";let t=document.createElement("span");t.className="list-header",t.appendChild(document.createTextNode("Connection status:")),this.cnList.appendChild(t);let s=document.createElement("span");if(s.style.fontWeight="bold",s.style.color=e?"var(--color-green)":"var(--color-red)",s.appendChild(document.createTextNode(e?"CONNECTED":"DISCONNECTED")),this.cnList.appendChild(s),e){let i=document.createElement("span");i.appendChild(document.createTextNode("Server IP: "+this.server.address)),this.cnList.appendChild(i)}}}getHTML(){return this.html}},I=b;var C=class extends l{constructor(e,t){super(e),this.screen.style.background="#111",this.screen.style.display="flex",this.screen.style.alignItems="center",this.screen.style.justifyContent="center",this.screen.style.containerType="inline-size";let s=document.createElement("div");s.innerHTML=U,s=s.children[0],this.screen.appendChild(s),s.style.width="40cqw",s.style.setProperty("--circle-fill","100"),s.style.setProperty("--opacity","0"),s.style.setProperty("--scale2","0.5"),setTimeout(()=>{s.style.setProperty("--scale","1"),s.style.setProperty("--scale2","1")},100),setTimeout(()=>{s.style.setProperty("--opacity","1")},300),setTimeout(()=>{s.style.setProperty("--circle-fill","18")},1200),setTimeout(()=>{this.status=new I(t),this.screen.appendChild(this.status.getHTML())},3e3),this.show()}update(){this.status.update()}},$=C;function R(){let r=document.createElement("div");r.className="waiting";for(let e=0;e<4;e++){let t=document.createElement("span");t.className="waiting-dot",t.style.translate=`0 ${e%2*10}px`,r.appendChild(t),setTimeout(()=>{setInterval(()=>{t.style.scale=t.style.scale=="1"?"0":"1"},500)},250*e)}return r}var q=R;var h=document.createElement("div");h.className="dialog-mask";var J=new class{constructor(){this.callback=null,this.server=null,this.window=document.createElement("div"),this.window.className="dialog-window",this.header=document.createElement("h1"),this.header.className="dialog-header",this.header.appendChild(document.createTextNode("Add notes")),this.srcFile=document.createElement("button"),this.srcFile.className="option notes-source-file",this.srcFile.innerHTML="Load PDF from file",this.srcFile.onclick=()=>{this.loadPdfFromFile(this.callback)},this.srcServer=document.createElement("button"),this.srcServer.className="option notes-source-lp",this.srcServer.innerHTML="Load PDF from LP server",this.srcServer.onclick=()=>{this.loadPdfFromLPServer(this.callback,this.server)},this.cancel=document.createElement("button"),this.cancel.className="option cancel",this.cancel.innerHTML="Cancel",this.cancel.onclick=()=>{this.closeDialog()},this.waiting=q()}loadPdfFromFile(e){let t=document.createElement("input");t.type="file",t.accept="application/pdf",t.onchange=()=>{console.log(t.files[0]);let s="uploadpdf",i=new FormData;i.append("file",t.files[0]),fetch(s,{method:"POST",body:i}).then(a=>a.json()).then(a=>{this.window.innerHTML="",this.window.appendChild(this.header),this.window.appendChild(this.waiting),this.window.appendChild(document.createElement("br")),e(a).then(()=>{this.closeDialog()}).catch(n=>{this.window.innerHTML="",this.window.appendChild(this.header),this.window.appendChild(document.createElement("br")),this.window.appendChild(document.createTextNode(n)),this.window.appendChild(this.cancel)})})},document.body.appendChild(t),t.click(),document.body.removeChild(t)}loadPdfFromLPServer(e,t){this.window.innerHTML="",this.window.appendChild(this.header);let s=document.createElement("div");s.className="dialog-list-container",s.innerHTML="",s.appendChild(this.waiting),this.window.appendChild(s),this.window.appendChild(document.createElement("br")),console.log(t);let i=t.getConnected();if(!i){let a=setInterval(()=>{i=t.getConnected(),i&&(clearInterval(a),this.loadPdfFromLPServer(e,t))},1e3);return}t.get("pdflist").then(a=>{s.innerHTML="",console.log(a),a.forEach(n=>{n.filename=n.name.split("/").at(-1),n.src=t.getUrl()+"/pdf";let o=document.createElement("button");o.className="option notes-source-file",o.style.marginTop="0",o.appendChild(document.createTextNode(n.filename)),o.onclick=()=>{e(n).then(()=>{this.closeDialog()})},s.appendChild(o)})}),this.window.appendChild(this.cancel)}openDialog(e,t){this.window.innerHTML="",this.window.appendChild(this.header),this.window.appendChild(this.srcFile),this.window.appendChild(this.srcServer),this.window.appendChild(this.cancel),this.callback=e,this.server=t,h.style.opacity=1,document.body.appendChild(h),document.body.appendChild(this.window)}closeDialog(){h.style.opacity=0,this.window.remove(),setTimeout(()=>{h.remove()},300)}},z=J;var N=class{constructor(e,t,s){this.screenArea=e,this.prewiewArea=t,this.server=s,this.mode="list",this.pdfList=[],this.html=document.createElement("div"),this.html.className="notes-panel dock-left vp0",this.content=document.createElement("div"),this.content.className="notes-panel-content",this.html.appendChild(this.content);let i=0,a=!1;this.content.onscroll=()=>{a||(i=this.content.scrollTop/this.content.scrollHeight)},new ResizeObserver(()=>{a||(a=!0,setTimeout(()=>{a=!1},500)),this.content.scrollTop=i*this.content.scrollHeight}).observe(this.content),this.closeButton=document.createElement("button"),this.closeButton.className="notes-open-button",document.getElementById("app-main").appendChild(this.closeButton),this.closeButton.onclick=()=>{let n=this.html.classList.contains("vp0");this.setPanelMode(n?"small":"hidden")},this.backButton=document.createElement("button"),this.backButton.className="notes-back-button",document.getElementById("app-main").appendChild(this.backButton),this.backButton.onclick=()=>{this.displayNoteList()},this.addButton=document.createElement("button"),this.addButton.className="notes-add-button",document.getElementById("app-main").appendChild(this.addButton),this.addButton.onclick=()=>{z.openDialog(async n=>{n.path?await this.setupNotes(n.path):n.id&&this.addPdf(n)},this.server)},this.darkModeSwitcher=document.createElement("button"),this.darkModeSwitcher.className="notes-dark-button",document.getElementById("app-main").appendChild(this.darkModeSwitcher),this.darkModeSwitcher.onclick=()=>{this.html.classList.toggle("dark-mode"),this.darkMode=this.html.classList.contains("dark-mode")},this.ratioSwitcher=document.createElement("button"),this.ratioSwitcher.className="notes-ratio-switcher vp0",document.getElementById("app-main").appendChild(this.ratioSwitcher),this.ratioSwitcher.onclick=()=>{let n=this.html.classList.contains("vp40");this.setPanelMode(n?"large":"small")},this.setPanelMode("hidden"),this.displayNoteList()}getHTML(){return this.html}addPdf(e){this.pdfList.push(e),this.mode=="list"&&this.displayNoteList()}async setupNotes(e){return await fetch(`/mkpdf?file=${e}`).then(t=>t.json()).then(t=>(this.addPdf(t),t))}displayNotes(e){console.log(e),this.content.innerHTML="";for(let t=0;t<e.pages;t++){let s=document.createElement("img");s.className="notes-page",s.src=`${e.src}/${e.id}.${t+1}.png`,s.onabort=()=>{console.log("aborted")},this.content.appendChild(s)}this.mode="notes",this.backButton.style.display="block",this.darkModeSwitcher.style.display="block",this.addButton.style.display="none"}displayNoteList(){this.content.innerHTML="",this.pdfList.forEach(e=>{let t=document.createElement("button");t.className="option notes-item",t.innerHTML=e.filename,t.onclick=()=>{this.displayNotes(e)},this.content.appendChild(t)}),this.mode="list",this.backButton.style.display="none",this.darkModeSwitcher.style.display="none",this.addButton.style.display="block"}setPanelMode(e){e=="hidden"?(this.html.className="notes-panel dock-left vp0",this.screenArea.className="screen-area dock-right vp0",this.ratioSwitcher.className="notes-ratio-switcher vp0",this.prewiewArea.className="next-screen vp0",this.closeButton.className="notes-open-button",this.backButton.style.translate="-100px",this.addButton.style.translate="-100px",this.darkModeSwitcher.style.translate="-100px"):e=="small"?(this.html.className="notes-panel dock-left vp40"+(this.darkMode?" dark-mode":""),this.screenArea.className="screen-area dock-right vp40",this.ratioSwitcher.className="notes-ratio-switcher vp40",this.prewiewArea.className="next-screen vp40",this.closeButton.className="notes-close-button",this.backButton.style.translate="0",this.addButton.style.translate="0",this.darkModeSwitcher.style.translate="0"):e=="large"&&(this.html.className="notes-panel dock-left vp75"+(this.darkMode?" dark-mode":""),this.screenArea.className="screen-area dock-right vp75",this.ratioSwitcher.className="notes-ratio-switcher vp75",this.prewiewArea.className="next-screen vp75",this.closeButton.className="notes-close-button",this.backButton.style.translate="0",this.addButton.style.translate="0",this.darkModeSwitcher.style.translate="0")}},F=N;var E=class{constructor(e){this.nextButton=document.createElement("button"),this.nextButton.className="controls-next-button",document.getElementById("app-main").appendChild(this.nextButton),this.nextButton.onclick=()=>{e.send("next")},this.previousButton=document.createElement("button"),this.previousButton.className="controls-previous-button",document.getElementById("app-main").appendChild(this.previousButton),this.previousButton.onclick=()=>{e.send("prev")},this.blackSwitcher=document.createElement("button"),this.blackSwitcher.className="controls-black-button",document.getElementById("app-main").appendChild(this.blackSwitcher),this.blackSwitcher.onclick=()=>{e.send("black")}}},V=E;var L=class{constructor(e){this.appMain=e,this.widgets=[],this.currentWidget=0,this.container=document.createElement("div"),this.container.className="tool-widget",this.appMain.appendChild(this.container),this.content=document.createElement("div"),this.content.className="tool-widget-content",this.container.appendChild(this.content),this.switchButton=document.createElement("button"),this.switchButton.className="tool-widget-switch",this.switchButton.onclick=()=>{this.currentWidget=(this.currentWidget+1)%this.widgets.length,this.content.innerHTML="",this.content.appendChild(this.widgets[this.currentWidget])},this.container.appendChild(this.switchButton),this.addClock(),this.addBattery(),this.content.appendChild(this.widgets[0])}addClock(){let e=document.createElement("div");e.className="tool-widget-clock",this.widgets.push(e),setInterval(()=>{let s=new Date().toLocaleTimeString();e.innerHTML=`${s.split(":")[0]}:${s.split(":")[1]}`},1e3)}addBattery(){let e=document.createElement("div");e.className="tool-widget-battery-container",e.style.display="flex",e.style.gap="8px",this.widgets.push(e);let t=document.createElement("div");t.className="tool-widget-battery",e.appendChild(t);let s=document.createElement("span");s.className="tool-widget-battery-fill",t.appendChild(s);let i=document.createElement("div");i.className="tool-widget-battery-text",e.appendChild(i);let a=document.createElement("span");a.className="tool-widget-battery-time",i.appendChild(a);let n=document.createElement("span");n.className="tool-widget-battery-time",i.appendChild(n),setInterval(()=>{navigator.getBattery().then(o=>{s.style.width=`${o.level*100}%`,s.classList.toggle("low",o.level<.15),s.classList.toggle("charge",o.charging),a.innerHTML=`${Math.floor(o.level*100)}%`;function p(c){if(c==1/0)return"";let d=Math.floor(c/3600),m=Math.floor(c/60);return d>0?`${d}h`:m>0?`${m}m`:"< 1m"}n.innerHTML=o.charging?p(o.chargingTime):p(o.dischargingTime)})},1e3)}},D=L;var T=class{constructor(){document.body.style.cursor="none";let e=null;document.body.onmousemove=()=>{document.body.style.cursor="auto",e&&clearTimeout(e),e=setTimeout(()=>{document.body.style.cursor="none"},3e3)},document.addEventListener("contextmenu",m=>m.preventDefault()),this.settings=new M("LP.Node.Settings"),this.server=new S,this.appMain=document.createElement("div"),this.appMain.id="app-main",this.appMain.className="app-main",document.body.appendChild(this.appMain),this.screenArea=document.createElement("div"),this.screenArea.className="screen-area dock-right vp0",this.appMain.appendChild(this.screenArea),this.nextPrewArea=document.createElement("div"),this.nextPrewArea.className="screen-area next-screen",this.appMain.appendChild(this.nextPrewArea),this.background=document.createElement("div"),this.background.className="fill",this.screenArea.appendChild(this.background),this.content=document.createElement("div"),this.content.className="fill",this.screenArea.appendChild(this.content),this.nextPrewContent=document.createElement("div"),this.nextPrewContent.className="fill",this.nextPrewArea.appendChild(this.nextPrewContent);let t=new $(this.content,this.server),s=new P(this.content,this.server,this.settings),i=new G(this.content,this.server,this.settings),a=new A(this.nextPrewContent,this.server,this.settings),n=new H(this.content),o=new B(this.server,s,i,void 0,n,t,a);setTimeout(()=>o.connect(),4e3);let p=new F(this.screenArea,this.nextPrewArea,this.server);this.appMain.insertBefore(p.getHTML(),this.screenArea),new V(this.server),new D(this.appMain);let c=document.createElement("span");c.className="screen-area-label",c.appendChild(document.createTextNode("Teraz")),this.screenArea.appendChild(c);let d=document.createElement("span");d.className="screen-area-label",d.appendChild(document.createTextNode("Nastepny")),this.nextPrewArea.appendChild(d)}},O=T;setTimeout(()=>{new O},2e3);})();
