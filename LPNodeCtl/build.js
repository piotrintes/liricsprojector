const esbuild = require('esbuild');
const fs = require('fs');
const exec = require('child_process').exec;

const targetDir = 'dist';

// define flags and parse command line arguments
let flags = {
    'clean': {
        short: 'c',
        long: 'clean',
        description: 'Clean target directory before building',
        value: false,
    },
    'watch': {
        short: 'w',
        long: 'watch',
        description: 'Watch source directories for changes',
        value: false,
    },
    'dev': {
        short: 'd',
        long: 'dev',
        description: 'Build for development (no minification)',
        value: false,
    },
    'help': { short: 'h', long: 'help', description: 'Show this help', value: false },
};

/** Mark a flag as true if it is present in the command line arguments
 * @param {string} string Flag to mark
 */
function markFlag(string) {
    if (string.startsWith('--')) {
        string = string.substring(2);
        for (let flag in flags) {
            if (flags[flag].long == string)
                flags[flag].arguemnt
                    ? (flags[flag].value = args[args.indexOf('--' + string) + 2])
                    : (flags[flag].value = true);
        }
    } else if (string.startsWith('-')) {
        for (let flag in flags) {
            if (string.includes(flags[flag].short))
                flags[flag].arguemnt
                    ? (flags[flag].value = args[args.indexOf('-' + string) + 2])
                    : (flags[flag].value = true);
        }
    }
}

/** Get the value of a flag
 * @param {string} string Flag to get
 * @returns {boolean} Value of the flag
 */
function getFlag(string) {
    return flags[string].value;
}

// parse command line arguments
let args = process.argv.slice(2);
if (args.length != 0) {
    args.forEach(arg => markFlag(arg));
}

/** Clear target directory if it exists */
function clean() {
    console.log('\x1b[37mCleaning target directory...\x1b[0m');
    if (fs.existsSync(targetDir)) fs.rmSync(targetDir, { recursive: true }, () => { });
    if (fs.existsSync('tmp')) fs.rmSync('tmp', { recursive: true }, () => { });
}

/** Build frontend */
async function buildFrontend() {
    console.log('\x1b[37mBuilding frontend...\x1b[0m');

    // create target directory
    if (!fs.existsSync(`${targetDir}/front`)) fs.mkdirSync(`${targetDir}/front`, { recursive: true });

    // copy static files
    fs.copyFileSync('src/front/index.html', `${targetDir}/front/index.html`);
    //fs.copyFileSync('front/favicon.png', `${targetDir}/front/favicon.png`);

    await esbuild.build({
        entryPoints: ['src/front/src/main.js'],
        bundle: true,
        outfile: `${targetDir}/front/main.js`,
        minify: !getFlag('dev'),
        sourcemap: getFlag('dev'),
    }).catch((e) => {
        console.log('\n\x1b[91mBUILD FAILED\x1b[0m');
        console.log('\x1b[90m' + e + '\x1b[0m\n');
    });

    await esbuild.build({
        entryPoints: ['src/front/style/style.css'],
        bundle: true,
        outfile: `${targetDir}/front/style/style.css`,
        minify: !getFlag('dev'),
        sourcemap: getFlag('dev'),
        loader: {
            '.png': 'file',
            '.svg': 'file',
            '.woff': 'file',
            '.woff2': 'file',
            '.ttf': 'file',
            '.eot': 'file',
            // Add more file extensions as needed
        },
    }).catch((e) => {
        console.log('\n\x1b[91mBUILD FAILED\x1b[0m');
        console.log('\x1b[90m' + e + '\x1b[0m\n');
    });

    // postprocess stylesheets
    // fileReplace(`${targetDir}/front/style/style.css`, /url\(".\//g, `url("css/`);
}

/** Build backend */
async function buildBackend() {
    console.log('\x1b[37mBuilding backend...\x1b[0m');

    // create target directory
    if (!fs.existsSync(targetDir)) fs.mkdirSync(targetDir, { recursive: true });

    // build backend
    await esbuild.build({
        entryPoints: ['src/app.js'],
        bundle: true,
        outfile: `${targetDir}/app.js`,
        platform: 'node',
        minify: !getFlag('dev'),
        sourcemap: getFlag('dev'),
    }).catch((e) => {
        console.log('\n\x1b[91mBUILD FAILED\x1b[0m');
        console.log('\x1b[90m' + e + '\x1b[0m\n');
    });

    // copy static files
    // fs.copyFileSync('config.json', `${targetDir}/config.json`);
}

async function buildExecutable(target, output) {
    // use npx pkg to build the executable
    let promise = new Promise((resolve, reject) => {
        exec(`npx pkg ${targetDir}/app.js -t ${target} -o ${targetDir}/${output}`, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                reject();
                return;
            }
            resolve();
        })
    });
    return promise;
}

/** Build everything according to the flags */
async function build() {
    let timeStart = Date.now();
    if (getFlag('clean')) clean();
    await buildFrontend();
    await buildBackend();

    // build executables for linux x64, linux arm64, windows and mac
    if (!getFlag('dev')) {
        console.log('\x1b[37mBuilding executables...\x1b[0m');
        await buildExecutable('node18-linux-x64', 'lpnodectl');
        await buildExecutable('node18-linux-arm64', 'lpnodectl-arm64');
        await buildExecutable('node18-win-x64', 'lpnodectl.exe');
        await buildExecutable('node18-mac-x64', 'lpnodectl-mac');
    }


    console.log('\n\x1b[92mBUILD SUCCESSFUL\x1b[0m');

    console.log('\x1b[90mBuild in: ' + (Date.now() - timeStart) + 'ms\x1b[0m\n');
}

// show help
if (getFlag('help')) {
    console.log('\n\x1b[37mUsage: node build.js [options]\x1b[0m\n');
    console.log('\x1b[37mOptions:\x1b[0m');
    for (let flag in flags) {
        console.log(
            `  \x1b[93m - ${flags[flag].short}\x1b[0m \x1b[97m--${flags[flag].long.padEnd(
                10
            )
            }\x1b[0m \x1b[90m${flags[flag].description}\x1b[0m`
        );
    }
    console.log();
    process.exit(0);
}

console.log();

// build
build().then(() => {
    // watch for changes
    if (getFlag('watch')) {
        let watchPaths = ['src'];
        console.log('\x1b[94mWatching for changes...\x1b[0m');

        let scheduleUpdate = false;

        // watch for changes
        watchPaths.forEach(path => {
            fs.watch(path, { recursive: true }, (event, filename) => {
                if (filename == null) return;
                scheduleUpdate = true;
            });
        });

        // update on change
        setInterval(async () => {
            if (scheduleUpdate) {
                scheduleUpdate = false;
                console.log('\x1b[97mChange detected, rebuilding...\x1b[0m\n');
                try {
                    await build();
                    scheduleUpdate = false;
                } catch (e) {
                    console.log('\n\x1b[91mBUILD FAILED\x1b[0m');
                    console.log('\x1b[90m' + e + '\x1b[0m\n');
                }
            }
        }, 1000);
    }
});