# chromium --kiosk --no-first-run --password-store=basic --autoplay-policy=no-user-gesture-required --app="http://localhost:3000"
/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=/app/bin/chrome --file-forwarding com.google.Chrome --kiosk --no-first-run --password-store=basic --autoplay-policy=no-user-gesture-required --app="http://localhost:3000"
