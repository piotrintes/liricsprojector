#-------------------------------------------------
#
# Project created by QtCreator 2021-04-13T20:33:56
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets network websockets

#QT       += winextras   # windows only

VERSION = 2.5.2
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LyricsProjector
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

#include(config/include.pri)

SOURCES += \
        misc/aboutwindow.cpp \
        misc/styledtoolbutton.cpp \
        config/autostartmanager.cpp \
        config/config.cpp \
        config/sourcesmanager.cpp \
        config/theme.cpp \
        config/themeeditor.cpp \
        network/httprequest.cpp \
        network/network.cpp \
        network/networkmanager.cpp \
        library/pdf.cpp \
        library/planelement.cpp \
        library/song.cpp \
        library/bcg.cpp \
        main_window/mainwindow.cpp \
        main_window/mainwindow_autostart.cpp \
        main_window/mainwindow_bible.cpp \
        main_window/mainwindow_images.cpp \
        main_window/mainwindow_menus.cpp \
        main_window/mainwindow_pdf.cpp \
        main_window/mainwindow_songs.cpp \
        main_window/mainwindow_ui.cpp \
        main_window/mainwindow_videos.cpp \
        main_window/findandreplacewindow.cpp \
        main_window/songlibrarybutton.cpp \
        main_window/imagelibrarybutton.cpp \
        main_window/itemlistbutton.cpp \
        output_window/outputwindow.cpp \
        output_window/screenmanager.cpp \
        song_editor/songedit.cpp \
        song_editor/songeditor.cpp \
        main.cpp

HEADERS += \
        misc/aboutwindow.h \
        misc/consts.h \
        misc/styledtoolbutton.h \
        config/autostartmanager.h \
        config/config.h \
        config/sourcesmanager.h \
        config/theme.h \
        config/themeeditor.h \
        network/network.h \
        network/networkmanager.h \
        library/pdf.h \
        library/planelement.h \
        library/song.h \
        library/bcg.h \
        main_window/mainwindow.h \
        main_window/findandreplacewindow.h \
        main_window/imagelibrarybutton.h \
        main_window/itemlistbutton.h \
        main_window/songlibrarybutton.h \
        output_window/screenmanager.h \
        song_editor/songedit.h \
        song_editor/songeditor.h \
        output_window/outputwindow.h

FORMS += \
        misc/aboutwindow.ui \
        config/autostartmanager.ui \
        config/sourcesmanager.ui \
        config/themeeditor.ui \
        network/networkmanager.ui \
        main_window/mainwindow.ui \
        main_window/findandreplacewindow.ui \
        song_editor/songeditor.ui \
        output_window/outputwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
