#ifndef STYLEDTOOLBUTTON_H
#define STYLEDTOOLBUTTON_H

#include <QToolButton>

class StyledToolButton : public QToolButton
{
    Q_OBJECT
public:
    StyledToolButton(QWidget* parent = Q_NULLPTR);

protected:
    virtual void paintEvent(QPaintEvent *event);
};

#endif // STYLEDTOOLBUTTON_H
