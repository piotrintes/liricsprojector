#include "styledtoolbutton.h"

#include <QPainter>

StyledToolButton::StyledToolButton(QWidget* parent): QToolButton(parent)
{}

void StyledToolButton::paintEvent(QPaintEvent *event)
{
    // this button is platform-specyfic
#ifndef Q_OS_WINDOWS
    // on Linux/Mac follow theme (it looks good)
    QToolButton::paintEvent(event);
#else
    // on Windows draw over - native QToolButton looks owfull

    // setup painter
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    if(underMouse()){
        painter.setPen(QColor(0,0,0,0));
        painter.setBrush(QColor(128,128,128,80));
        painter.drawRoundedRect(0,0,width(),height(),4,4);
    }
    if(isChecked()){
        painter.setPen(QColor(0,0,0,0));
        painter.setBrush(QColor(0,0,0,40));
        painter.drawRoundedRect(0,0,width(),height(),4,4);
    }


    painter.drawPixmap((width()-22)/2,(height()-22)/2,22,22,icon().pixmap(22,22));
#endif
}
