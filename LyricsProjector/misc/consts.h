#ifndef CONSTS_H
#define CONSTS_H

#include <QColor>

namespace indicator_colors {
    const QColor verse_color(120,120,120);
    const QColor chorus_color(70,104,255);
    const QColor bridge_color(255,60,60);
    const QColor prechorus_color(60,200,60);
    const QColor other_color(200,100,255);
}


#endif // CONSTS_H
