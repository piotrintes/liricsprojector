/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "pdf.h"

#include <QDebug>
#include <QDir>
#include <QDateTime>

/** Get pdf title */
QString Pdf::title() const {
     return title_;
}

/** Set pdf title
 * @param title Pdf title
 */
void Pdf::setTitle(const QString& title) {
     title_ = title;
}

/** Get pdf filename
 * @return Pdf filename
 */
QString Pdf::filename() const {
     return filename_;
}

/** Set pdf filename
 * @param filename Pdf filename
 */
void Pdf::setFilename(const QString& filename) {
     filename_ = filename;
}

/** Get pdf button for the library list
 * @return Pdf button
 */
ImageLibraryButton* Pdf::button() const {
     return button_;
}

/** Set pdf button for the library list
 * @param value Pdf button
 */
void Pdf::setButton(ImageLibraryButton* value) {
     button_ = value;
     button_->setSlideNumber(slides.length());
     button_->repaint();
}

/** Add to a pdf reference counter. Reference counter is used to keep track of the pdf references to prevent memory leaks.
 * This should be called every time a new reference to the pdf is created.
 */
QString Pdf::id() const {
     return id_;
}
void Pdf::addReference() {
     qDebug() << "Pdf " << filename_ << " added reference: " << referenceCounter;
     referenceCounter++;
}

/** Remove from a pdf reference counter.
 * This should be called every time a reference to the pdf is removed. If the reference counter reaches 0, the pdf is deleted.
 */
void Pdf::removeReference() {
     qDebug() << "Pdf " << filename_ << " removed reference: " << referenceCounter;
     referenceCounter--;
     if (referenceCounter == 0) delete this;
}

/**
 * @param filename Pdf filename
*/
Pdf::Pdf(QString filename) {
     //setup filename and title
     filename_ = filename;
     title_ = filename.split("/").last();

     id_ = QString::number(QDateTime::currentDateTime().toMSecsSinceEpoch(), 16);

     // load images rendered by mutool from pdf cache dir and add to slides list
     int slide = 1;

     while (QFile(QDir::homePath() + "/.LyricsProjector/pdfCache/" + title_ + QString::number(slide) + ".png").exists()) {
          QPixmap p;
          p.load(QDir::homePath() + "/.LyricsProjector/pdfCache/" + title_ + QString::number(slide) + ".png");
          // cut edges in case they contain faulty colors
          QPixmap* pic = new QPixmap(p.copy(1, 1, p.width() - 2, p.height() - 2));
          slides.append(pic);
          slide++;
     }
}

Pdf::~Pdf() {
     qDebug() << "Pdf " << filename_ << " deleted";
     foreach(QPixmap * pic, slides) {
          delete pic;
     }
}
