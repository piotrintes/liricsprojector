/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef SONG_H
#define SONG_H

#include <QHash>
#include <QList>
#include <QMenu>
#include <QString>
#include <QWidget>

/** This class represents song item object (single slide) */
class SongItem {
private:
    QString name_ = "v1";                           // item name
    QString text_;                                  // item text
    QString searchText_;                            // item text modified for search
    QHash<QString, QString> textLng_;               // item text in non-default language

public:
    SongItem(QString name, QString text);

    QString text() const;
    void setText(QString& value);
    void setTextLng(QString lng, QString& value);
    QString name() const;
    void setName(const QString& value);
    QString searchText() const;
    QString textLng(QString lng) const;
};

/** This class represents song object */

class Song : public QObject {
    Q_OBJECT
private:
    uint referenceCounter = 0;                      // reference counter
    QString title_;                                 // song title
    QString filename_;                              // song filename

    QWidget* libButton_;                            // song library button
    QMenu* rmbLibMenu_;                             // song right mouse button context menu (for library button)
    QAction* edit_;                                 // edit song action for context menu (both library and plan) and edit button

    bool hasWarning_ = false;                       // should a warning icon be displayed with song
    QString warningMessage_;                        // warning message
    QStringList lngList_;                           // list of song languages
public:
    Song();

    QList<SongItem> items;                          // public list of song items

    void readFromXML(QString XML);
    void readLngFromXML(QString lng, QString XML);
    QString wrtieToXML();
    QString wrtieLngToXML(QString lng);

    bool search(QString text);
    int findAndReplace(QString find, QString replace, bool searchText, bool searchTitle, bool searchItemName);

    bool evaluate();

    QString filename() const;
    void setFilename(const QString& filename);
    QWidget* libButton() const;
    QString title() const;
    void setTitle(const QString& title);
    QMenu* rmbLibMenu() const;
    QAction* editAction() const;
    bool hasWarning() const;
    QString warningMessage() const;
    QStringList lngList() const;
    void addLng(QString lng);

signals:
    void addToPlan();                               // emited when song should be added for plan
    void openSong();                                // emited when song should be opened
    void edit();                                    // emited when song should be edited
    void removeSong();                              // emited when song should be removed
};

#endif // SONG_H
