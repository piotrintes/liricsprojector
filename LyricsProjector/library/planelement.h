/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef PLANELEMENT_H
#define PLANELEMENT_H

#include <QMenu>
#include <QObject>

/** This class represents plan element. It is paired with list item */
class PlanElement : public QObject {
    Q_OBJECT
private:
    QMenu* rmbMenu_;                        // right mouse button context menu

public:
    PlanElement();

    void setRmbMenu(QMenu* rmbMenu);
    QMenu* rmbMenu() const;

public slots:
    void trigger();

signals:
    void triggered();                       // emited when paired item is double clicked

};

#endif // PLANELEMENT_H
