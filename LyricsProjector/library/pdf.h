/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef PDF_H
#define PDF_H

#include "main_window/imagelibrarybutton.h"

#include <QObject>
#include <QPixmap>

/** Pdf object, represents a PDF document in LP library */
class Pdf : public QObject {
    Q_OBJECT
private:
    uint referenceCounter = 0;                      // reference counter
    QString id_;                                    // id for web socket
    QString title_;                                 // pdf title
    QString filename_;                              // pdf filename

    ImageLibraryButton* button_;                    // library list button

public:
    Pdf(QString filename);
    ~Pdf();

    QList<QPixmap*> slides;                         // public list containing rendered slides

    QString title() const;
    void setTitle(const QString& title);
    QString filename() const;
    void setFilename(const QString& filename);
    ImageLibraryButton* button() const;
    void setButton(ImageLibraryButton* value);
    bool isAutoloaded() const;
    void setIsAutoloaded(bool isAutoloaded);
    QString id() const;

    void addReference();
    void removeReference();
};

#endif // PDF_H
