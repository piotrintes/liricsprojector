/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef BCG_H
#define BCG_H

#include "main_window/imagelibrarybutton.h"

#include <QObject>
#include <QPixmap>

/** Bcg object, represents a background in LP library */
class Bcg : public QObject {
    Q_OBJECT
private:
    QString name_;
    QString image_;
    QString video_;
    QColor color_;

    QString type_;

    ImageLibraryButton* button_ = nullptr;                    // library list button

public:
    Bcg(QString type, QString source);
    ~Bcg();

    QString image() const;
    QString name() const;
    QString type() const;
    QString video() const;
    QColor color() const;

    ImageLibraryButton* button();

    void setIsAutoloaded(bool isAutoloaded);
};

#endif