#include "bcg.h"
#include <QDebug>

Bcg::Bcg(QString type, QString source) {
    type_ = type;
    if (type_ == "color") {
        color_ = QColor(source);
        name_ = "Jednolity kolor";

        button_ = new ImageLibraryButton("color", name_);
        QPixmap* pix = new QPixmap(80, 50);
        pix->fill(color_);
        button_->setImage(pix);
        button_->repaint();

    } else if (type_ == "image") {
        image_ = source;
        name_ = source.split("/").last();

        button_ = new ImageLibraryButton(source, name_);

    } else if (type_ == "video") {
        video_ = source;
        name_ = source.split("/").last();

        button_ = new ImageLibraryButton("", name_);
        button_->setVideoPreview(source);
    }

    button_->setArrowOnMouseOver(false);
}

Bcg::~Bcg() {
    if (button_ != nullptr) delete button_;
}

QString Bcg::image() const {
    return image_;
}

QString Bcg::name() const {
    return name_;
}

QString Bcg::type() const {
    return type_;
}

QString Bcg::video() const {
    return video_;
}

QColor Bcg::color() const {
    return color_;
}

ImageLibraryButton* Bcg::button() {
    return button_;
}

void Bcg::setIsAutoloaded(bool isAutoloaded) {
    if (button_ != nullptr) button_->setAutoloaded(isAutoloaded);
}
