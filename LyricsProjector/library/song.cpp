/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "song.h"
#include "main_window/songlibrarybutton.h"
#include <QApplication>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

/****************************************************************************
 ** song item object (single slide)
****************************************************************************/

/**
 * @param name - item name
 * @param text - item text
*/
SongItem::SongItem(QString name, QString text) {
    setName(name);
    setText(text);
}

/** Get text to search in (lower case, no commas, spaces or new lines) */
QString SongItem::searchText() const {
    return searchText_;
}

/** Get text in specific language. If not found, returns default text
 * @param lng - language code
 */
QString SongItem::textLng(QString lng) const {
    if (textLng_.contains(lng))
        return textLng_.value(lng);
    else
        return text_;
}

/** Get item name */
QString SongItem::name() const {
    return name_;
}

/** Set item name (ex Z1). First letter is capitalized.
 * @param value - item name
 */
void SongItem::setName(const QString& value) {
    name_ = value.at(0).toUpper() + value.right(value.size() - 1);
}

/** Get item text */
QString SongItem::text() const {
    return text_;
}

/** Set item text
 * @param value - item text
 */
void SongItem::setText(QString& value) {
    text_ = value;
    searchText_ = value.remove(",").replace("\n", "").remove(" ").toLower();
}

/** Set item text in specific language
 * @param lng - language code
 * @param value - item text
 */
void SongItem::setTextLng(QString lng, QString& value) {
    textLng_.insert(lng, value);
}

/****************************************************************************
 ** song
****************************************************************************/

/** Get song filename */
QString Song::filename() const {
    return filename_;
}

/** Set song filename
 * @param filename - song filename
 */
void Song::setFilename(const QString& filename) {
    filename_ = filename;
}

/** Get library button */
QWidget* Song::libButton() const {
    return libButton_;
}

/** Get song title */
QString Song::title() const {
    return title_;
}

/** Set song title
 * @param title - song title
 */
void Song::setTitle(const QString& title) {
    title_ = title;
    libButton()->repaint();
}

/** Get RMB library menu */
QMenu* Song::rmbLibMenu() const {
    return rmbLibMenu_;
}

/** Get edit action */
QAction* Song::editAction() const {
    return edit_;
}

/** Check if song has warnings */
bool Song::hasWarning() const {
    return hasWarning_;
}

/** Get warning message */
QString Song::warningMessage() const {
    return warningMessage_;
}

/** Get list of song languages */
QStringList Song::lngList() const {
    return lngList_;
}

/** Add language for the song
 * @param lng - language code
 */
void Song::addLng(QString lng) {
    lngList_.append(lng);
}

Song::Song() {
    // create library button
    SongLibraryButton* libBtn = new SongLibraryButton(this);


    // when library button addClicked is triggered - execute lambda expression - request to add song to plan
    connect(libBtn, &SongLibraryButton::addClicked, [this]() {
        emit addToPlan();
    });
    // when library button is selected - execute lambda expression - request to open song
    connect(libBtn, &SongLibraryButton::selected, [this]() {
        emit openSong();
    });

    // set library button
    libButton_ = libBtn;


    //setup RMB menus
    QAction* add = new QAction("Dodaj do planu");
#ifndef Q_OS_LINUX
    add->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/arrow-right.png")));
#else
    add->setIcon(QIcon::fromTheme("arrow-right"));
#endif

    edit_ = new QAction("Edytuj");
#ifndef Q_OS_LINUX
    edit_->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/document-edit.png")));
#else
    edit_->setIcon(QIcon::fromTheme("document-edit"));
#endif

    QAction* save = new QAction("Eksportuj");
#ifndef Q_OS_LINUX
    save->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/document-save-as.png")));
#else
    save->setIcon(QIcon::fromTheme("document-save-as"));
#endif

    QAction* remove = new QAction("Usuń");
#ifndef Q_OS_LINUX
    remove->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png")));
#else
    remove->setIcon(QIcon::fromTheme("edit-delete"));
#endif


    // when add action is triggered - execute lambda expression - request to add song to plan
    connect(add, &QAction::triggered, [this]() {
        emit addToPlan();
    });
    // when edit action is triggered - execute lambda expression - request for song editor
    connect(edit_, &QAction::triggered, [this]() {
        emit edit();
    });
    // when save action is triggered - execute lambda expression - export song (copy file to provided location)
    connect(save, &QAction::triggered, [this]() {
        QString file = QFileDialog::getSaveFileName(nullptr, "Eksportuj pieśń", title(), "xml (*.xml)");
        if (file != "")
            QFile::copy(filename(), file);
    });
    // when remove action is triggered - execute lambda expression - request to remove song from library
    connect(remove, &QAction::triggered, [this]() {
        emit removeSong();
    });

    // compose menu
    rmbLibMenu_ = new QMenu();
    rmbLibMenu_->addAction(add);
    rmbLibMenu_->addAction(edit_);
    rmbLibMenu_->addAction(save);
    rmbLibMenu_->addAction(remove);
}

/** Read song from XML string
 * @param XML - song XML
 */
void Song::readFromXML(QString XML)     // no, there's no need for XML parser there ;-)
{
    // get the title
    title_ = XML.split("<title>").at(1).split("</title>").at(0);

    // get verse names
    QStringList verseNames = XML.split("<verse name=\"");
    for (int i = 0;i < verseNames.size();i++) {
        verseNames[i] = verseNames.at(i).split("\">").at(0);
    }

    // get verse texts
    QStringList verses = XML.split("<lines>");
    for (int i = 0;i < verses.size();i++) {
        verses[i] = verses.at(i).split("</lines>").at(0);
    }

    // clear items list and warning flag
    items.clear();
    hasWarning_ = false;

    // for each verse create song item
    for (int i = 1;i < verses.size();i++) {
        QString text = verses.at(i);
        items.append(SongItem(verseNames.at(i), text.replace("<br/>", "\n")));
    }

    // search for eventual warnings
    evaluate();
}

/** Read song language text from XML string
 * @param lng - language code
 * @param XML - song XML
 */
void Song::readLngFromXML(QString lng, QString XML) {
    // get verse texts
    QStringList verses = XML.split("<lines>");
    for (int i = 0;i < verses.size();i++) {
        verses[i] = verses.at(i).split("</lines>").at(0);
    }

    // for each verse fill song item
    for (int i = 1;i < verses.size();i++) {
        QString text = verses.at(i);
        if (i <= items.length())
            items[i - 1].setTextLng(lng, text.replace("<br/>", "\n"));
    }

    lngList_.append(lng);
}

/** Write song to XML string
 * @return song XML
*/
QString Song::wrtieToXML()              // even less need for XML parser there :-)
{
    // build file frame
    QString XML = "<?xml version='1.0' encoding='UTF-8'?>\n<song"
        " xmlns=\"http://openlyrics.info/namespace/2009/song\" version=\"0.8\" "
        "createdIn=\"LyricsProjector\">\n<properties>\n<titles>\n<title>";
    XML.append(title());
    XML.append("</title>\n</titles>\n<authors><author>.</author></authors>\n</properties>\n<lyrics>\n");

    // print all items
    foreach(SongItem item, items) {
        XML.append("<verse name=\"" + item.name() + "\">\n");
        XML.append("<lines>" + item.text().replace("\n", "<br/>") + "</lines>\n</verse>\n");
    }
    // end file
    XML.append("</lyrics>\n</song>");
    return XML;
}

/** Write song language text to XML string
 * @param lng - language code
 * @return song XML
*/
QString Song::wrtieLngToXML(QString lng) {
    // build file frame
    QString XML = "<?xml version='1.0' encoding='UTF-8'?>\n<song"
        " xmlns=\"http://openlyrics.info/namespace/2009/song\" version=\"0.8\" "
        "createdIn=\"LyricsProjector\">\n<properties>\n<titles>\n<title>";
    XML.append(title());
    XML.append("</title>\n</titles>\n<authors><author>.</author></authors>\n</properties>\n<lyrics>\n");

    // print all items
    foreach(SongItem item, items) {
        XML.append("<verse name=\"" + item.name() + "\">\n");
        XML.append("<lines>" + item.textLng(lng).replace("\n", "<br/>") + "</lines>\n</verse>\n");
    }
    // end file
    XML.append("</lyrics>\n</song>");
    return XML;
}

/** Search song for text
 * @param text - text to search
 * @return true if found
*/
bool Song::search(QString text) {
    if (text.length() < 4) return true;
    if (title_.contains(text, Qt::CaseInsensitive)) return true;

    foreach(SongItem item, items) {
        if (item.searchText().contains(text.remove(" "), Qt::CaseInsensitive)) return true;
    }

    return false;
}

/** Find and replace text
 * @param find - text to find
 * @param replace - text to replace
 * @param searchText - true if text should be searched
 * @param searchTitle - true if title should be searched
 * @param searchItemName - true if item name should be searched
 * @return number of replacements
*/
int Song::findAndReplace(QString find, QString replace, bool searchText, bool searchTitle, bool searchItemName) {
    if (find == "") return 0;

    int replacements = 0;

    if (searchTitle) {
        replacements += title_.count(find);
        title_.replace(find, replace);
    }

    if (searchItemName) {
        for (int i = 0;i < items.size();i++) {
            replacements += items.at(i).name().count(find);
            items[i].setName(items.at(i).name().replace(find, replace));
        }
    }

    if (searchText) {
        for (int i = 0;i < items.size();i++) {
            replacements += items.at(i).text().count(find);
            items[i].setText(items.at(i).text().replace(find, replace));
        }
    }

    return replacements;
}

/** Evaluate song
 * @return true if song has warnings
*/
bool Song::evaluate() {
    hasWarning_ = false;

    foreach(SongItem item, items) {
        //check for warnings
        if (item.text().count("\n") >= 3) {
            hasWarning_ = true;
            warningMessage_ = "Niektóre slajdy mają więcej niż 3 wersy.";
            return true;
        }
    }

    return false;
}
