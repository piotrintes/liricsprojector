/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef THEME_H
#define THEME_H

#include <QColor>
#include <QFile>
#include <QFont>

/** This class represents theme for the output window */
class Theme : public QObject {
    Q_OBJECT
private:
    QString name_;                                          // theme name (for when you can have multiple themes, if that frature is ever needed)

    QFont Font_;                                            // font
    int textSize_;                                          // text size (‰ of screen height)
    QColor textColor_;                                      // text color
    QColor bcgColor_;                                       // background color
    QString bcgFilename_;                                   // filename for background (either image or video)
    QString bcgType_;                                       // background type (color, image or video)
    bool textShadow_;                                       // draw text shadow
    int textTop_ = 50;                                      // text top
    int secondaryTextTop_ = 80;                             // secondary text top
    QString secondaryLang_ = "";                            // secondary language

    int bibleTextSize_;                                     // text size for bible (‰ of screen height)
    QColor bibleTextColor_;                                 // text color for bible
    QColor bibleBcgColor_;                                  // background color for bible
    QList<int> bibleMargins_ = { 10,10,10,10 };             // margins for bible (left,right,top,bottom)

public:
    Theme();
    Theme(Theme* theme);
    void setTheme(Theme* theme);

    void loadTheme(QFile* file);
    void saveTheme(QFile* file);

    QFont Font() const;
    void setFont(const QFont& Font);
    int textSize() const;
    void setTextSize(int textSize);
    QColor textColor() const;
    void setTextColor(const QColor& textColor);
    QColor bcgColor() const;
    void setBcgColor(const QColor& bcgColor);
    QString bcgFilename() const;
    void setBcgFilename(const QString& bcgFilename);
    QString bcgType() const;
    void setBcgType(const QString& bcgType);
    QString name() const;
    void setName(const QString& name);
    int textTop() const;
    void setTextTop(int textTop);
    int secondaryTextTop() const;
    void setSecondaryTextTop(int secondaryTextTop);
    QString secondaryLang() const;
    void setSecondaryLang(const QString& secondaryLang);


    QList<int> bibleMargins() const;
    void setBibleMargins(const QList<int>& bibleMargins);
    QColor bibleBcgColor() const;
    void setBibleBcgColor(const QColor& bibleBcgColor);
    QColor bibleTextColor() const;
    void setBibleTextColor(const QColor& bibleTextColor);
    int bibleTextSize() const;
    void setBibleTextSize(int bibleTextSize);

    bool textShadow() const;
    void setTextShadow(bool newTextShadow);

signals:
    void updated();                                         // emited when theme is modified
};

#endif // THEME_H
