/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef SOURCESMANAGER_H
#define SOURCESMANAGER_H

#include <QMainWindow>
#include <QStringList>

namespace Ui {
    class SourcesManager;
}

/** This class is a source editor window. Sources are the list of directories from which additional materials will be loaded */
class SourcesManager : public QMainWindow {
    Q_OBJECT
private:
    QList<QString>* sourcesList;                    // sources list

public:
    explicit SourcesManager(QList<QString>* sourcesList, QWidget* parent = nullptr);
    ~SourcesManager();

private slots:
    void on_addButton_clicked();
    void on_pushButton_clicked();
    void on_removeButton_clicked();
    void on_saveButton_clicked();

private:
    Ui::SourcesManager* ui;

signals:
    void accepted();                                    // signal emited when user saves changes
};

#endif // SOURCESMANAGER_H
