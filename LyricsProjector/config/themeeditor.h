/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef THEMEEDITOR_H
#define THEMEEDITOR_H

#include "theme.h"

#include <QMainWindow>

/** This class is a song editor window. */
namespace Ui {
    class ThemeEditor;
}

class ThemeEditor : public QMainWindow {
    Q_OBJECT
private:
    Theme* initTheme;                                   // initial theme (used to revert changes)
    Theme* theme;                                       // actual theme

public:
    explicit ThemeEditor(Theme* theme, QWidget* parent = nullptr);
    ~ThemeEditor();

private slots:
    void on_ApplyButton_clicked();
    void on_pushButton_clicked();

    void on_fontButton_clicked();
    void on_TextSizeSpinBox_valueChanged(int arg1);
    void on_bcgColorButton_clicked();
    void on_textColorButton_clicked();
    void on_TextSizeSpinBox_2_valueChanged(int arg1);
    void on_bcgColorButton_2_clicked();
    void on_textColorButton_2_clicked();
    void on_Margin0SpinBox_valueChanged(int arg1);
    void on_Margin1SpinBox_valueChanged(int arg1);
    void on_Margin2SpinBox_valueChanged(int arg1);
    void on_Margin3SpinBox_valueChanged(int arg1);

    void on_textShadowCheckBox_toggled(bool checked);

private:
    Ui::ThemeEditor* ui;
};

#endif // THEMEEDITOR_H
