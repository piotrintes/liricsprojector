/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "config.h"

/** Get list of images to add to autostart */
QList<QString>* Config::autoAddImages() const {
    return autoAddImages_;
}

/** Get list of pdfs to add to autostart */
QList<QString>* Config::autoAddPDFs() const {
    return autoAddPDFs_;
}

/** Get list of videos to add to autostart */
QList<QString>* Config::autoAddVideos() const {
    return autoAddVedeos_;
}

/** Get list of source directorys for autoload */
QList<QString>* Config::autoloadDirs() const {
    return autoloadDirs_;
}

/** Get output screen index (-1 means output is in window) */
int Config::outScreenIndex() const {
    return outScreenIndex_;
}

/** Set output screen index
 * @param outScreenIndex output screen index (-1 means output is in window)
 */
void Config::setOutScreenIndex(int outScreenIndex) {
    outScreenIndex_ = outScreenIndex;
}

/** Get should autoloaded images be added to plan */
bool Config::autoAddSrcImages() const {
    return autoAddSrcImages_;
}

/** Set should autoloaded images be added to plan
 * @param autoAddSrcImages if true, autoloaded images will be added to plan
 */
void Config::setAutoAddSrcImages(bool autoAddSrcImages) {
    autoAddSrcImages_ = autoAddSrcImages;
}

/** Get should autoloaded pdfs be added to plan */
bool Config::autoAddSrcPDFs() const {
    return autoAddSrcPDFs_;
}

/** Set should autoloaded pdfs be added to plan
 * @param autoAddSrcPDFs if true, autoloaded pdfs will be added to plan
 */
void Config::setAutoAddSrcPDFs(bool autoAddSrcPDFs) {
    autoAddSrcPDFs_ = autoAddSrcPDFs;
}

/** Get should autoloaded videos be added to plan */
bool Config::autoAddSrcVideos() const {
    return autoAddSrcVideos_;
}

/** Set should autoloaded videos be added to plan
 * @param autoAddSrcVideos if true, autoloaded videos will be added to plan
 */
void Config::setAutoAddSrcVideos(bool autoAddSrcVideos) {
    autoAddSrcVideos_ = autoAddSrcVideos;
}

Config::Config() {}
