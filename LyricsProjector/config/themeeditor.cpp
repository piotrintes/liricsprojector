/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "themeeditor.h"
#include "ui_themeeditor.h"

#include <QColorDialog>
#include <QDir>
#include <QFile>
#include <QFontDialog>
#include <QDebug>

/**
 * @parm theme Theme to edit
 * @param parent Parent widget (inherited from QMainWindow, can be nullptr)
 */
ThemeEditor::ThemeEditor(Theme* theme, QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::ThemeEditor) {
    // setup theme, initTheme as theme's copy and fill up the form
    this->theme = theme;
    initTheme = new Theme(theme);
    ui->setupUi(this);
    ui->TextSizeSpinBox->setValue(theme->textSize());
    ui->TextSizeSpinBox_2->setValue(theme->bibleTextSize());
    ui->Margin0SpinBox->setValue(theme->bibleMargins().at(0));
    ui->Margin1SpinBox->setValue(theme->bibleMargins().at(1));
    ui->Margin2SpinBox->setValue(theme->bibleMargins().at(2));
    ui->Margin3SpinBox->setValue(theme->bibleMargins().at(3));
    ui->textShadowCheckBox->setChecked(theme->textShadow());
}

ThemeEditor::~ThemeEditor() {
    delete initTheme;
    delete ui;
}

/** Apply button clicked: saves theme and closes dialog */
void ThemeEditor::on_ApplyButton_clicked() {
    QFile file(QDir::homePath() + "/.LyricsProjector/theme.cfg");
    theme->saveTheme(&file);
    delete this;
}

/** Text size changed: update theme */
void ThemeEditor::on_TextSizeSpinBox_valueChanged(int arg1) {
    theme->setTextSize(arg1);
}

/** Revert button clicked: revert to initial theme and reset form */
void ThemeEditor::on_pushButton_clicked() {
    // revert
    theme->setTheme(initTheme);
    ui->TextSizeSpinBox->setValue(theme->textSize());
    ui->TextSizeSpinBox_2->setValue(theme->bibleTextSize());
    ui->Margin0SpinBox->setValue(theme->bibleMargins().at(0));
    ui->Margin1SpinBox->setValue(theme->bibleMargins().at(1));
    ui->Margin2SpinBox->setValue(theme->bibleMargins().at(2));
    ui->Margin3SpinBox->setValue(theme->bibleMargins().at(3));
    ui->textShadowCheckBox->setChecked(theme->textShadow());
}

/** Bcg color button clicked: open color dialog */
void ThemeEditor::on_bcgColorButton_clicked() {
    theme->setBcgColor(QColorDialog::getColor(theme->bcgColor(), this, "Ustaw kolor tła"));
}

/** Text color button clicked: open color dialog */
void ThemeEditor::on_textColorButton_clicked() {
    theme->setTextColor(QColorDialog::getColor(theme->textColor(), this, "Ustaw kolor tekstu"));
}

/** Bible text size changed: update theme */
void ThemeEditor::on_TextSizeSpinBox_2_valueChanged(int arg1) {
    theme->setBibleTextSize(arg1);
}

/** Bible bg color button clicked: open color dialog */
void ThemeEditor::on_bcgColorButton_2_clicked() {
    theme->setBibleBcgColor(QColorDialog::getColor(theme->bibleBcgColor(), this, "Ustaw kolor tła"));
}

/** Bible text color button clicked: open color dialog */
void ThemeEditor::on_textColorButton_2_clicked() {
    theme->setBibleTextColor(QColorDialog::getColor(theme->bibleTextColor(), this, "Ustaw kolor tekstu"));
}

/** Font button clicked: open font dialog, update theme when done */
void ThemeEditor::on_fontButton_clicked() {
    theme->setFont(QFontDialog::getFont(nullptr, theme->Font()));
}

/** Bible margins changed: update theme */
void ThemeEditor::on_Margin0SpinBox_valueChanged(int arg1) {
    QList<int> m = theme->bibleMargins();
    m[0] = arg1;
    theme->setBibleMargins(m);
}

/** Bible margins changed: update theme */
void ThemeEditor::on_Margin1SpinBox_valueChanged(int arg1) {
    QList<int> m = theme->bibleMargins();
    m[1] = arg1;
    theme->setBibleMargins(m);
}

/** Bible margins changed: update theme */
void ThemeEditor::on_Margin2SpinBox_valueChanged(int arg1) {
    QList<int> m = theme->bibleMargins();
    m[2] = arg1;
    theme->setBibleMargins(m);
}

/** Bible margins changed: update theme */
void ThemeEditor::on_Margin3SpinBox_valueChanged(int arg1) {
    QList<int> m = theme->bibleMargins();
    m[3] = arg1;
    theme->setBibleMargins(m);
}

/** Shadow checkbox clicked: update theme */
void ThemeEditor::on_textShadowCheckBox_toggled(bool checked) {
    theme->setTextShadow(checked);
}

