/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "sourcesmanager.h"
#include "ui_sourcesmanager.h"

#include <QFileDialog>
#include <QListWidgetItem>
#include <QDebug>
#include <QMessageBox>

/**
 * @param sourcesList list of sources from which additional materials will be loaded
 * @param parent parent window (inherited from QMainWindow, can be nullptr)
*/
SourcesManager::SourcesManager(QList<QString>* sourcesList, QWidget* parent) : QMainWindow(parent), ui(new Ui::SourcesManager) {
    ui->setupUi(this);

    // setup icons (Windows and Mac, Linux uses icon theme)
#ifndef Q_OS_LINUX
    QPixmap addIcon(QApplication::applicationDirPath() + "/icons/list-add.png");
    QPixmap removeIcon(QApplication::applicationDirPath() + "/icons/edit-delete.png");
    ui->addButton->setIcon(QIcon(addIcon.scaledToWidth(32)));
    ui->removeButton->setIcon(QIcon(removeIcon.scaledToWidth(32)));
#endif

    // create item list from source list
    foreach(QString str, *sourcesList)
        if (str != "") ui->listWidget->addItem(str);

    this->sourcesList = sourcesList;
}

SourcesManager::~SourcesManager() {
    delete ui;
}

/** Add button clicked: opens dialog to let the user add directory to the list */
void SourcesManager::on_addButton_clicked() {
    // add source to list
    QString dir = QFileDialog::getExistingDirectory(nullptr, "Wybierz katalog");
    if (dir == "") return;
    ui->listWidget->addItem(dir + "/");
}

/** Cancel button clicked: closes dialog without doing anything */
void SourcesManager::on_pushButton_clicked() {
    delete this;
}

/** Remove button clicked: removes selected item from the list */
void SourcesManager::on_removeButton_clicked() {
    // remove source from list
    if (ui->listWidget->currentItem() != nullptr) delete ui->listWidget->currentItem();
}

/** Save button clicked: saves sources list, emits accepted event and closes dialog */
void SourcesManager::on_saveButton_clicked() {
    // clear and recreate sources list, then emit accepted signal and display information for user, then delete itself
    sourcesList->clear();
    for (int i = 0; i < ui->listWidget->count();i++)
        sourcesList->append(ui->listWidget->item(i)->data(0).toString());

    emit accepted();

    QMessageBox::information(nullptr, "Źródła danych zostały zautualizowane", "Zmiany zostaną wprowadzone po ponownym uruchomieniu programu.");

    delete this;
}
