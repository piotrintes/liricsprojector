/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "config/theme.h"

#include <QTextStream>
#include <QDebug>
#include "theme.h"

/** Gets the margins for the Bible in the form of a list (top, right, bottom, left). */
QList<int> Theme::bibleMargins() const {
    return bibleMargins_;
}

/** Sets the margins for the Bible
 * @param bibleMargins margins in the form of a list (top, right, bottom, left).
 */
void Theme::setBibleMargins(const QList<int>& bibleMargins) {
    if (bibleMargins.length() != 4) {
        qWarning() << "Invalid Bible margins: " << bibleMargins;
        return;
    }
    bibleMargins_ = bibleMargins;
    emit updated();
}

/** Gets the Bible background color */
QColor Theme::bibleBcgColor() const {
    return bibleBcgColor_;
}

/** Sets the Bible background color
 * @param bibleBcgColor Bible background color
 */
void Theme::setBibleBcgColor(const QColor& bibleBcgColor) {
    bibleBcgColor_ = bibleBcgColor;
    emit updated();
}

/** Gets the Bible text color */
QColor Theme::bibleTextColor() const {
    return bibleTextColor_;
}

/** Sets the Bible text color
 * @param bibleTextColor Bible text color
 */
void Theme::setBibleTextColor(const QColor& bibleTextColor) {
    bibleTextColor_ = bibleTextColor;
    emit updated();
}

/** Gets the Bible text size */
int Theme::bibleTextSize() const {
    return bibleTextSize_;
}

/** Sets the Bible text size
 * @param bibleTextSize Bible text size
 */
void Theme::setBibleTextSize(int bibleTextSize) {
    bibleTextSize_ = bibleTextSize;
    emit updated();
}

/** Gets the Text shadow (true or false) */
bool Theme::textShadow() const {
    return textShadow_;
}

/** Sets the Text shadow
 * @param newTextShadow If true, text will be drawn with a shadow
 */
void Theme::setTextShadow(bool newTextShadow) {
    textShadow_ = newTextShadow;
    emit updated();
}

Theme::Theme() {}

/** Gets the font */
QFont Theme::Font() const {
    return Font_;
}

/** Sets the font
 * @param Font Font
 */
void Theme::setFont(const QFont& Font) {
    Font_ = Font;
    emit updated();
}

/** Gets the text size */
int Theme::textSize() const {
    return textSize_;
}

/** Sets the text size
 * @param textSize Text size (‰ of screen height)
 */
void Theme::setTextSize(int textSize) {
    textSize_ = textSize;
    emit updated();
}

/** Gets the text color */
QColor Theme::textColor() const {
    return textColor_;
}

/** Sets the text color
 * @param textColor Text color
 */
void Theme::setTextColor(const QColor& textColor) {
    textColor_ = textColor;
    emit updated();
}

/** Gets the background color */
QColor Theme::bcgColor() const {
    return bcgColor_;
}

/** Sets the background color
 * @param bcgColor Background color
 */
void Theme::setBcgColor(const QColor& bcgColor) {
    bcgColor_ = bcgColor;
    emit updated();
}

/** Gets the background filename */
QString Theme::bcgFilename() const {
    return bcgFilename_;
}

/** Sets the background filename
 * @param bcgFilename Background filename
 */
void Theme::setBcgFilename(const QString& bcgFilename) {
    bcgFilename_ = bcgFilename;
    emit updated();
}

/** Gets the background type */
QString Theme::bcgType() const {
    return bcgType_;
}

/** Sets the background type
 * @param bcgType Background type
 */
void Theme::setBcgType(const QString& bcgType) {
    bcgType_ = bcgType;
    emit updated();
}

/** Gets the theme name */
QString Theme::name() const {
    return name_;
}

/** Sets the theme name
 * @param name Theme name
 */
void Theme::setName(const QString& name) {
    name_ = name;
}

int Theme::textTop() const {
    return textTop_;
}

void Theme::setTextTop(int textTop) {
    textTop_ = textTop;
    emit updated();
}

int Theme::secondaryTextTop() const {
    return secondaryTextTop_;
}

void Theme::setSecondaryTextTop(int secondaryTextTop) {
    secondaryTextTop_ = secondaryTextTop;
    emit updated();
}

QString Theme::secondaryLang() const {
    return secondaryLang_;
}

void Theme::setSecondaryLang(const QString& secondaryLang) {
    secondaryLang_ = secondaryLang;
    emit updated();
}

/** Constructor that copies a theme
 * @param theme Theme
 */
Theme::Theme(Theme* theme) {
    setTheme(theme);
}

/** Sets the theme
 * @param theme Theme
 */
void Theme::setTheme(Theme* theme) {
    name_ = theme->name();

    Font_ = theme->Font();
    textSize_ = theme->textSize();
    textColor_ = theme->textColor();
    textTop_ = theme->textTop();
    secondaryTextTop_ = theme->secondaryTextTop();
    secondaryLang_ = theme->secondaryLang();
    bcgColor_ = theme->bcgColor();
    bcgFilename_ = theme->bcgFilename();
    bcgType_ = theme->bcgType();
    bibleTextSize_ = theme->bibleTextSize();
    bibleTextColor_ = theme->bibleTextColor();
    bibleBcgColor_ = theme->bibleBcgColor();
    bibleMargins_ = theme->bibleMargins();
    textShadow_ = theme->textShadow();
    emit updated();
}

/** Loads a theme from a file
 * @param file File
 */
void Theme::loadTheme(QFile* file) {
    QString cfg;
    if (file->open(QFile::ReadOnly | QFile::Text)) {
        QTextStream in(file);
        cfg = in.readAll();
    } else {
        return;
    }

    QStringList lines = cfg.split("\n");

    foreach(QString line, lines) {
        if (line != "") {
            QString var = line.split(":").at(0);
            QString val = line.split(":").at(1);
            if (var == "bcgColor") setBcgColor(QColor(val));
            else if (var == "bcgFilename") setBcgFilename(val);
            else if (var == "bcgType") setBcgType(val);
            else if (var == "textColor") setTextColor(QColor(val));
            else if (var == "textSize") setTextSize(val.toInt());
            else if (var == "textFont") {
                QFont font;
                font.fromString(val);
                setFont(font);
            } else if (var == "textShadow") setTextShadow(val.toInt());
            else if (var == "textTop") setTextTop(val.toInt());
            else if (var == "secondaryTextTop") setSecondaryTextTop(val.toInt());
            else if (var == "secondaryLang") setSecondaryLang(val);
            else if (var == "bibleBcgColor") setBibleBcgColor(QColor(val));
            else if (var == "bibleTextColor") setBibleTextColor(QColor(val));
            else if (var == "bibleTextSize") setBibleTextSize(val.toInt());
            else if (var == "bibleMarginsLeft") bibleMargins_[0] = val.toInt();
            else if (var == "bibleMarginsRight") bibleMargins_[1] = val.toInt();
            else if (var == "bibleMarginsTop") bibleMargins_[2] = val.toInt();
            else if (var == "bibleMarginsBottom") bibleMargins_[3] = val.toInt();
        }
    }
}

/** Saves the theme to a file
 * @param file File
 */
void Theme::saveTheme(QFile* file) {
    if (file->exists()) file->remove();

    if (file->open(QIODevice::WriteOnly)) {
        QTextStream out(file);
        out << "bcgColor:" << bcgColor().name() << "\n";
        out << "bcgFilename:" << bcgFilename() << "\n";
        out << "bcgType:" << bcgType() << "\n";
        out << "textColor:" << textColor().name() << "\n";
        out << "textShadow:" << int(textShadow()) << "\n";
        out << "textSize:" << textSize() << "\n";
        out << "textFont:" << Font().toString() << "\n";
        out << "textTop:" << textTop() << "\n";
        out << "secondaryTextTop:" << secondaryTextTop() << "\n";
        out << "secondaryLang:" << secondaryLang() << "\n";
        out << "bibleBcgColor:" << bibleBcgColor().name() << "\n";
        out << "bibleTextColor:" << bibleTextColor().name() << "\n";
        out << "bibleTextSize:" << bibleTextSize() << "\n";
        out << "bibleMarginsLeft:" << bibleMargins().at(0) << "\n";
        out << "bibleMarginsRight:" << bibleMargins().at(1) << "\n";
        out << "bibleMarginsTop:" << bibleMargins().at(2) << "\n";
        out << "bibleMarginsBottom:" << bibleMargins().at(3) << "\n";
        file->close();
    }
}
