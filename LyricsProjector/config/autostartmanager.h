/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef AUTOSTARTMANAGER_H
#define AUTOSTARTMANAGER_H

#include "config/config.h"
#include "library/pdf.h"

#include <QListWidgetItem>
#include <QMainWindow>

namespace Ui {
    class AutostartManager;
}

/** This class is a autostart configuration window */
class AutostartManager : public QMainWindow {
    Q_OBJECT
private:
    Config* cfg;                                                        // pointer to configuration

public:
    explicit AutostartManager(QList<QString> libraryImages, QList<Pdf*> libraryPDFs, QList<QString> libraryVideos,
        Config* cfg, QWidget* parent = nullptr);
    ~AutostartManager();

private slots:
    void on_saveButton_clicked();
    void on_EditSourcesButton_clicked();
    void on_pdfListWidget_itemChanged(QListWidgetItem* item);
    void on_imgListWidget_itemChanged(QListWidgetItem* item);
    void on_videoListWidget_itemChanged(QListWidgetItem* item);
    void on_addAutoPdf_toggled(bool checked);
    void on_addAutoImg_toggled(bool checked);
    void on_addAutoMov_toggled(bool checked);

private:
    Ui::AutostartManager* ui;

signals:
    void sourcesEditorRequested();                                      // Emited when user wants to edit sources
    void accepted();                                                    // Emited when user accepted changes
};

#endif // AUTOSTARTMANAGER_H
