/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "autostartmanager.h"
#include "ui_autostartmanager.h"


#include <QMessageBox>

/**
 * @param libraryImages list of images to be added to plan on startup
 * @param libraryPDFs list of pdfs to be added to plan on startup
 * @param libraryVideos list of videos to be added to plan on startup
 * @param cfg config object
 * @param parent parent window (inherited from QMainWindow, can be nullptr)
*/
AutostartManager::AutostartManager(QList<QString> libraryImages, QList<Pdf*> libraryPDFs, QList<QString> libraryVideos,
    Config* cfg, QWidget* parent) : QMainWindow(parent), ui(new Ui::AutostartManager) {

    ui->setupUi(this);

    this->cfg = cfg;

    // setup checkboxes
    ui->addAutoImg->setChecked(cfg->autoAddSrcImages());
    ui->addAutoPdf->setChecked(cfg->autoAddSrcPDFs());
    ui->addAutoMov->setChecked(cfg->autoAddSrcVideos());

    // setup library lists
    foreach(QString img, libraryImages) {
        QListWidgetItem* item = new QListWidgetItem(img);
        if (cfg->autoAddImages()->contains(img))
            item->setCheckState(Qt::Checked);
        else
            item->setCheckState(Qt::Unchecked);

        ui->imgListWidget->addItem(item);
    }

    foreach(Pdf * pdf, libraryPDFs) {
        QListWidgetItem* item = new QListWidgetItem(pdf->filename());
        if (cfg->autoAddPDFs()->contains(pdf->filename()))
            item->setCheckState(Qt::Checked);
        else
            item->setCheckState(Qt::Unchecked);

        ui->pdfListWidget->addItem(item);
    }

    foreach(QString vid, libraryVideos) {
        QListWidgetItem* item = new QListWidgetItem(vid);
        if (cfg->autoAddVideos()->contains(vid))
            item->setCheckState(Qt::Checked);
        else
            item->setCheckState(Qt::Unchecked);

        ui->videoListWidget->addItem(item);
    }
}

AutostartManager::~AutostartManager() {
    delete ui;
}

/** Save button: emits accepted event and closes dialog */
void AutostartManager::on_saveButton_clicked() {
    emit accepted();
    delete this;
}

/** Edit sources button: emits sourcesEditorRequested signal */
void AutostartManager::on_EditSourcesButton_clicked() {
    emit sourcesEditorRequested();
}

/** PDF list item changed: update config to add or remove pdf from autostart list */
void AutostartManager::on_pdfListWidget_itemChanged(QListWidgetItem* item) {
    if (item->checkState() == Qt::Checked)
        cfg->autoAddPDFs()->append(item->text());
    else
        cfg->autoAddPDFs()->removeAll(item->text());
}

/** Image list item changed: update config to add or remove image from autostart list */
void AutostartManager::on_imgListWidget_itemChanged(QListWidgetItem* item) {
    if (item->checkState() == Qt::Checked)
        cfg->autoAddImages()->append(item->text());
    else
        cfg->autoAddImages()->removeAll(item->text());
}

/** Video list item changed: update config to add or remove video from autostart list */
void AutostartManager::on_videoListWidget_itemChanged(QListWidgetItem* item) {
    if (item->checkState() == Qt::Checked)
        cfg->autoAddVideos()->append(item->text());
    else
        cfg->autoAddVideos()->removeAll(item->text());
}

/** Auto add PDFs checkbox changed: update config */
void AutostartManager::on_addAutoPdf_toggled(bool checked) {
    cfg->setAutoAddSrcPDFs(checked);
}

/** Auto add images checkbox changed: update config */
void AutostartManager::on_addAutoImg_toggled(bool checked) {
    cfg->setAutoAddSrcImages(checked);
}

/** Auto add videos checkbox changed: update config */
void AutostartManager::on_addAutoMov_toggled(bool checked) {
    cfg->setAutoAddSrcVideos(checked);
}
