/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H

#include <QList>
#include <QString>

/** Configuration class. */
class Config {
private:
    QList<QString>* autoAddImages_ = new QList<QString>();                      // list of images that should be added to plan on startup
    QList<QString>* autoAddPDFs_ = new QList<QString>();                        // list of PDFs that should be added to plan on startup
    QList<QString>* autoAddVedeos_ = new QList<QString>();                      // list of Videos that should be added to plan on startup
    QList<QString>* autoloadDirs_ = new QList<QString>();                       // list of source directorys for autoload
    int outScreenIndex_ = -1;                                                   // output screen index (-1 means output is in window)

    bool autoAddSrcImages_ = false;                                             // should autoloaded images be added to plan
    bool autoAddSrcPDFs_ = false;                                               // should autoloaded pdfs be added to plan
    bool autoAddSrcVideos_ = false;                                             // should autoloaded videos be added to plan

public:
    Config();
    QList<QString>* autoAddImages() const;
    QList<QString>* autoAddPDFs() const;
    QList<QString>* autoAddVideos() const;
    QList<QString>* autoloadDirs() const;
    int outScreenIndex() const;
    void setOutScreenIndex(int outScreenIndex);
    bool autoAddSrcImages() const;
    void setAutoAddSrcImages(bool autoAddSrcImages);
    bool autoAddSrcPDFs() const;
    void setAutoAddSrcPDFs(bool autoAddSrcPDFs);
    bool autoAddSrcVideos() const;
    void setAutoAddSrcVideos(bool autoAddSrcVideos);
};

#endif // CONFIG_H
