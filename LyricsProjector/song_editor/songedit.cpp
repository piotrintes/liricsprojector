#include "misc/consts.h"
#include "songedit.h"
#include <QPainter>
#include <QStringList>
#include <QScrollBar>
#include <QDebug>

SongEdit::SongEdit(QWidget *parent): QTextEdit(parent)
{
    //viewport()->setStyleSheet("padding-left:20px");

    /*QTextBlockFormat bf = this->textCursor().blockFormat();
    //bf.setLineHeight(lineSpacing, QTextBlockFormat::LineDistanceHeight) ;
    bf.setLeftMargin(10);
    this->textCursor().setBlockFormat(bf);*/
    document()->setDocumentMargin(22);

    //document()->contentsChanged()
}

void SongEdit::paintEvent(QPaintEvent *event)
{
    QTextEdit::paintEvent(event);

    // setup painter
    QPainter painter(viewport());
    painter.setRenderHint(QPainter::Antialiasing, true);

    QColor indicatorColor = QColor(0,0,0);

    int fontHeight = fontMetrics().height();

    int h1 = document()->documentMargin();
    int h2 = h1;
    int scroll = -this->verticalScrollBar()->value();
    bool isFirst = true;
    QString lastName;

    QStringList lines = toPlainText().split("\n");
    foreach (QString line, lines) {
        if(line.left(1) == "[" && line.right(1) == "]") {
            if(!isFirst){
                // draw separator
                painter.setRenderHint(QPainter::Antialiasing, false);
                painter.setPen(palette().dark().color());
                painter.drawLine(0,h2+scroll,width(),h2+scroll);
                painter.setRenderHint(QPainter::Antialiasing, true);
                // draw indicator
                painter.setPen(QPen(indicatorColor,8,Qt::SolidLine,Qt::RoundCap));
                painter.drawLine(9,h1+6+scroll,9,h2-6+scroll);
                // connect indicator if same as last block
                if(line == lastName) painter.drawLine(9,h2-6+scroll,9,h2+6+scroll);
            } else isFirst = false;
            h1 = h2;
            indicatorColor = indicator_colors::other_color;
            if(line.at(1).toLower() == "v" || line.at(1).toLower() == "z") indicatorColor = indicator_colors::verse_color;
            if(line.at(1).toLower() == "c" || line.at(1).toLower() == "r") indicatorColor = indicator_colors::chorus_color;
            if(line.at(1).toLower() == 'b') indicatorColor = indicator_colors::bridge_color;
            if(line.at(1).toLower() == 'p') indicatorColor = indicator_colors::prechorus_color;
            lastName = line;
        }
        h2 += fontHeight;
    }
    painter.setPen(QPen(indicatorColor,8,Qt::SolidLine,Qt::RoundCap));
    painter.drawLine(9,h1+6+scroll,9,h2-6+scroll);
}

void SongEdit::keyReleaseEvent(QKeyEvent *event)
{
    QTextEdit::keyReleaseEvent(event);
    viewport()->update();
}
void SongEdit::scrollContentsBy(int dx, int dy)
{
    QTextEdit::scrollContentsBy(dx, dy);
    viewport()->update();
}

