/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "songeditor.h"
#include "ui_songeditor.h"

#include <QMessageBox>
#include <QDebug>
#include <QException>
#include <QFile>
#include <QPainter>
#include <QInputDialog>
#include "misc/consts.h"

QIcon createDotIcon(QColor color)
{
    //create colored dot
    QPixmap px(16,16);
    px.fill(QColor(0,0,0,0));
    QPainter painter(&px);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBrush(color);
    painter.setPen(QPen(color, 4));
    painter.drawEllipse(3,4,8,8);
    QIcon icon(px);
    return icon;
}

SongEditor::SongEditor(Song *song, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SongEditor)
{
    ui->setupUi(this);

    this->song = song;

    ui->textEdit->setPlaceholderText("[Znacznik slajdu]\n tekst slajdu");

    // fill the form
    ui->titleEdit->setText(song->title());

    // add every song item as [name]\nText\n there will be edditional \n so it must be removed manually
    QString text;
    foreach (SongItem item, song->items) {
        text.append("[" + item.name() + "]\n" + item.text() + "\n");
    }

    // fill toolbar
#ifdef Q_OS_WIN
    ui->toolBar->layout()->setMargin(4);
#endif
    textList.insert("",text.left(text.size()-1));
    ui->textEdit->setPlainText(text.left(text.size()-1));
    ui->toolBar->addWidget(ui->toolBarElements);
    ui->toolBar->addWidget(ui->addElementButton);
    //ui->toolBar->addWidget(ui->autoformatButton);
    ui->autoformatButton->hide();

    // add translations
    ui->lngComboBox->addItem("Domyślny");

    foreach(QString lng, song->lngList()) {
        ui->lngComboBox->addItem(lng);
        QString text;
        foreach (SongItem item, song->items) {
            text.append("[" + item.name() + "]\n" + item.textLng(lng) + "\n");
        }
        textList.insert(lng,text.left(text.size()-1));
    }

    ui->lngComboBox->addItem("Dodaj");

#ifndef Q_OS_LINUX
    ui->addElementButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/list-add.png").scaled(32,32)));
#endif

    // create add menu
    QMenu *addMenu = new QMenu;
    QAction *addVerse = new QAction(createDotIcon(indicator_colors::verse_color),"Dodaj zwrotkę");
    QAction *addChorus = new QAction(createDotIcon(indicator_colors::chorus_color),"Dodaj refren");
    QAction *addBridge = new QAction(createDotIcon(indicator_colors::bridge_color),"Dodaj bridge");
    QAction *addPrechorus = new QAction(createDotIcon(indicator_colors::prechorus_color),"Dodaj przedrefren");
    QAction *addOther = new QAction(createDotIcon(indicator_colors::other_color),"Dodaj własny znacznik");

    addMenu->addAction(addVerse);
    addMenu->addAction(addChorus);
    addMenu->addAction(addBridge);
    addMenu->addAction(addPrechorus);
    addMenu->addAction(addOther);

    connect(addVerse,&QAction::triggered,this,[this] {
        if(ui->textEdit->textCursor().position() > 0)
            ui->textEdit->textCursor().insertText("\n[Z]\n");
        else
            ui->textEdit->textCursor().insertText("[Z]\n");
    });
    connect(addChorus,&QAction::triggered,this,[this] {
        if(ui->textEdit->textCursor().position() > 0)
            ui->textEdit->textCursor().insertText("\n[R]\n");
        else
            ui->textEdit->textCursor().insertText("[R]\n");
    });
    connect(addBridge,&QAction::triggered,this,[this] {
        if(ui->textEdit->textCursor().position() > 0)
            ui->textEdit->textCursor().insertText("\n[B]\n");
        else
            ui->textEdit->textCursor().insertText("[B]\n");
    });
    connect(addPrechorus,&QAction::triggered,this,[this] {
        if(ui->textEdit->textCursor().position() > 0)
            ui->textEdit->textCursor().insertText("\n[P]\n");
        else
            ui->textEdit->textCursor().insertText("[P]\n");
    });
    connect(addOther,&QAction::triggered,this,[this] {
        if(ui->textEdit->textCursor().position() > 0)
            ui->textEdit->textCursor().insertText("\n[?]\n");
        else
            ui->textEdit->textCursor().insertText("[?]\n");
    });

    ui->addElementButton->setMenu(addMenu);

    // format menu
    QMenu *formatMenu = new QMenu;
    QAction *formatRemoveWhiteChars = new QAction("Usuń puste znaki");
    formatMenu->addAction(formatRemoveWhiteChars);

    connect(formatRemoveWhiteChars,&QAction::triggered,this,[this] {
        QString text = ui->textEdit->toPlainText();
        text.replace("  ","").replace("\t","").replace("\n ","\n");
        while(text.contains("\n\n")) text.replace("\n\n","\n");
        ui->textEdit->setPlainText(text);
    });

    ui->autoformatButton->setMenu(formatMenu);
}

SongEditor::~SongEditor()
{
    delete ui;
}

void SongEditor::on_abortButton_clicked()
{
    delete this;
}

void SongEditor::on_saveButton_clicked()
{
    // save current text
    textList[currentLng] = ui->textEdit->toPlainText();

    // create nes list of song items
    QList<SongItem> newList;

    // check for some simple error that could cause crash
    if(textList[""].front() != '[') {
        QMessageBox::critical(this,"Edycja pieśni","We wprowadzonym tekście są błędy. Zmiany nie zostaną zapisane.\n\n"
                                                   "Brak pierwszego znacznika - tekst powinien zaczynać się od np. [Z1]");
        return;
    }

    // split text to single items name]\nText\n
    QStringList texts = textList[""].split("[");
    texts.removeFirst();

    foreach (QString str, texts) {
        // get item name
        QString name = str.split("]\n").at(0);
        // check for some simple error that could cause crash
        if(name == "]") {
            QMessageBox::critical(this,"Edycja pieśni","We wprowadzonym tekście są błędy. Zmiany nie zostaną zapisane.\n\n"
                                                       "Pusty znacznik - []");
            return;
        }
        if(str.split("]\n").size()<2) {
            QMessageBox::critical(this,"Edycja pieśni","We wprowadzonym tekście są błędy. Zmiany nie zostaną zapisane.\n\n"
                                                       "Nie znaleziono tekstu zwrotki [" + name + ".");
            return;
        }
        // get item text, remove additionall "\n" if one is found and create new item
        QString text = str.split("]\n").at(1);
        if(text.back() == '\n') text = text.left(text.size()-1);
        newList.append(SongItem(name,text));
    }


    // evaluate translations
    foreach(QString lng, song->lngList()) {
        if(textList[lng].front() != '[') {
            QMessageBox::critical(this,"Edycja pieśni","We wprowadzonym tekście (" + lng + ") są błędy. Zmiany nie zostaną zapisane.\n\n"
                                                       "Brak pierwszego znacznika - tekst powinien zaczynać się od np. [Z1]");
            return;
        }

        // split text to single items name]\nText\n
        QStringList texts = textList[lng].split("[");
        texts.removeFirst();

        int i = 0;
        foreach (QString str, texts) {
            // get item name
            QString name = str.split("]\n").at(0);
            // check for some simple error that could cause crash
            if(name == "]") {
                QMessageBox::critical(this,"Edycja pieśni","We wprowadzonym tekście (" + lng + ") są błędy. Zmiany nie zostaną zapisane.\n\n"
                                                           "Pusty znacznik - []");
                return;
            }
            if(str.split("]\n").size()<2) {
                QMessageBox::critical(this,"Edycja pieśni","We wprowadzonym tekście (" + lng + ") są błędy. Zmiany nie zostaną zapisane.\n\n"
                                                           "Nie znaleziono tekstu zwrotki [" + name + ".");
                return;
            }
            // get item text, remove additionall "\n" if one is found and create new item
            QString text = str.split("]\n").at(1);
            if(text.back() == '\n') text = text.left(text.size()-1);

            if(newList.size()<=i) {
                QMessageBox::critical(this,"Edycja pieśni","We wprowadzonym tekście (" + lng + ") są błędy. Zmiany nie zostaną zapisane.\n\n"
                                                           "Nieprawidłowa ilość elementów.");
                return;
            }
            newList[i].setTextLng(lng, text);
            i++;
        }
    }

    //overwrite song items list, evaluate song and set new title
    song->items = newList;
    song->evaluate();
    song->setTitle(ui->titleEdit->text());

    QFile file(song->filename());

    // save song file
    if(file.exists()) file.remove();

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out.setCodec("UTF-8");
        out.setAutoDetectUnicode(false);
        out << song->wrtieToXML();
        file.close();
    }

    // save translations
    foreach(QString lng, song->lngList()) {
        QFile file(song->filename() + ".lng." + lng);

        if(file.exists()) file.remove();

        if (file.open(QIODevice::WriteOnly)) {
            QTextStream out(&file);
            out.setCodec("UTF-8");
            out << song->wrtieLngToXML(lng);
            file.close();
        }
    }

    // emit accepted signal and delete editor (close window, and clean up memory)
    emit accepted();
    delete this;
}

void SongEditor::on_lngComboBox_currentIndexChanged(const QString &arg1)
{
    if(arg1 == currentLng) return;
    // save current text
    textList[currentLng] = ui->textEdit->toPlainText();
    if(arg1 == ui->lngComboBox->itemText(0)) {
        // no translation
        ui->textEdit->setText(textList.value(""));
        currentLng = "";
    } else if (arg1 == "Dodaj") {
        // add new translation
        QString newLng = QInputDialog::getText(this,"Dodaj język","Kod języka (np. en, pl, de):");
        if(newLng != "") {
            currentLng = newLng;
            song->addLng(newLng);
            textList.insert(newLng,textList[""]);
            ui->lngComboBox->addItem(newLng);
        }
        ui->lngComboBox->setCurrentText(currentLng);
    } else {
        // switch to translation
        ui->textEdit->setText(textList.value(arg1));
        currentLng = arg1;
    }
}
