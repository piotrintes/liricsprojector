/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef SONGEDITOR_H
#define SONGEDITOR_H

/****************************************************************************
 ** This class is a song editor window.
****************************************************************************/


#include "library/song.h"

#include <QMainWindow>

namespace Ui {
class SongEditor;
}

class SongEditor : public QMainWindow
{
    Q_OBJECT
private:
    Song *song;                                                     // song that is being edited
    bool textEditReady = true;

    QHash<QString, QString> textList;
    QString currentLng = "";

public:
    explicit SongEditor(Song *song, QWidget *parent = nullptr);     // constructor - requires song for editor
    ~SongEditor();

private slots:
    void on_abortButton_clicked();
    void on_saveButton_clicked();

    void on_lngComboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::SongEditor *ui;

signals:
    void accepted();                                                // emited when song user saves the song
};

#endif // SONGEDITOR_H
