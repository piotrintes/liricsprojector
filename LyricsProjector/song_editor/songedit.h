#ifndef SONGEDIT_H
#define SONGEDIT_H

#include <QTextEdit>

class SongEdit : public QTextEdit
{
    Q_OBJECT
public:
    SongEdit(QWidget* parent = Q_NULLPTR);

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    virtual void scrollContentsBy(int dx, int dy);
};

#endif // SONGEDIT_H
