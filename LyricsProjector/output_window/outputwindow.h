/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#ifndef OUTPUTWINDOW_H
#define OUTPUTWINDOW_H

 /****************************************************************************
  ** This class represents output window (full screen window)
  ****************************************************************************/

#include "config/theme.h"
#include "output_window/screenmanager.h"
#include "library/bcg.h"

#include <QGraphicsDropShadowEffect>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QWidget>

namespace Ui {
    class OutputWindow;
}

class OutputWindow : public QWidget {
    Q_OBJECT
private:
    QWidget* lastActiveTab;          // last active tab (used when returning from black/empty screen
    Theme* theme_;                   // theme (used to setup colors and text style

    ScreenManager scrM;
    QMediaPlayer* player = new QMediaPlayer;               // media player
    QVideoWidget* videoWidget = new QVideoWidget;          // video widget (canvas for media player)
    QString currentVideo;
    QPixmap videoFrame;

    bool usePreview_ = true;

public:
    explicit OutputWindow(QWidget* parent = nullptr);
    ~OutputWindow();

    void setToScreen(QScreen* screen);          // set window to screen (if nullptr is provided display as window)

    void restore();                          // restore from black/empty
    void setBlack();                         // set to black screen
    void setEmpty();                         // set to empty screen
    void videoPlayStop();                    // play/stop video playback
    void setVideoPos(int position);          // set video position

    void displaySong(SongItem item);               // display song slide
    void displayBible(QString text);              // display bible slide
    void displayImage(QPixmap* image);            // display image
    void displayVideo(QString filename);          // display video

    QPixmap preview();          // get preview image

    void setTheme(Theme* theme);          // set theme_
    Theme* theme() const;                 // get theme_

    void setBackground(Bcg* bcg);         // set background

    bool videoPlaying() const;

    void setUsePreview(bool newUsePreview);
    bool usePreview() const;

protected:
    virtual void resizeEvent(QResizeEvent* event);

public slots:
    void updateTheme();          // update theme

signals:
    void playerUpdate(qint64 position, qint64 duration);

private:
    Ui::OutputWindow* ui;

    void closeEvent(QCloseEvent* event);
};

#endif          // OUTPUTWINDOW_H
