#ifndef SCREENMANAGER_H
#define SCREENMANAGER_H

#include "qmediaplaylist.h"
#include "../library/song.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsVideoItem>
#include <QGraphicsView>
#include <QMediaPlayer>
#include <QScreen>
#include <QTime>

class ScreenManager : public QObject {
    Q_OBJECT
private:
    QGraphicsScene* scene = new QGraphicsScene();
    QGraphicsView* graphicsView = new QGraphicsView(scene);
    QMediaPlayer* player = new QMediaPlayer;
    QMediaPlaylist bcgPlayList;

    int width = QApplication::primaryScreen()->size().width();
    int height = QApplication::primaryScreen()->size().height();
    int transition = 500;

    QColor songTextColor;
    QFont songTextFont;
    int songTop = 50;
    QString secondaryLang = "";
    int secondaryTop = 75;

    QString text = "tekst";
    QString textSecondary = "";
    QString lastScene = "";
    bool isEmpty = false;
    bool isBlack = false;
    QString backgroundVideo = QApplication::applicationDirPath() + "/mbcg/default.mp4";
    QString backgroundImage = QApplication::applicationDirPath() + "/mbcg/default.jpg";
    QString bcgType = "video";
    QString currentVideo = "";

    QGraphicsRectItem* bcgBlack = new QGraphicsRectItem();
    QGraphicsRectItem* bcgColor = new QGraphicsRectItem();
    QGraphicsPixmapItem* bcgImage = new QGraphicsPixmapItem();
    QGraphicsVideoItem* videoItem = new QGraphicsVideoItem();
    QGraphicsRectItem* blackRect = new QGraphicsRectItem();
    QGraphicsPixmapItem* songText = new QGraphicsPixmapItem();
    QGraphicsPixmapItem* songTextSecondary = new QGraphicsPixmapItem();
    QGraphicsRectItem* imageBcg = new QGraphicsRectItem();
    QGraphicsPixmapItem* image = new QGraphicsPixmapItem();

    void renderSongText(QGraphicsPixmapItem* songText, QString text, int top);
    void renderSongText();
    void fadeToBlack();
    void fadeFromBlack();
    bool compareColors(QColor color1, QColor color2);
    void videoEnd();
    void playBackgroundVideo();

public:
    ScreenManager();
    QGraphicsView* getGraphicsView() const;

    void setupSongFont(QColor color, QFont font, int size, int top, int secondaryTop);
    void setupBcgColor(QColor color);
    void setupSecondaryLang(QString lng);
    void resize(int w, int h);

    void setBackgroundVideo(QString video);
    void setBackgroundImage(QString image);
    void setBackgroundType(QString type);

    void black();
    void restore();
    void empty();
    void setSong();
    void setSongText(SongItem item);
    void setImage();
    void setImagePixmap(QPixmap px);
    void playVideo(QString filename, int position = 0);
    void stopVideo();
    void toggleVideo();
    QMediaPlayer* getPlayer();
};

#endif          // SCREENMANAGER_H
