/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "outputwindow.h"
#include "ui_outputwindow.h"

#include <QDebug>
#include <QFileInfo>
#include <QMessageBox>
#include <QMouseEvent>
#include <QScreen>
#include <QThread>
#include <QVideoProbe>
#include <QWidget>
#include <QtGlobal>

#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
extern QImage qt_imageFromVideoFrame(const QVideoFrame& f);
#endif

Theme* OutputWindow::theme() const {
    return theme_;
}

bool OutputWindow::videoPlaying() const {
    return (player->state() == QMediaPlayer::State::PlayingState);
}

void OutputWindow::setUsePreview(bool newUsePreview) {
    usePreview_ = newUsePreview;
}

bool OutputWindow::usePreview() const {
    return usePreview_;
}

OutputWindow::OutputWindow(QWidget* parent) : QWidget(parent),
ui(new Ui::OutputWindow) {
    ui->setupUi(this);
    // hide taskbar icon and setup lastActiveTab
    setWindowFlags(Qt::Tool);
    lastActiveTab = ui->stackedWidget->currentWidget();

    // add screen manager to the window
    ui->stackedWidget->addWidget(scrM.getGraphicsView());
    ui->stackedWidget->setCurrentWidget(scrM.getGraphicsView());

    // setup media player
    player->setVideoOutput(videoWidget);
    ui->video->layout()->addWidget(videoWidget);

    // this is mechanic essential for video preview
    QVideoProbe* videoProbe = new QVideoProbe(this);

#ifndef Q_OS_LINUX
    connect(player, &QMediaPlayer::positionChanged, [this](qint64 position) {
        emit playerUpdate(position / 1000, player->duration() / 1000);
    });
#endif

    if (videoProbe->setSource(scrM.getPlayer())) {
        QThread* thread = new QThread();
        videoProbe->moveToThread(thread);
        thread->start();

        connect(videoProbe, &QVideoProbe::videoFrameProbed, [this](QVideoFrame frame) {
            if (usePreview_) {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
                QImage imgbuf = qt_imageFromVideoFrame(frame);
#else
                QImage imgbuf = frame.image();
#endif
                videoFrame = QPixmap::fromImage(imgbuf);
            }
#ifdef Q_OS_LINUX
            emit playerUpdate(scrM.getPlayer()->position() / 1000, scrM.getPlayer()->duration() / 1000);
#endif
        });
    }
}

void OutputWindow::setTheme(Theme* theme) {
    theme_ = theme;
    updateTheme();
}

void OutputWindow::updateTheme() {
    // update color palette
    QPalette pal;
    pal.setColor(QPalette::Window, theme_->bcgColor());
    pal.setColor(QPalette::WindowText, theme_->textColor());

    setPalette(pal);

    scrM.setupBcgColor(theme_->bcgColor());

    // update background
    if (theme_->bcgType() == "color" || theme_->bcgType() == "") {
        scrM.setBackgroundType("color");
    } else if (theme_->bcgType() == "image") {
        scrM.setBackgroundType("image");
        scrM.setBackgroundImage(theme_->bcgFilename());
    } else if (theme_->bcgType() == "video") {
        scrM.setBackgroundType("video");
        scrM.setBackgroundVideo(theme_->bcgFilename());
    }

    // update font
    QFont f(theme_->Font());
    f.setPixelSize(height() * theme_->textSize() / 1000);
    ui->SlideText->setFont(f);

    if (theme_->textShadow()) {
        if (ui->SlideText->graphicsEffect() == nullptr) {
            // setup text shadow effect
            QGraphicsDropShadowEffect* sh = new QGraphicsDropShadowEffect();
            sh->setColor(QColor(0, 0, 0, 150));
            sh->setOffset(0, 1);
            sh->setBlurRadius(20);
            ui->SlideText->setGraphicsEffect(sh);
        }
    } else
        ui->SlideText->setGraphicsEffect(nullptr);

    // update bible color palette
    pal.setColor(QPalette::Window, theme_->bibleBcgColor());
    pal.setColor(QPalette::WindowText, theme_->bibleTextColor());

    // update bible font
    ui->bible->setPalette(pal);
    f.setPixelSize(height() * theme_->bibleTextSize() / 1000);
    ui->BibleSlideText->setFont(f);

    // update bible margins
    ui->bible->setContentsMargins(width() * theme_->bibleMargins().at(0) / 100,
        height() * theme_->bibleMargins().at(2) / 100,
        width() * theme_->bibleMargins().at(1) / 100,
        height() * theme_->bibleMargins().at(3) / 100);

    scrM.setupSongFont(
        theme_->textColor(),
        theme_->Font(),
        height() * theme_->textSize() / 1000,
        theme_->textTop(),
        theme_->secondaryTextTop()
    );

    scrM.setupSecondaryLang(theme_->secondaryLang());
}

void OutputWindow::setBackground(Bcg* bcg) {
    if (bcg->type() == "color") {
        scrM.setupBcgColor(bcg->color());
        scrM.setBackgroundType("color");
    } else if (bcg->type() == "image") {
        scrM.setBackgroundImage(bcg->image());
        scrM.setBackgroundType("image");
    } else if (bcg->type() == "video") {
        scrM.setBackgroundVideo(bcg->video());
        scrM.setBackgroundType("video");
    }
}

void OutputWindow::closeEvent(QCloseEvent* event) {
    event->ignore();

    if (QMessageBox::question(this, "Zakończ pracę", "Zamknięcie okna prezentacji spowoduje zamknięcie programu. Czy na pweno chcesz zakończyć pracę?") == QMessageBox::Yes) {
        QApplication::quit();
    }
}

OutputWindow::~OutputWindow() {
    delete ui;
}

void OutputWindow::setToScreen(QScreen* screen) {
    setWindowState(Qt::WindowNoState);
    if (screen == nullptr) {
        // set to normal window
        setGeometry(5, 50, 800, 600);
        setWindowState(Qt::WindowNoState);
    } else {
        // move window to provided screen and enable fullscreen mode
        setGeometry(screen->geometry());
        setWindowState(Qt::WindowFullScreen);
    }
}

void OutputWindow::restore() {
    scrM.restore();
}

void OutputWindow::setBlack() {
    scrM.black();
}

void OutputWindow::setEmpty() {
    scrM.empty();
}

void OutputWindow::videoPlayStop() {
    scrM.toggleVideo();
}

void OutputWindow::setVideoPos(int position) {
    scrM.getPlayer()->setPosition(position * 1000);
}

void OutputWindow::displaySong(SongItem item) {
    scrM.setSongText(item);
    scrM.setSong();
}

void OutputWindow::displayBible(QString text) {
    /*// setup text and set lastActiveTab to bible. If not black or empty, show song tab and stop media player
    ui->BibleSlideText->setText(text);
    lastActiveTab = ui->bible;

    if (ui->stackedWidget->currentWidget() == ui->black || ui->stackedWidget->currentWidget() == ui->empty) return;

    ui->stackedWidget->setCurrentWidget(ui->bible);
    player->stop();*/
}

bool compareColors(QColor color1, QColor color2) {
    if (int(color1.red() * 10) != int(color2.red() * 10)) return false;
    if (int(color1.green() * 10) != int(color2.green() * 10)) return false;
    if (int(color1.blue() * 10) != int(color2.blue() * 10)) return false;
    return true;
}

void OutputWindow::displayImage(QPixmap* image) {
    scrM.setImagePixmap(*image);
    scrM.setImage();
}

void OutputWindow::displayVideo(QString filename) {
    currentVideo = filename;
    scrM.playVideo(filename, 0);
}

QPixmap OutputWindow::preview() {
    if (usePreview_) {
        // either return actual video frame or render window
        if (ui->stackedWidget->currentWidget() == ui->video)
            return videoFrame;
        else
            return (grab());
    }
}

void OutputWindow::resizeEvent(QResizeEvent* event) {
    QWidget::resizeEvent(event);
    scrM.resize(geometry().width(), geometry().height());
    updateTheme();
}
