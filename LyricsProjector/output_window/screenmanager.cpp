#include "screenmanager.h"

#include <QApplication>
#include <QDateTime>
#include <QGraphicsDropShadowEffect>
#include <QScreen>
#include <QThread>

ScreenManager::ScreenManager() {
    graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicsView->setFrameStyle(0);

    player->setVideoOutput(videoItem);
    videoItem->setSize(QSize(width, height));

    player->setNotifyInterval(10);
    connect(player, &QMediaPlayer::positionChanged, [this](qint64 pos) {
        if (player->mediaStatus() != QMediaPlayer::MediaStatus::BufferedMedia) return;
        if (player->duration() > 0 && pos >= player->duration() - 20) videoEnd();
    });

    bcgBlack->setRect(0, 0, width, height);
    bcgBlack->setBrush(QBrush(QColor(0, 0, 0, 255)));
    bcgBlack->setPen(QPen(QColor(0, 0, 0, 0), 0));

    bcgColor->setRect(0, 0, width, height);
    bcgColor->setBrush(QBrush(QColor(0, 0, 0, 0)));
    bcgColor->setPen(QPen(QColor(0, 0, 0, 0), 0));

    if (bcgType == "image") {
        bcgImage->setPixmap(QPixmap(backgroundImage).scaled(width, height, Qt::KeepAspectRatio));
        QRectF bR = bcgImage->sceneBoundingRect();
        bcgImage->setPos(width / 2 - bR.width() / 2, height / 2 - bR.height() / 2);
    } else
        bcgImage->setVisible(false);

    blackRect->setRect(0, 0, width, height);
    blackRect->setBrush(QBrush(QColor(0, 0, 0, 0)));
    blackRect->setPen(QPen(QColor(0, 0, 0, 0), 0));

    imageBcg->setRect(0, 0, width, height);
    imageBcg->setBrush(QBrush(QColor(0, 0, 0, 255)));
    imageBcg->setPen(QPen(QColor(0, 0, 0, 0), 0));
    imageBcg->setVisible(false);
    image->setVisible(false);

    scene->addItem(bcgBlack);
    scene->addItem(bcgColor);
    scene->addItem(bcgImage);
    scene->addItem(videoItem);
    scene->addItem(songText);
    scene->addItem(songTextSecondary);
    scene->addItem(imageBcg);
    scene->addItem(image);
    scene->addItem(blackRect);

    if (bcgType == "video") {
        player->setMedia(QUrl::fromLocalFile(backgroundVideo));
        player->play();
    }
}

QGraphicsView* ScreenManager::getGraphicsView() const {
    return graphicsView;
}

void ScreenManager::renderSongText(QGraphicsPixmapItem* songText, QString text, int top) {
    songText->setVisible(true);

    // create new text pixmap
    QFontMetrics fm(songTextFont);

    QPixmap px(fm.width(text), fm.height() * 4);
    px.fill(QColor(0, 0, 0, 0));
    QPainter painter(&px);

    painter.setPen(songTextColor);
    painter.setFont(songTextFont);
    painter.drawText(0, 0, fm.width(text), fm.height() * 4, Qt::AlignCenter, text);
    painter.end();

    // setup shadow effect
    QGraphicsDropShadowEffect* sh = new QGraphicsDropShadowEffect();
    sh->setColor(QColor(0, 0, 0, 100));
    sh->setOffset(0, 2);
    sh->setBlurRadius(30);

    // apply shadow
    QGraphicsPixmapItem* pxi = new QGraphicsPixmapItem(px);
    pxi->setGraphicsEffect(sh);
    pxi->update();

    QGraphicsScene sc(0, 0, px.width(), px.height());
    sc.addItem(pxi);
    sc.addPixmap(px);

    px.fill(QColor(0, 0, 0, 0));
    QPainter p(&px);
    sc.render(&p);

    songText->setPixmap(px);

    QRectF bR = songText->sceneBoundingRect();
    songText->setPos(width / 2 - bR.width() / 2, height * top / 100 - bR.height() / 2);
}

void ScreenManager::renderSongText() {
    renderSongText(songText, text, songTop);

    if (secondaryLang != "") {
        renderSongText(songTextSecondary, textSecondary, secondaryTop);
    }

    bcgBlack->update();
    bcgImage->update();
    bcgColor->update();
}

void ScreenManager::fadeToBlack() {
    if (blackRect->brush().color().alpha() == 255) return;
    QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

    QTime timeLeft = QTime::currentTime();
    while (timeLeft.msecsTo(fadeEndTime) > 0) {
        timeLeft = QTime::currentTime();
        int val = 255 - (255 * 1000 / transition) * timeLeft.msecsTo(fadeEndTime) / 1000;
        if (val > 255) val = 255;
        blackRect->setBrush(QBrush(QColor(0, 0, 0, val)));
        QCoreApplication::processEvents();
    }
    blackRect->setBrush(QBrush(QColor(0, 0, 0, 255)));
}

void ScreenManager::fadeFromBlack() {
    if (blackRect->brush().color().alpha() == 0) return;

    QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

    QTime timeLeft = QTime::currentTime();
    while (timeLeft.msecsTo(fadeEndTime) > 0) {
        timeLeft = QTime::currentTime();
        int val = (255 * 1000 / transition) * timeLeft.msecsTo(fadeEndTime) / 1000;
        if (val < 0) val = 0;
        blackRect->setBrush(QBrush(QColor(0, 0, 0, val)));
        QCoreApplication::processEvents();
    }
}

void ScreenManager::setupSongFont(QColor color, QFont font, int size, int top, int secondaryTop) {
    font.setPixelSize(height * size / 1000);
    color.setAlpha(0);
    // songText->setBrush(QBrush(color));
    // songText->setFont(font);
    songTextColor = color;
    songTextFont = font;

    songTop = top;
    secondaryTop = secondaryTop;
}

void ScreenManager::setupBcgColor(QColor color) {
    graphicsView->setBackgroundBrush(QBrush(color));
    bcgColor->setBrush(QBrush(QColor(color)));
}

void ScreenManager::setupSecondaryLang(QString lng) {
    secondaryLang = lng;
}

void ScreenManager::resize(int w, int h) {
    width = w;
    height = h;

    bcgBlack->setRect(0, 0, width, height);
    bcgColor->setRect(0, 0, width, height);
    bcgImage->setPixmap(QPixmap(backgroundImage).scaled(width, height, Qt::KeepAspectRatio));
    QRectF bR = bcgImage->sceneBoundingRect();
    bcgImage->setPos(width / 2 - bR.width() / 2, height / 2 - bR.height() / 2);
    videoItem->setSize(QSize(width, height));
    blackRect->setRect(0, 0, width, height);
    imageBcg->setRect(0, 0, width, height);
}

void ScreenManager::setBackgroundVideo(QString video) {
    setBackgroundType("video");
    if (video == "") video = QApplication::applicationDirPath() + "/mbcg/default.mp4";
    backgroundVideo = video;
    player->setMedia(QUrl::fromLocalFile(backgroundVideo));
}

void ScreenManager::setBackgroundImage(QString image) {
    setBackgroundType("image");
    if (image == "") image = QApplication::applicationDirPath() + "/mbcg/default.jpg";
    backgroundImage = image;
    bcgImage->setPixmap(QPixmap(backgroundImage).scaled(width, height, Qt::KeepAspectRatio));
    QRectF bR = bcgImage->sceneBoundingRect();
    bcgImage->setPos(width / 2 - bR.width() / 2, height / 2 - bR.height() / 2);
}

void ScreenManager::setBackgroundType(QString type) {
    if (type != "image" && type != "video" && type != "color") return;
    bcgType = type;

    if (bcgType == "image") {
        bcgImage->setVisible(true);
        videoItem->setVisible(lastScene == "video");
        bcgColor->setVisible(false);
        backgroundVideo = "";
    } else if (bcgType == "video") {
        bcgImage->setVisible(false);
        videoItem->setVisible(true);
        bcgColor->setVisible(false);
        if (lastScene != "video" && lastScene != "image") {
            player->setMedia(QUrl::fromLocalFile(backgroundVideo));
            player->setPosition(0);
            player->play();
        }
    } else if (bcgType == "color") {
        bcgImage->setVisible(false);
        videoItem->setVisible(lastScene == "video");
        backgroundVideo = "";
        bcgColor->setVisible(true);
    }
}

void ScreenManager::black() {
    isBlack = true;

    fadeToBlack();

    if (lastScene == "image") {
        imageBcg->setVisible(false);
        image->setVisible(false);
    }

    if (lastScene == "video") {
        if (bcgType == "video") {
            videoItem->setVisible(true);
            player->setMedia(QUrl::fromLocalFile(backgroundVideo));
            player->setPosition(0);
            player->play();
        } else {
            player->stop();
            videoItem->setVisible(false);
        }
    }

    songTextColor.setAlpha(0);
    songText->setVisible(false);
    songTextSecondary->setVisible(false);
    blackRect->setBrush(QBrush(QColor(0, 0, 0, 255)));
    QCoreApplication::processEvents();
    player->pause();
}

void ScreenManager::restore() {
    isEmpty = false;
    isBlack = false;

    if (lastScene == "song") {
        if (bcgType == "color") bcgColor->setVisible(true);
        setSong();
    } else if (lastScene == "video") {
        videoItem->setVisible(true);
        player->setMedia(QUrl::fromLocalFile(currentVideo));
        player->setPosition(0);
        player->play();
        // QThread::msleep(300);
        bcgColor->setVisible(false);
    } else if (lastScene == "image") {
        if (bcgType != "image") fadeToBlack();
        setImage();
        bcgColor->setVisible(false);
    }

    fadeFromBlack();
}

void ScreenManager::empty() {
    isEmpty = true;
    isBlack = false;

    if (lastScene == "image") {
        if (bcgType != "image") fadeToBlack();
        imageBcg->setVisible(false);
        image->setVisible(false);
        videoItem->setVisible(bcgType == "video");
        bcgColor->setVisible(bcgType == "color");
    }

    if (lastScene == "video") {
        fadeToBlack();
        if (bcgType == "video") {
            videoItem->setVisible(true);
            player->setMedia(QUrl::fromLocalFile(backgroundVideo));
            player->setPosition(0);
            player->play();
        } else {
            player->stop();
            videoItem->setVisible(false);
        }
        bcgColor->setVisible(false);
    }

    if (songTextColor.alpha() == 255) {
        QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

        QTime timeLeft = QTime::currentTime();
        while (timeLeft.msecsTo(fadeEndTime) > 0) {
            timeLeft = QTime::currentTime();
            int val = (255 * 1000 / transition) * timeLeft.msecsTo(fadeEndTime) / 1000;
            if (val < 0) val = 0;
            songTextColor.setAlpha(val);

            renderSongText();
            QCoreApplication::processEvents();
        }
    }

    fadeFromBlack();
}

void ScreenManager::setSong() {
    if (isEmpty || isBlack) {
        lastScene = "song";
        return;
    }

    if (player->media().canonicalUrl() != QUrl::fromLocalFile(backgroundVideo))
        player->setMedia(QUrl::fromLocalFile(backgroundVideo));

    player->play();

    if (lastScene == "image" || lastScene == "video") {
        fadeToBlack();
        if (bcgType == "color") bcgColor->setVisible(true);
        if (lastScene == "video") {
            if (bcgType == "video") {
                videoItem->setVisible(true);
                player->setMedia(QUrl::fromLocalFile(backgroundVideo));
                player->play();
            } else {
                player->stop();
                videoItem->setVisible(false);
            }
        }
        songTextColor.setAlpha(255);
        imageBcg->setVisible(false);
        image->setVisible(false);
        renderSongText();
        fadeFromBlack();
    } else {
        if (songTextColor.alpha() == 255) return;
        QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

        QTime timeLeft = QTime::currentTime();
        while (timeLeft.msecsTo(fadeEndTime) > 0) {
            timeLeft = QTime::currentTime();
            int val = 255 - (255 * 1000 / transition) * timeLeft.msecsTo(fadeEndTime) / 1000;
            if (val > 255) val = 255;
            songTextColor.setAlpha(val);

            renderSongText();
            QCoreApplication::processEvents();
        }
        songTextColor.setAlpha(255);
    }

    lastScene = "song";
}

void ScreenManager::setSongText(SongItem item) {
    this->text = item.text();
    if (secondaryLang != "") {
        this->textSecondary = item.textLng(secondaryLang);
        if (this->textSecondary == this->text) this->textSecondary = "";
    }
    if (lastScene == "song" or lastScene == "video") renderSongText();
}

void ScreenManager::setImage() {
    if (isEmpty || isBlack) {
        lastScene = "image";
        return;
    }

    if (lastScene == "song" || lastScene == "video") fadeToBlack();

    if (lastScene == "video") {
        if (bcgType == "video") {
            videoItem->setVisible(true);
            player->setMedia(QUrl::fromLocalFile(backgroundVideo));
            player->play();
        } else {
            player->stop();
            videoItem->setVisible(false);
        }
    }

    songTextColor.setAlpha(0);
    renderSongText();

    imageBcg->setVisible(true);
    image->setVisible(true);
    lastScene = "image";
    fadeFromBlack();
}

bool ScreenManager::compareColors(QColor color1, QColor color2) {
    if (int(color1.red() * 10) != int(color2.red() * 10)) return false;
    if (int(color1.green() * 10) != int(color2.green() * 10)) return false;
    if (int(color1.blue() * 10) != int(color2.blue() * 10)) return false;
    return true;
}

void ScreenManager::videoEnd() {
    if (player->media().canonicalUrl().toString().remove("file://") == backgroundVideo) {
        player->setPosition(0);
    } else {
        player->stop();
        fadeToBlack();
    }
}

void ScreenManager::setImagePixmap(QPixmap px) {
    image->setPixmap(px.scaled(width, height, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    if (compareColors(px.toImage().pixelColor(0, 0), px.toImage().pixelColor(0, px.height() - 2)) &&
        compareColors(px.toImage().pixelColor(px.width() - 2, 0), px.toImage().pixelColor(px.width() - 2, px.height() - 2)) &&
        compareColors(px.toImage().pixelColor(0, 0), px.toImage().pixelColor(px.width() - 2, px.height() - 2))) {
        imageBcg->setBrush(QBrush(px.toImage().pixelColor(0, 0)));
    } else {
        imageBcg->setBrush(QBrush(QColor(0, 0, 0, 255)));
    }

    QRectF bR = image->sceneBoundingRect();
    image->setPos(width / 2 - bR.width() / 2, height / 2 - bR.height() / 2);
}

void ScreenManager::playVideo(QString filename, int position) {
    if (isEmpty || isBlack) {
        lastScene = "video";
        currentVideo = filename;
        return;
    }

    fadeToBlack();
    bcgColor->setVisible(false);
    imageBcg->setVisible(false);
    image->setVisible(false);
    songText->setVisible(false);
    songTextSecondary->setVisible(false);
    videoItem->setVisible(true);
    songTextColor.setAlpha(0);
    renderSongText();

    videoItem->update();

    lastScene = "video";
    currentVideo = filename;

    player->setMedia(QUrl::fromLocalFile(filename));
    if (position > 0) player->setPosition(position);
    player->play();

    // add 100ms delay
    QTime fadeEndTime = QTime::currentTime().addMSecs(100);

    QTime timeLeft = QTime::currentTime();
    while (timeLeft.msecsTo(fadeEndTime) > 0) {
        timeLeft = QTime::currentTime();
        QCoreApplication::processEvents();
    }

    fadeFromBlack();
}

void ScreenManager::stopVideo() {
    player->pause();
    fadeToBlack();
    player->stop();
}

void ScreenManager::toggleVideo() {
    if (player->state() == QMediaPlayer::PlayingState)
        player->pause();
    else
        player->play();
}

QMediaPlayer* ScreenManager::getPlayer() {
    return player;
}
