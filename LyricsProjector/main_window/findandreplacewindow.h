/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef FINDANDREPLACEWINDOW_H
#define FINDANDREPLACEWINDOW_H

/****************************************************************************
 ** This class is find and replace window.
****************************************************************************/

#include <QMainWindow>

namespace Ui {
    class FindAndReplaceWindow;
}

/** Class that contains find and replace window. */
class FindAndReplaceWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit FindAndReplaceWindow(QWidget* parent = nullptr);
    ~FindAndReplaceWindow();

private slots:
    void on_pushButton_2_clicked();
    void on_ApplyButton_clicked();
private:
    Ui::FindAndReplaceWindow* ui;

signals:
    void find(QString find, QString replace, bool searchText, bool searchTitle, bool searchItemName);   // emited when find&replace is requested
};

#endif // FINDANDREPLACEWINDOW_H
