/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef ITEMLISTBUTTON_H
#define ITEMLISTBUTTON_H

#include <QPushButton>
#include <QShortcut>

/** This class represents button for selecting slide item (middle screen list) */
class ItemListButton : public QPushButton {
    Q_OBJECT
private:
    QString itemName_;                          // item's name
    QString itemText_;                          // item's text (song verse)
    QPixmap* preview_;                          // preview (PDF page image)
    QString type_;                              // type - determines how this button should be painted (is it song verse, PDF page or something else)

    QIcon warningIcon;                          // warning icon, displayed should item have a warning flag

    ItemListButton* prew_ = nullptr;            // previous item on the list (if first item it's nullptr) - used by previous button
    ItemListButton* next_ = nullptr;            // next item on the list (if last item it's nullptr) - used by next button
    ItemListButton** current = nullptr;         // pointer to pointer to current item (yes, pointer to pointer :-)

    ItemListButton** jump = nullptr;            // pointer to pointer to which should jump at the end of the block
    bool settingJump = true;                    // should jump be enabled if this item is activated

    QShortcut* shortcut;

public:
    ItemListButton(QWidget* parent = Q_NULLPTR);
    ~ItemListButton();

    void setForSong();
    void setForImage();
    void setShortcut(QString sequence);

    void setItemName(const QString& itemName);
    const QString& itemName() const;
    QString itemText() const;
    void setItemText(const QString& itemText);
    QString type() const;
    void setPreview(QPixmap* preview);
    QPixmap* preview() const;

    ItemListButton* prew() const;
    void setPrew(ItemListButton* prew);
    ItemListButton* next() const;
    void setNext(ItemListButton* next);

    void select();

    void setCurrent(ItemListButton** value);
    void setJump(ItemListButton** value);

protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void focusInEvent(QFocusEvent* event);
#ifdef Q_OS_MAC
    virtual void mouseReleaseEvent(QMouseEvent* event);
#endif

signals:
    void triggered();                           // emited when button is selected
};

#endif // ITEMLISTBUTTON_H
