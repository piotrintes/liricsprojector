/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "mainwindow.h"

#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QMouseEvent>
#include <QFileDialog>
#include <QProcess>

/** Load all files from source directory
 * @param dir source directory
 */
void MainWindow::loadFromSourceDir(QString dir) {
    // if provided source dir exists, load all fils within. Those files should not be saved in ~/.LyricsProjector/
    QDir directory(dir);
    if (!directory.exists()) return;

    QStringList pdfList = directory.entryList(QStringList() << "*.pdf", QDir::Files);
    QStringList imageList = directory.entryList(QStringList() << "*.png" << "*.jpg" << "*.bmp" << "*.xpm" << "*.svg", QDir::Files);
    QStringList vidList = directory.entryList(QStringList() << "*.avi" << "*.mp4" << "*.mov" << "*.flv" << "*.mpg", QDir::Files);

    foreach(QString pdf, pdfList) addPdfToLib(dir + pdf, true, true);
    foreach(QString image, imageList) addImageToLib(dir + image, true, true);
    foreach(QString vid, vidList) addVideoToLib(dir + vid, true, true);
}

/** Save autoload dirs to ~/.LyricsProjector/sources.cfg */
void MainWindow::saveAutoloadDirs() {
    QFile file(QDir::homePath() + "/.LyricsProjector/sources.cfg");
    if (file.exists()) file.remove();

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        foreach(QString src, *cfg->autoloadDirs())
            out << src << "\n";
        file.close();
    }
}

/** Load autoload dirs from ~/.LyricsProjector/sources.cfg */
void MainWindow::loadAutoloadDirs() {
    QFile file(QDir::homePath() + "/.LyricsProjector/sources.cfg");
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QStringList srcList;
        QString s = file.readAll();
        file.close();

        srcList = s.split("\n");
        foreach(QString src, srcList) {
            if (src == "") continue;
            cfg->autoloadDirs()->append(src);
            loadFromSourceDir(src);
        }
    }
}
