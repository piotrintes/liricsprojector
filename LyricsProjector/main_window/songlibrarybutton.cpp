/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "songlibrarybutton.h"

#include <QApplication>
#include <QMouseEvent>
#include <QPainter>

Song *SongLibraryButton::song() const
{
    return song_;
}

SongLibraryButton::SongLibraryButton(Song *song, QWidget *parent): QPushButton(parent)
{
    song_ = song;

    //set button size and margin, to make it looke nice on the list
    setMinimumSize(100,70);
    setStyleSheet("padding-bottom:10px");
    setMouseTracking(true);

    //set addToListIcon and warningIcon icons
    #ifndef Q_OS_LINUX
        addToListIcon = QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/arrow-add.png"));
        warningIcon = QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/dialog-warning.png"));
    #else
        addToListIcon = QIcon::fromTheme("arrow-right");
        warningIcon = QIcon::fromTheme("dialog-warning");
    #endif
}

void SongLibraryButton::paintEvent(QPaintEvent *event)
{

    // setup painter
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // if button is selected it should have it's background colored, and text color should be set to system's highlightedText color
    if(hasFocus()){
        painter.setPen(QPen(palette().highlight(),0));
        painter.setBrush(QBrush(palette().highlight()));
        painter.drawRect(0,0,width(),height());
        painter.setPen(QPen(palette().highlightedText(),0));
    }

    // setup font (bold text)
    QFont f(font());

    f.setBold(true);
    painter.setFont(f);

    // print song title
    painter.drawText(10,30,song_->title());

    // switch font from bold to italic
    f.setBold(false);
    f.setItalic(true);
    painter.setFont(f);

    // print song first verse
    if(song_->items.size())
        painter.drawText(30,50,song_->items.at(0).text().split("\n").at(0).left(30) + " ...");

    // paint warning icon if needed
    if(song()->hasWarning()){
        painter.drawPixmap(10,height()-24,24,24,warningIcon.pixmap(24,24));
    }

#ifndef Q_OS_MAC
    // when button is under mouse, display arrow on the right
    if(underMouse()) painter.drawPixmap(width() - 50,height()/2-16,32,32,addToListIcon.pixmap(32,32));
#endif

}

void SongLibraryButton::mouseReleaseEvent(QMouseEvent *event)
{
    // mouse click event:
    if(event->button() == Qt::RightButton)
        song_->rmbLibMenu()->popup(event->globalPos()); // if right button, display context menu
    else {
        if(event->x() > width()-50)
            emit addClicked();                          // mouse click ended on the right side of the button (or outside button) emit addClicked signal
        else
            emit selected();                            // otherwise emit selected signal
    }
}

void SongLibraryButton::mouseDoubleClickEvent(QMouseEvent *event)
{
    // double click event - emit addClicked signal
    emit addClicked();
}
