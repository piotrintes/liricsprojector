/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "config/autostartmanager.h"
#include "config/sourcesmanager.h"
#include "config/themeeditor.h"
#include "findandreplacewindow.h"
#include "mainwindow.h"
#include "misc/aboutwindow.h"
#include "network/networkmanager.h"
#include "song_editor/songeditor.h"
#include "ui_mainwindow.h"

#include <QDateTime>
#include <QDir>
#include <QFileDialog>
#include <QGuiApplication>
#include <QMessageBox>
#include <QScreen>

 /** Initialize main menu.
  * @param t - theme (reference will be passed to theme editor)
  */
void MainWindow::initMainMenu(Theme* t) {
    // setup icons (Windows and Mac, Linux uses icon theme)
#ifndef Q_OS_LINUX
    QPixmap menuIcon(QApplication::applicationDirPath() + "/icons/application-menu.png");
    QPixmap arrowLeftIcon(QApplication::applicationDirPath() + "/icons/arrow-left.png");
    QPixmap arrowRightIcon(QApplication::applicationDirPath() + "/icons/arrow-right.png");
    QPixmap editIcon(QApplication::applicationDirPath() + "/icons/document-edit.png");
    QPixmap addIcon(QApplication::applicationDirPath() + "/icons/list-add.png");

    ui->MainMenuButton->setIcon(QIcon(menuIcon));
    ui->SongsMenuButton->setIcon(QIcon(menuIcon));
    ui->historyGoBack->setIcon(QIcon(arrowLeftIcon));
    ui->EditOpenedButton->setIcon(QIcon(editIcon));
    ui->prewButton->setIcon(QIcon(arrowLeftIcon));
    ui->nextButton->setIcon(QIcon(arrowRightIcon));
    ui->addImageButton->setIcon(QIcon(addIcon));
    ui->addPdfButton->setIcon(QIcon(addIcon));
    ui->addVideoButton->setIcon(QIcon(addIcon));
#endif

    // if theme config file exists load theme from file
    QDir dir(QDir::homePath() + "/.LyricsProjector/");
    if (dir.exists()) {
        QFile file(QDir::homePath() + "/.LyricsProjector/theme.cfg");
        t->loadTheme(&file);
    } else {
        dir.mkdir(QDir::homePath() + "/.LyricsProjector/");
    }

    // connect theme update signal to output window updateTheme and main window's updatePreview
    connect(t, &Theme::updated, outputWindow, &OutputWindow::updateTheme);
    connect(t, &Theme::updated, this, &MainWindow::updatePreview);

    // setup main menu
    QMenu* mainMenu = new QMenu;
    QAction* editTheme = new QAction(QIcon::fromTheme("color-management"), "Styl");
    QAction* sources = new QAction(QIcon::fromTheme("document-open-folder"), "Źródła danych");
    QAction* startCfg = new QAction(QIcon::fromTheme("automated-tasks"), "Autostart");
    QAction* network = new QAction(QIcon::fromTheme("folder-network"), "Sieć");
    QAction* about = new QAction(QIcon::fromTheme("help-about"), "O programie");

    // setup icons (Windows and Mac, Linux uses icon theme)
#ifndef Q_OS_LINUX
    QPixmap editThemeIcon(QApplication::applicationDirPath() + "/icons/color-management.png");
    QPixmap sourcesIcon(QApplication::applicationDirPath() + "/icons/document-open-folder.png");
    QPixmap startCfgIcon(QApplication::applicationDirPath() + "/icons/automated-tasks.png");
    QPixmap networkIcon(QApplication::applicationDirPath() + "/icons/folder-network.png");
    QPixmap aboutIcon(QApplication::applicationDirPath() + "/icons/help-about.png");

    editTheme->setIcon(QIcon(editThemeIcon));
    sources->setIcon(QIcon(sourcesIcon));
    startCfg->setIcon(QIcon(startCfgIcon));
    network->setIcon(QIcon(networkIcon));
    about->setIcon(QIcon(aboutIcon));
#endif

    QMenu* screenSelector = new QMenu("Ekran docelowy");

    connect(screenSelector, &QMenu::aboutToShow, [this, screenSelector]() {
        screenSelector->clear();

        QAction* setWindow = new QAction(QIcon::fromTheme(""), "Wyświetl w oknie");
        // when output screen is directed to window - execute lambda expression - set output window to nullptr
        connect(setWindow, &QAction::triggered, [this]() {
            outputWindow->setToScreen(nullptr);
            cfg->setOutScreenIndex(-1);
            saveConfig();
        });
        screenSelector->addAction(setWindow);

        // add button for each screen
        foreach(QScreen * scr, QGuiApplication::screens()) {
            QAction* setScr = new QAction(QIcon::fromTheme(""), scr->name());
            // when screen is selected - execute lambda expression - set output window to selected screen
            connect(setScr, &QAction::triggered, [this, scr]() {
                outputWindow->setToScreen(scr);
                cfg->setOutScreenIndex(QGuiApplication::screens().indexOf(scr));
                saveConfig();
            });
            screenSelector->addAction(setScr);
        }
    });

    // when edit theme action is triggered - execute lambda expression - open theme editor
    connect(editTheme, &QAction::triggered, [t]() {
        ThemeEditor* e = new ThemeEditor(t);
        e->show();
    });

    // when edit sources action is triggered - execute lambda expression - open sources editor
    connect(sources, &QAction::triggered, [this]() {
        SourcesManager* e = new SourcesManager(this->cfg->autoloadDirs());
        e->show();

        // when source editor is accepted - execute lambda expression - save autoloaded sources
        connect(e, &SourcesManager::accepted, [this]() {
            saveAutoloadDirs();
        });
    });

    // when edit autostart action is triggered - execute lambda expression - open autostart settings window
    connect(startCfg, &QAction::triggered, [this]() {
        AutostartManager* am = new AutostartManager(libraryImages, libraryPDFs, libraryVideos, cfg);
        am->show();

        // when source editor is accepted - execute lambda expression - save autoloaded sources
        connect(am, &AutostartManager::accepted, [this]() {
            saveConfig();
        });

        // when edit sources action is triggered - execute lambda expression - open sources editor
        connect(am, &AutostartManager::sourcesEditorRequested, [this]() {
            SourcesManager* e = new SourcesManager(this->cfg->autoloadDirs());
            e->show();

            // when source editor is accepted - execute lambda expression - save autoloaded sources
            connect(e, &SourcesManager::accepted, [this]() {
                saveAutoloadDirs();
            });
        });
    });

    // when network action is triggered - execute lambda expression - open network manager
    connect(network, &QAction::triggered, [this]() {
        NetworkManager* netW = new NetworkManager(this->network);
        netW->show();

        // when source editor is accepted - execute lambda expression - save autoloaded sources
        connect(netW, &NetworkManager::accepted, [this]() {
            saveConfig();
        });
    });

    // when about action is triggered - execute lambda expression - open about window
    connect(about, &QAction::triggered, []() {
        AboutWindow* aboutW = new AboutWindow();
        aboutW->show();
    });

    // add actions to menu
    mainMenu->addAction(editTheme);
    mainMenu->addMenu(screenSelector);
    mainMenu->addSeparator();
    mainMenu->addAction(sources);
    mainMenu->addAction(startCfg);
    mainMenu->addAction(network);
    mainMenu->addSeparator();
    mainMenu->addAction(about);

    ui->MainMenuButton->setMenu(mainMenu);

    // setup preview menu
    QMenu* prevMenu = new QMenu;
    QAction* prevEnable = new QAction("Podgląd");
    prevEnable->setCheckable(true);
    prevEnable->setChecked(true);

    // when preview enable checkbox is triggered - execute lambda expression - enable/disable preview
    connect(prevEnable, &QAction::changed, [this, prevEnable]() {
        outputWindow->setUsePreview(prevEnable->isChecked());
        if (prevEnable->isChecked())
            updatePreview();
        else
            ui->preview->setText("Podgląd wyłączony");
    });

    // add actions to menu
    prevMenu->addAction(prevEnable);

    // add menu to preview context menu
    connect(ui->preview, &QLabel::customContextMenuRequested, [this, prevMenu](QPoint p) {
        prevMenu->popup(ui->preview->mapToGlobal(p));
    });
}

/** Initialize songs menu */
void MainWindow::initSongMenu() {
    // setup songs menu
    QMenu* songsMenu = new QMenu;
    QAction* newSong = new QAction(QIcon::fromTheme("list-add"), "Dodaj pieśń");
    newSong->setShortcut(QKeySequence("ctrl+n"));
    QAction* importSongs = new QAction(QIcon::fromTheme("document-import"), "Importuj pieśni");
    QAction* exportSongs = new QAction(QIcon::fromTheme("document-export"), "Eksportuj pieśni");
    QAction* findReplaceSongs = new QAction(QIcon::fromTheme("edit-find-replace"), "Znajdź i zamień");

    // setup icons (Windows only, Linux uses icon theme)
#ifndef Q_OS_LINUX
    QPixmap importIcon(QApplication::applicationDirPath() + "/icons/document-import.png");
    QPixmap exportIcon(QApplication::applicationDirPath() + "/icons/document-export.png");
    QPixmap findIcon(QApplication::applicationDirPath() + "/icons/edit-find-replace.png");
    QPixmap addIcon(QApplication::applicationDirPath() + "/icons/list-add.png");

    newSong->setIcon(QIcon(addIcon));
    importSongs->setIcon(QIcon(importIcon));
    exportSongs->setIcon(QIcon(exportIcon));
    findReplaceSongs->setIcon(QIcon(findIcon));
#endif

    // when new song action is triggered - execute lambda expression - create new song
    connect(newSong, &QAction::triggered, [this]() {
        // create new song, set new filename and open song editor
        Song* song = new Song();
        song->setFilename(QDir::homePath() + "/.LyricsProjector/Songs/" + QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss"));
        SongEditor* e = new SongEditor(song);
        e->show();
        // when song editor is accepted - execute lambda expression - add song to library
        connect(e, &SongEditor::accepted, [this, song]() {
            addSongToLib(song);
        });
    });

    // when import songs action is triggered - execute lambda expression - import songs
    connect(importSongs, &QAction::triggered, [this]() {
        QStringList files = QFileDialog::getOpenFileNames(nullptr, "Wybierz pieśni do importu");
        foreach(QString filename, files) {
            // copy song file to ~/.LyricsProjector/Songs/
            QFile::copy(filename, QDir::homePath() + "/.LyricsProjector/Songs/" + filename.split("/").last());
            QFile file(QDir::homePath() + "/.LyricsProjector/Songs/" + filename.split("/").last());
            if (file.open(QFile::ReadOnly | QFile::Text)) {
                QTextStream in(&file);
                // create song, read from XML file, set filename and add to library
                Song* song = new Song();
                song->readFromXML(in.readAll());
                song->setFilename(QDir::homePath() + "/.LyricsProjector/Songs/" + filename.split("/").last());
                addSongToLib(song);
                file.close();
            }
        }
        QMessageBox::information(nullptr, "Import pieśni", "Pomyślnie importowano " + QString::number(files.size()) + " pieśni.");
    });

    // when export songs action is triggered - execute lambda expression - export songs
    connect(exportSongs, &QAction::triggered, [this]() {
        QString dir = QFileDialog::getExistingDirectory(nullptr, "Wybierz katalog do eksportu");
        if (dir != "") {
            // copy all song files to export location
            foreach(Song * song, librarySongs)
                QFile::copy(song->filename(), dir + "/" + song->filename().split("/").last());
            QMessageBox::information(nullptr, "Eksport pieśni", "Pomyślnie wyeksportowano " + QString::number(librarySongs.size()) + " pieśni.");
        }
    });

    // when find and replace songs action is triggered - execute lambda expression - display find and replace window
    connect(findReplaceSongs, &QAction::triggered, [this]() {
        FindAndReplaceWindow* w = new FindAndReplaceWindow();
        w->show();

        // when find and replace is accepted - execute lambda expression - replace and save for all songs
        connect(w, &FindAndReplaceWindow::find, [this](QString find, QString replace, bool searchText, bool searchTitle, bool searchItemName) {
            // counters for output information
            int songCounter = 0;
            int changesCounter = 0;

            foreach(Song * song, librarySongs) {
                // count changes in this song
                int localCounter = song->findAndReplace(find, replace, searchText, searchTitle, searchItemName);
                if (localCounter) {          // if changes were made in this song

                    // save song file
                    QFile file(song->filename());
                    if (file.exists()) file.remove();

                    if (file.open(QIODevice::WriteOnly)) {
                        QTextStream out(&file);
                        out.setCodec("UTF-8");
                        out << song->wrtieToXML();
                        file.close();
                    }

                    // update counters
                    songCounter++;
                    changesCounter += localCounter;
                }
            }
            // output message
            QMessageBox::information(nullptr, "Znajdź i zamień", "Wykonano " + QString::number(changesCounter) + " zmian w " + QString::number(songCounter) + " pieśniach.");
        });
    });

    // add actions to song menu
    songsMenu->addAction(newSong);
    songsMenu->addAction(importSongs);
    songsMenu->addAction(exportSongs);
    songsMenu->addAction(findReplaceSongs);

    ui->SongsMenuButton->setMenu(songsMenu);
}
