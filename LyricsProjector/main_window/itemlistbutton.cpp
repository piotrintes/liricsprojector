/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "itemlistbutton.h"

#include <QApplication>
#include <QDebug>
#include <QFocusEvent>
#include <QPainter>
#include "misc/consts.h"

/** Get item type */
QString ItemListButton::type() const {
    return type_;
}

/** Set preview image (used for pdf page)
 * @param preview preview image
 */
void ItemListButton::setPreview(QPixmap* preview) {
    preview_ = preview;
}

/** Get preview image (used for pdf page)
 * @return preview image or nullptr if there is no preview
 */
QPixmap* ItemListButton::preview() const {
    return preview_;
}


/** Get previous item button
 * @return previous item pointer or nullptr if there is no previous item
 */
ItemListButton* ItemListButton::prew() const {
    return prew_;
}

/** Set previous item button
 * @param prew previous item pointer
 */
void ItemListButton::setPrew(ItemListButton* prew) {
    prew_ = prew;
}

/** Get next item button
 * @return next item pointer or nullptr if there is no next item
 */
ItemListButton* ItemListButton::next() const {
    return next_;
}

/** Set next item button
 * @param next next item pointer
 */
void ItemListButton::setNext(ItemListButton* next) {
    next_ = next;
}

/** Set item name (used for pdf page and song verse)
 * @param itemName item name
 */
void ItemListButton::setItemName(const QString& itemName) {
    itemName_ = itemName;
}

/** Get item name (used for pdf page and song verse)
 * @return item name
 */
const QString& ItemListButton::itemName() const {
    return itemName_;
}

/** Get item text (used for song verse)
 * @return item text
 */
QString ItemListButton::itemText() const {
    return itemText_;
}

/** Set item text (used for song verse)
 * @param itemText item text
 */
void ItemListButton::setItemText(const QString& itemText) {
    itemText_ = itemText;
}

/** Select this item */
void ItemListButton::select() {
    setFocus();
    emit triggered();
    repaint();
}

/** Set pointer to current item pointer. This is needed for intelligent slide jumping
 * @param value pointer to current item pointer
 */
void ItemListButton::setCurrent(ItemListButton** value) {
    current = value;
}

/** Set pointer to which should jump at the end of the block
 * @param value pointer to which should jump at the end of the block
 */
void ItemListButton::setJump(ItemListButton** value) {
    jump = value;
}

/**
 * @param parent parent widget (inherited from QPushButton, can be nullptr)
 */
ItemListButton::ItemListButton(QWidget* parent) : QPushButton(parent) {
    shortcut = new QShortcut(this);
    connect(shortcut, &QShortcut::activated, [this]() {
        bool isChorus = false;
        if ((*current)->itemName().left(1).toLower() == 'c' || (*current)->itemName().left(1).toLower() == 'r') {
            isChorus = true;
        }

        if (settingJump) {
            // if current element is a chorus, do not reset jump
            if (!isChorus) {
                // set jump to next non-refren element after current's block
                ItemListButton* newJump = *current;
                while ((newJump->itemName().left(1).toLower() == 'r'
                    || newJump->itemName().left(1).toLower() == 'c'
                    || newJump->itemName() == (*current)->itemName())
                    && newJump->next() != nullptr) {
                    newJump = newJump->next();
                }
                if (newJump == *current) newJump = nullptr;
                *jump = newJump;
            }
        } else {
            *jump = nullptr;
        }

        setFocus();
        emit triggered();
        repaint();
    });

#ifndef Q_OS_LINUX
    warningIcon = QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/dialog-warning.png"));
#else
    warningIcon = QIcon::fromTheme("dialog-warning");
#endif
    setForSong();
}

ItemListButton::~ItemListButton() {
    delete shortcut;
}

/** Set style for song item */
void ItemListButton::setForSong() {
    // set style for song item, set type_ to "song"
    setMinimumSize(100, itemText_.count("\n") * 20 + 40);
    setStyleSheet("padding-bottom:10px");
    type_ = "song";
}

/** Set style for image (pdf page) item */
void ItemListButton::setForImage() {
    // set style for image item, set type_ to "image"
    setMinimumSize(100, 200);
    setStyleSheet("padding-bottom:10px");
    type_ = "image";
}

/** Set shortcut
 * @param sequence shortcut
 */
void ItemListButton::setShortcut(QString sequence) {
    shortcut->setKey(QKeySequence(sequence));

    // check if this is setting jump (first chorus)
    settingJump = sequence == "R";
}

/** Override paint event
 * @param event paint event
 */
void ItemListButton::paintEvent(QPaintEvent* event) {
    // setup painter
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    //draw separator
    painter.drawLine(0, height(), width(), height());


    // if button is selected it should have it's background colored, and text color should be set to system's highlightedText color
    if (*current == this) {
        painter.setPen(QPen(palette().highlight(), 0));
        painter.setBrush(QBrush(palette().highlight()));
        if (type_ == "song")
            painter.drawRect(10, 0, width() - 10, height());
        else
            painter.drawRect(0, 0, width(), height());
        painter.setPen(QPen(palette().highlightedText(), 0));
    }

    // type-specyfic drawing:
    if (type_ == "song") {
        // draw song's verse text
        painter.drawText(45, 0, width() - 50, height(), Qt::AlignCenter | Qt::AlignVCenter, itemText_);

        // setup font for item name
        QFont f(font());

        f.setBold(true);
        painter.setFont(f);
        // draw item name text
        painter.drawText(20, 0, 20, height(), Qt::AlignCenter | Qt::AlignVCenter, itemName_.left(2));

        // draw item group indicators
        QColor indicatorColor;
        indicatorColor = indicator_colors::other_color;
        if (itemName_.at(0).toLower() == 'v' || itemName_.at(0).toLower() == 'z') indicatorColor = indicator_colors::verse_color;
        if (itemName_.at(0).toLower() == 'c' || itemName_.at(0).toLower() == 'r') indicatorColor = indicator_colors::chorus_color;
        if (itemName_.at(0).toLower() == 'b') indicatorColor = indicator_colors::bridge_color;
        if (itemName_.at(0).toLower() == 'p') indicatorColor = indicator_colors::prechorus_color;

        painter.setPen(QPen(indicatorColor, 8, Qt::SolidLine, Qt::RoundCap));

        // draw group beginning indicator
        if (prew() == nullptr || prew()->itemName().left(2) != itemName_.left(2)) {
            if (next() != nullptr && next()->itemName().left(2) == itemName_.left(2))
                painter.drawLine(10, 10, 10, height() + 10);
        }

        // draw group item indicator
        if (next() != nullptr && next()->itemName().left(2) == itemName_.left(2)) {
            if (prew() != nullptr && prew()->itemName().left(2) == itemName_.left(2))
                painter.drawLine(10, -10, 10, height() + 10);
        }

        // draw standalone item indicator
        if (next() == nullptr || next()->itemName().left(2) != itemName_.left(2)) {
            if (prew() == nullptr || prew()->itemName().left(2) != itemName_.left(2))
                painter.drawLine(10, 10, 10, height() - 10);
        }

        // draw group end indicator
        if (next() == nullptr || next()->itemName().left(2) != itemName_.left(2)) {
            if (prew() != nullptr && prew()->itemName().left(2) == itemName_.left(2)) {
                painter.drawLine(10, -10, 10, height() - 10);
                // if jump element is selected and this is end of current chorus draw jump indicator
                if (*current != nullptr
                    && (itemName_.left(1).toLower() == "c" || itemName_.left(1).toLower() == "r")
                    && itemName_.left(2) == (*current)->itemName().left(2)) {
                    if (next() != nullptr && *jump != nullptr) {
                        painter.setPen(QPen(palette().text().color()));
                        painter.drawText(width() - 30, height() - 30, 20, 30, Qt::AlignCenter | Qt::AlignVCenter, (*jump)->itemName());
                    }
                }
            }
        }

        // check for warnings
        if (itemText_.count("\n") >= 3)
            painter.drawPixmap(0, 0, 32, 32, warningIcon.pixmap(32, 32));    // draw warning icon if needed

    } else if (type_ == "image") {
        // draw preview image
        QPixmap p = preview_->scaledToHeight(height() - 20, Qt::SmoothTransformation);
        painter.drawPixmap(40 + (width() - 40) / 2 - p.width() / 2, 10, p);

        // setup font for item name
        QFont f(font());

        f.setBold(true);
        painter.setFont(f);
        // draw item name text
        painter.drawText(0, 0, 40, height(), Qt::AlignCenter | Qt::AlignVCenter, itemName_);
    }

}

/** Override focus in event.
 * @param event focus in event
*/
void ItemListButton::focusInEvent(QFocusEvent* event) {
    // handle jump
    if (jump != nullptr && *jump != nullptr) {
        if (event->reason() == Qt::MouseFocusReason) {
            // if clicked by mouse remove jump event
            *jump = nullptr;
        } else if (prew() != nullptr
            && (itemName().left(1).toLower() != "c" && itemName().left(1).toLower() != "r")
            && (prew()->itemName().left(1).toLower() == "c" || prew()->itemName().left(1).toLower() == "r")) {
            // if this is jumping item, jump
            (*jump)->select();
            *jump = nullptr;
            prew()->repaint();
            return;
        }
    }

    // if button gets focus it it trigerred - emit triggered
    emit triggered();
    repaint();
}

#ifdef Q_OS_MAC
void ItemListButton::mouseReleaseEvent(QMouseEvent* event) {
    // if button is clicked - emit triggered
    emit triggered();
    repaint();
}
#endif
