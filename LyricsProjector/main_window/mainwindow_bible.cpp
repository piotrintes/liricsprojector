/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>

/** Load Bible.
 * @param filename path to Bible XML
*/
void MainWindow::loadBible(QString filename) {     // there's still no need for XML parser there ;-)

    bible.clear();

    bool showBibleUi = false;

    //hide bible ui
    ui->noBibleLabel->setVisible(!showBibleUi);
    ui->BibleAddToPlan->setVisible(showBibleUi);
    ui->BiblePreviewFrame->setVisible(showBibleUi);
    ui->BibleAddToPlan->setVisible(showBibleUi);
    ui->BibleBookComboBox->setVisible(showBibleUi);
    ui->BiblieTextPreview->setVisible(showBibleUi);
    ui->BibleChapterComboBox->setVisible(showBibleUi);
    ui->BibleVerseToComboBox->setVisible(showBibleUi);
    ui->BibleVerseFromComboBox->setVisible(showBibleUi);
    ui->label_3->setVisible(showBibleUi);
    ui->label_4->setVisible(showBibleUi);
    ui->label_5->setVisible(showBibleUi);
    ui->label_6->setVisible(showBibleUi);

    QFile file(filename);
    if (!file.exists()) return;

    // open and read XML file
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QString f = file.readAll();
        file.close();

        // make list of books
        QStringList bookList = f.split("<BIBLEBOOK bname=\"");
        bookList.removeFirst();

        QThread* thread = new QThread();
        connect(thread, &QThread::started, [this, bookList]() {
            foreach(QString bookEntry, bookList) {
                // get name and chapter list for each book
                QString BookName = bookEntry.split("\"").at(0);
                QStringList BookChapters = bookEntry.split("<CHAPTER cnumber=\"");
                BookChapters.removeFirst();

                QList<QList<QString>> chapterList;
                foreach(QString chapter, BookChapters) {
                    // get list of verses for each chapter
                    QStringList verses = chapter.split("<VERS vnumber=\"");
                    verses.removeFirst();

                    QList<QString> versList;
                    foreach(QString vers, verses) {
                        // fix each vers to make sure it fits into display
                        vers.remove("</VERS>\n");
                        vers = vers.split("> ").last();
                        vers.replace("\t", "").replace("\n", "");
                        while (vers.contains("  ")) vers.replace("  ", " ");
                        // add each verse to list of verse for current chapter
                        versList.append(vers);
                    }

                    // Remove file's end
                    versList.last().remove("</CHAPTER>").remove("</BIBLEBOOK>").remove("</XMLBIBLE>");

                    // add each verse list for current chapter to current book
                    chapterList.append(versList);
                }
                // key each book name with it's chapter list (set of lists of verses for every chapter)
                bible.insert(BookName, chapterList);
                // fill BibleBookComboBox
                ui->BibleBookComboBox->addItem(BookName);
            }
            //select first item
            ui->BibleBookComboBox->setCurrentIndex(0);
        });

        thread->start();

        //show bible ui
        showBibleUi = true;

        ui->noBibleLabel->setVisible(!showBibleUi);
        ui->BibleAddToPlan->setVisible(showBibleUi);
        ui->BiblePreviewFrame->setVisible(showBibleUi);
        ui->BibleAddToPlan->setVisible(showBibleUi);
        ui->BibleBookComboBox->setVisible(showBibleUi);
        ui->BiblieTextPreview->setVisible(showBibleUi);
        ui->BibleChapterComboBox->setVisible(showBibleUi);
        ui->BibleVerseToComboBox->setVisible(showBibleUi);
        ui->BibleVerseFromComboBox->setVisible(showBibleUi);
        ui->label_3->setVisible(showBibleUi);
        ui->label_4->setVisible(showBibleUi);
        ui->label_5->setVisible(showBibleUi);
        ui->label_6->setVisible(showBibleUi);
    }
}

/** Bible book changed: update chapter list.*/
void MainWindow::on_BibleBookComboBox_currentIndexChanged(const QString& book) {
    ui->BibleChapterComboBox->clear();
    for (int i = 0; i < bible[book].size(); ++i) {
        ui->BibleChapterComboBox->addItem(QString::number(i + 1));
    }
}

/** Bible chapter changed: update verse list.*/
void MainWindow::on_BibleChapterComboBox_currentIndexChanged(int chapter) {
    if (ui->BibleChapterComboBox->count() == 0) return;
    ui->BibleVerseFromComboBox->clear();

    QString book = ui->BibleBookComboBox->currentText();
    int verseCount = bible[book][chapter].size();

    for (int i = 0; i < verseCount; ++i) {
        ui->BibleVerseFromComboBox->addItem(QString::number(i + 1));
    }
}

/** Bible verse from changed: update verse to list.*/
void MainWindow::on_BibleVerseFromComboBox_currentIndexChanged(int startVerse) {
    if (ui->BibleVerseFromComboBox->count() == 0) return;
    ui->BibleVerseToComboBox->clear();

    QString book = ui->BibleBookComboBox->currentText();
    int chapter = ui->BibleChapterComboBox->currentIndex();
    int verseCount = bible[book][chapter].size();

    for (int i = startVerse; i < verseCount; ++i) {
        ui->BibleVerseToComboBox->addItem(QString::number(i + 1));
    }
}

/** Bible verse to changed: update text preview (from startVerse to endVerse).*/
void MainWindow::on_BibleVerseToComboBox_currentIndexChanged(int endVerse) {
    QString fragment = "";

    QString book = ui->BibleBookComboBox->currentText();
    int chapter = ui->BibleChapterComboBox->currentIndex();
    int startVerse = ui->BibleVerseFromComboBox->currentIndex();
    endVerse += startVerse;

    for (int vers = startVerse; vers < endVerse + 1; ++vers) {
        fragment += " " + QString::number(vers + 1) + ". " +
            bible[book][chapter][vers] + "\n";
    }

    ui->BiblieTextPreview->setPlainText(fragment);
    ui->BibliePreview->setText(fragment);

    updateBiblePreview();
}

/** Add Bible to plan clicked: create list item for plan list and plan elemet (context menu and lambda expressions).*/
void MainWindow::on_BibleAddToPlan_clicked() {
    QString book = ui->BibleBookComboBox->currentText();
    int chapter = ui->BibleChapterComboBox->currentIndex() + 1;
    int startVerse = ui->BibleVerseFromComboBox->currentIndex() + 1;
    int endVerse = startVerse + ui->BibleVerseToComboBox->currentIndex();


    QString name = book + " " + QString::number(chapter) + " " + QString::number(startVerse);
    if (startVerse != endVerse) name += "-" + QString::number(endVerse);
    QString text = ui->BibliePreview->text();


    // create list item for plan list and plan elemet (context menu and lambda expressions)
#ifndef Q_OS_LINUX
    QListWidgetItem* bibleListItem = new QListWidgetItem(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/bible.png")), name);
#else
    QListWidgetItem* bibleListItem = new QListWidgetItem(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/bible.svg")), name);
#endif
    PlanElement* biblePlanElement = new PlanElement();

    //when plan element is triggered (list item double clicked) - execute lambda expression - display image on screen
    connect(biblePlanElement, &PlanElement::triggered, [this, text, name, biblePlanElement]() {
        // if video is played - ask for confirmation
        if (outputWindow->videoPlaying()) {
            if (QMessageBox::question(this, "Wymagane potwierdzenie.", "Odtwarzanie filmu zostanie przerwane. Czy na pewno chcesz kontynuować?")
                == QMessageBox::No) {
                return;
            }
        }

        // history - work in progress
        history.append(biblePlanElement);

        // next/previous buttons won't work for bible - set current to nullptr
        current = nullptr;

        // setup ui - hide alert frame and edit button, set title
        ui->alertFrame->hide();
        ui->EditOpenedButton->hide();
        ui->ElementTitle->setText(name);

        // clear item list (middle)
        QLayoutItem* item;
        while ((item = ui->ItemsListLayout->takeAt(0)) != nullptr) {
            if (item->widget())
                delete item->widget();
            delete item;
        }

        // display image on screen and update preview
        outputWindow->displayBible(text);
        ui->VideoControlsFrame->hide();
        updatePreview();
    });

    // setup RMB context menu
    QMenu* rmbMenu = new QMenu;

    QAction* remove = new QAction("Usuń z planu");
#ifndef Q_OS_LINUX
    remove->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png").scaledToWidth(24)));
#else
    remove->setIcon(QIcon::fromTheme("edit-delete"));
#endif
    rmbMenu->addAction(remove);

    // when remove action is triggered - execute lambda expression - remove element from plan
    connect(remove, &QAction::triggered, [this, biblePlanElement]() {
        removeFromPlan(biblePlanElement);
    });

    // set element menu
    biblePlanElement->setRmbMenu(rmbMenu);

    // pair list item with plan elemet (context menu and lambda expressions) and add list item to plan list
    planElementList.insert(bibleListItem, biblePlanElement);
    ui->planListWidget->addItem(bibleListItem);
}

/** Update Bible preview */
void MainWindow::updateBiblePreview() {
    Theme* t = outputWindow->theme();
    QFont f = t->Font();
    f.setPixelSize(t->bibleTextSize() * ui->BiblePreviewFrame->height() / 1000);
    ui->BibliePreview->setFont(f);

    QPalette p;
    p.setColor(QPalette::Window, t->bibleBcgColor());
    p.setColor(QPalette::WindowText, t->bibleTextColor());
    ui->BiblePreviewFrame->setPalette(p);


    ui->BibliePreview->setContentsMargins(ui->BiblePreviewFrame->width() * t->bibleMargins().at(0) / 100,
        ui->BiblePreviewFrame->height() * t->bibleMargins().at(2) / 100,
        ui->BiblePreviewFrame->width() * t->bibleMargins().at(1) / 100,
        ui->BiblePreviewFrame->height() * t->bibleMargins().at(3) / 100);
}

/** Add Bible from XML button clicked: open file dialog and copy selected file to ~/.LyricsProjector, load Bible */
void MainWindow::on_BibleAddXML_clicked() {
    // copy file into library dir
    QString filename = QFileDialog::getOpenFileName(nullptr, "Wybierz plik Biblii", QString(), "XML (*.XML *.xml)");
    if (filename == "") return;
    QFile::copy(filename, QDir::homePath() + "/.LyricsProjector/Bible.xml");

    loadBible(QDir::homePath() + "/.LyricsProjector/Bible.xml");
}
