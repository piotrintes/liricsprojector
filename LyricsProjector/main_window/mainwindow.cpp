/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "mainwindow.h"
#include "imagelibrarybutton.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QMessageBox>
#include <QMimeData>
#include <QMouseEvent>
#include <QNetworkDatagram>
#include <QNetworkInterface>
#include <QProcess>
#include <QProgressDialog>
#include <QShortcut>
#include <QThread>
#include <QUdpSocket>

#ifdef Q_OS_WIN
#include <QSettings>
#endif

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    network = new Network(this);

    setAcceptDrops(true);

    QElapsedTimer timer;
    timer.start();

    // create library and config directory
    QDir dir(QDir::homePath() + "/.LyricsProjector/");
    if (!dir.exists()) dir.mkdir(QDir::homePath() + "/.LyricsProjector/");

    initUI();
    loadConfig();
    setupNetworking();

    // display splashScreen
    QPixmap splashScreen(QApplication::applicationDirPath() + "/SplashScreen.png");
    outputWindow->displayImage(&splashScreen);

    loadSongs();
    loadBible(QDir::homePath() + "/.LyricsProjector/Bible.xml");
    updateAutoBcgs();
    loadLibraryFiles();
    loadAutoloadDirs();

    // add global shortcuts
    QShortcut* delShc = new QShortcut(QKeySequence(tr("Delete")), this);
    connect(delShc, &QShortcut::activated, [this]() {
        if (ui->planListWidget->hasFocus() && ui->planListWidget->currentItem() != nullptr) {
            planElementList[ui->planListWidget->currentItem()]->rmbMenu()->actions().last()->trigger();
        }
    });

    for (int i = 0; i < ui->libraryTabWidget->count() + 1; i++) {
        QShortcut* tabSel = new QShortcut(QKeySequence("F" + QString::number(i)), this);
        connect(tabSel, &QShortcut::activated, [this, i]() {
            ui->libraryTabWidget->setCurrentIndex(i - 1);
        });
    }

    // set lists background
    QPalette pal = palette();
    pal.setColor(QPalette::ColorRole::Window, pal.base().color());

    ui->scrollArea->setPalette(pal);
    ui->scrollArea_2->setPalette(pal);
    ui->scrollArea_3->setPalette(pal);
    ui->scrollArea_4->setPalette(pal);
    ui->scrollArea_5->setPalette(pal);

    QIcon warningIcon;
#ifndef Q_OS_LINUX
    warningIcon = QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/dialog-warning.png"));
#else
    warningIcon = QIcon::fromTheme("dialog-warning");
#endif
    ui->WarningIcon->setPixmap(warningIcon.pixmap(48, 48));
    ui->alertFrame->hide();

    qDebug() << timer.elapsed() << "ms";
}

MainWindow::~MainWindow() {
    delete ui;
}

/** Go to next slide */
void MainWindow::nextSlide() {
    if (current == nullptr || current->next() == nullptr) return;
    current->next()->select();
    ui->scrollArea_2->repaint();
    ui->scrollArea_2->ensureWidgetVisible(current);
}

/** Go to previous slide */
void MainWindow::prevSlide() {
    if (current == nullptr || current->prew() == nullptr) return;
    current->prew()->select();
    ui->scrollArea_2->repaint();
    ui->scrollArea_2->ensureWidgetVisible(current);
}

void MainWindow::black(int setBlack) {
    if (setBlack == -1) {
        ui->setScreenBlack->toggle();
        setBlack = ui->setScreenBlack->isChecked();
    }
    ui->setScreenNormal->setChecked(false);
    ui->setScreenEmpty->setChecked(false);
    if (setBlack) {
        network->sentBlack();
        outputWindow->setBlack();
    } else {
        ui->setScreenNormal->setChecked(true);
        network->sentRestore();
        outputWindow->restore();
    }
    updatePreview();
}

QList<Pdf*> MainWindow::getPdfList() {
    return libraryPDFs;
}

QList<Pdf*> MainWindow::getAutoloadPDFList() {
    return libraryAutoloadPDFs;
}

/** Setup networking (udp socket for remote control) */
void MainWindow::setupNetworking() {
    // setup listener for remote control (via android device)
    QUdpSocket* udpSocket = new QUdpSocket(this);
    udpSocket->bind(QHostAddress::Any, network->remoteRecieverPort());

    connect(udpSocket, &QUdpSocket::readyRead, [this, udpSocket]() {
        while (udpSocket->hasPendingDatagrams()) {
            QString datagram = udpSocket->receiveDatagram().data();
            if (datagram == "-1") on_prewButton_clicked();
            if (datagram == "1") on_nextButton_clicked();
            if (datagram == "0") {
                if (ui->setScreenNormal->isChecked()) {
                    ui->setScreenBlack->toggle();
                    on_setScreenBlack_clicked(true);
                } else {
                    ui->setScreenNormal->toggle();
                    on_setScreenNormal_clicked();
                }
            }
        }
    });
}

/** Save library files to ~/.LyricsProjector/*.cfg */
void MainWindow::saveLibraryFiles() {
    // save images to list in ~/.LyricsProjector/images.cfg
    QFile fileImages(QDir::homePath() + "/.LyricsProjector/images.cfg");
    if (fileImages.exists()) fileImages.remove();

    if (fileImages.open(QIODevice::WriteOnly)) {
        QTextStream out(&fileImages);
        foreach(QString image, libraryImages)
            out << image << "\n";
        fileImages.close();
    }

    // save PDFs to list in ~/.LyricsProjector/pdfs.cfg
    QFile filePdf(QDir::homePath() + "/.LyricsProjector/pdfs.cfg");
    if (filePdf.exists()) filePdf.remove();

    if (filePdf.open(QIODevice::WriteOnly)) {
        QTextStream out(&filePdf);
        foreach(Pdf * pdf, libraryPDFs)
            out << pdf->filename() << "\n";
        filePdf.close();
    }

    // save videos to list in ~/.LyricsProjector/videos.cfg
    QFile fileVid(QDir::homePath() + "/.LyricsProjector/videos.cfg");
    if (fileVid.exists()) fileVid.remove();

    if (fileVid.open(QIODevice::WriteOnly)) {
        QTextStream out(&fileVid);
        foreach(QString vid, libraryVideos)
            out << vid << "\n";
        fileVid.close();
    }
}

/** Load library files from ~/.LyricsProjector/*.cfg */
void MainWindow::loadLibraryFiles() {
    // load images from list in ~/.LyricsProjector/images.cfg
    QFile fileImages(QDir::homePath() + "/.LyricsProjector/images.cfg");
    if (fileImages.open(QFile::ReadOnly | QFile::Text)) {
        QStringList imageList;
        QString s = fileImages.readAll();
        fileImages.close();

        imageList = s.split("\n");
        foreach(QString image, imageList) {
            addImageToLib(image, true);
        }
    }

    // load PDFs from list in ~/.LyricsProjector/pdfs.cfg
    QFile filePdf(QDir::homePath() + "/.LyricsProjector/pdfs.cfg");
    if (filePdf.open(QFile::ReadOnly | QFile::Text)) {
        QStringList pdfList;
        QString s = filePdf.readAll();
        filePdf.close();

        pdfList = s.split("\n");
        foreach(QString pdf, pdfList) {
            addPdfToLib(pdf, true);
        }
    }

    // load videos from list in ~/.LyricsProjector/videos.cfg
    QFile fileVid(QDir::homePath() + "/.LyricsProjector/videos.cfg");
    if (fileVid.open(QFile::ReadOnly | QFile::Text)) {
        QStringList videoList;
        QString s = fileVid.readAll();
        fileVid.close();

        videoList = s.split("\n");
        foreach(QString vid, videoList) {
            addVideoToLib(vid, true);
        }
    }

    // load backgrounds from list in ~/.LyricsProjector/bcg.cfg
    QFile fileBcg(QDir::homePath() + "/.LyricsProjector/bcg.cfg");
    if (fileBcg.open(QFile::ReadOnly | QFile::Text)) {
        QStringList bgList;
        QString s = fileBcg.readAll();
        fileBcg.close();

        bgList = s.split("\n");
        foreach(QString bg, bgList) {
            if (!bg.contains(":")) continue;
            addBcgToLib(bg, true);
        }
    }
}

void MainWindow::addBcgToLib(QString source, bool noNotUpdateLibFile, bool autoloaded) {
    QString type = source.split(":").at(0);
    QString filename = source.split(":").at(1);

    if (type != "color") {
        QFile file(filename);
        if (!file.exists()) return;
    }

    Bcg* bcg = new Bcg(type, filename);
    ImageLibraryButton* button = bcg->button();

    // when button sends addClicked signal - execute lambda expression - set background
    connect(button, &ImageLibraryButton::selected, [this, bcg]() {
        outputWindow->setBackground(bcg);
    });

    button->setAutoloaded(autoloaded);
    if (!autoloaded) libraryBCGs.append(bcg);
    if (!noNotUpdateLibFile) saveLibraryFiles();

    // qDebug() << "addBcgToLib - " << type << " : " << filename << " : " << autoloaded;

    ui->bcgLibLayout->addWidget(button);
}

void MainWindow::updateAutoBcgs() {
    foreach(Bcg * bcg, libraryBCGs) {
        ui->bcgLibLayout->removeWidget(bcg->button());
        // delete bcg;
    }

    // create solid color background from theme
    addBcgToLib("color:" + outputWindow->theme()->bcgColor().name(), true);

}

/** Remove element from plan
 * @param element element to remove
 */
void MainWindow::removeFromPlan(PlanElement* element) {
    // get list item paired with provided element
    QListWidgetItem* listItem = planElementList.key(element);
    // remove pair from hashpable
    planElementList.remove(listItem);
    // remove list item from plan list
    ui->planListWidget->removeItemWidget(listItem);
    // clean up memory - delete both objects
    delete listItem;
    delete element;
}

/** Override drag&drop event to enable dropping files into library
 * @param e event
 */
void MainWindow::dragEnterEvent(QDragEnterEvent* e) {
    if (e->mimeData()->hasUrls()) {
        e->acceptProposedAction();
    }
}

/** Override drag&drop event to enable dropping files into library
 * @param e event
 */
void MainWindow::dropEvent(QDropEvent* e) {
    foreach(const QUrl & url, e->mimeData()->urls()) {
        QString fileName = url.toLocalFile();
        if (fileName.right(3) == "jpg" || fileName.right(3) == "png" || fileName.right(3) == "bmp" || fileName.right(3) == "svg") {
            addImageToLib(fileName);
            ui->libraryTabWidget->setCurrentWidget(ui->tab_image);
        } else if (fileName.right(3) == "mp4" || fileName.right(3) == "flv" || fileName.right(3) == "avi" || fileName.right(3) == "mov") {
            addVideoToLib(fileName);
            ui->libraryTabWidget->setCurrentWidget(ui->tab_video);
        } else if (fileName.right(3) == "pdf") {
            addPdfToLib(fileName);
            ui->libraryTabWidget->setCurrentWidget(ui->tab_pdf);
        }
    }
}

/** Override show event: this sets up the main toolbar, and updates preview.
 * On Windows, it also, this sets up toolbar colors and icons based on background color for dark and light themes
 * @param event
 */
void MainWindow::showEvent(QShowEvent* event) {
    updatePreview();
    QApplication::processEvents();
    updateToolbar();
    // setup top bar colors and icons based on background
#ifdef Q_OS_WIN
    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\DWM", QSettings::NativeFormat);

    QColor WindBcg(Qt::white);
    if (settings.value("ColorPrevalence").toBool()) {
        WindBcg = QColor("#" + QString::number(settings.value("AccentColor").toUInt(), 16));

        // swap blue and red (for some reason they are in wrong order in windows reg)
        int r = WindBcg.blue();
        WindBcg.setBlue(WindBcg.red());
        WindBcg.setRed(r);

        double luminance = (0.299 * WindBcg.red() + 0.587 * WindBcg.green() + 0.114 * WindBcg.blue()) / 255;

        // if this is dark color, switch text and icons on toolbar to light colors
        if (luminance < 0.5) {
            QPalette p;
            p.setColor(QPalette::ButtonText, QColor(240, 240, 240));
            ui->ElementTitle->setPalette(p);
            ui->label->setPalette(p);
            ui->BibleAddXML->setPalette(p);
            QPixmap menuIcon(QApplication::applicationDirPath() + "/icons/application-menu-light.png");
            QPixmap arrowLeftIcon(QApplication::applicationDirPath() + "/icons/arrow-left-light.png");
            QPixmap editIcon(QApplication::applicationDirPath() + "/icons/document-edit-light.png");
            QPixmap addIcon(QApplication::applicationDirPath() + "/icons/list-add-light.png");

            ui->MainMenuButton->setIcon(QIcon(menuIcon));
            ui->SongsMenuButton->setIcon(QIcon(menuIcon));
            ui->historyGoBack->setIcon(QIcon(arrowLeftIcon));
            ui->EditOpenedButton->setIcon(QIcon(editIcon));
            ui->addPdfButton->setIcon(QIcon(addIcon));
            ui->addImageButton->setIcon(QIcon(addIcon));
            ui->addVideoButton->setIcon(QIcon(addIcon));
        }
    }

    // paint toolbar
    QPalette p;
    p.setColor(QPalette::Button, WindBcg);
    p.setColor(QPalette::Light, WindBcg);
    ui->toolBar->setAutoFillBackground(true);
    ui->toolBar->setPalette(p);
#endif
}

/** Override resize event: updates previews and toolbar
 * @param event
 */
void MainWindow::resizeEvent(QResizeEvent* event) {
    QMainWindow::resizeEvent(event);
    updatePreview();
    updateBiblePreview();
    updateToolbar();
}

/** Override close event: ask user if he wants to quit
 * @param event
 */
void MainWindow::closeEvent(QCloseEvent* event) {
    event->ignore();

    if (QMessageBox::question(this, "Zakończ pracę", "Czy na pweno chcesz zakończyć pracę?") == QMessageBox::Yes) {
        network->stopVideo();
        QApplication::quit();
    }
}

/** Library tab changed: update toolbar to corresponding tab
 * @param index
 */
void MainWindow::on_libraryTabWidget_currentChanged(int index) {
    ui->LibraryToolbarWidget->setCurrentIndex(index);
    updateToolbar();
}
