/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef IMAGELIBRARYBUTTON_H
#define IMAGELIBRARYBUTTON_H


#include <QFile>
#include <QMenu>
#include <QPushButton>

/** This class represents library button displaying image and element's title.
 * It is used to graphically represent elements like images, PDFs and Videos.
 */
class ImageLibraryButton : public QPushButton {
    Q_OBJECT
private:
    QPixmap* image_ = new QPixmap();                // image displayed one the left side of the button
    QString name_;                                  // element's name
    QString filename_;                              // filename of element's source
    QString length_;                                // string describing either video length or PDF slide count
    QIcon addToListIcon;                            // icon with right-pointing arrow that will be displayed when mouse hoveres over button

    QIcon isAutoloadedIcon;                         // icon indicating item has been loaded automatically rather than from library
    bool isAutoloaded_ = false;                     // autoloaded items are not saved into library
    bool arrowOnMouseOver = true;                   // arrow should be displayed when mouse is over button

    QMenu* rmbMenu = new QMenu;                     // right mouse button menu

public:
    ImageLibraryButton(QString imageFile, QString name = "", QWidget* parent = Q_NULLPTR);
    ~ImageLibraryButton();

    QPixmap* image() const;
    QString name() const;
    QString filename() const;

    void setAutoloaded(bool autoloaded);

    void setVidLength(const int& newVidLength);
    void setSlideNumber(const int& newSlideNumber);
    void setImage(QPixmap* newImage);
    void setVideoPreview(QString filename);
    void setArrowOnMouseOver(bool arrowOnMouseOver);

protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void mouseReleaseEvent(QMouseEvent* event);
    virtual void mouseDoubleClickEvent(QMouseEvent* event);

signals:
    void selected();                                // emited when button is selected
    void addClicked();                              // emited then add button is clicked (either via arrow icon, context menu or when double-clicked)
    void removeClicked();                           // emited when remove button is clicked (via context menu)
};

#endif // IMAGELIBRARYBUTTON_H
