/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "findandreplacewindow.h"
#include "ui_findandreplacewindow.h"

/**
 * @param parent parent widget (inherited from QMainWindow, can be nullptr)
*/
FindAndReplaceWindow::FindAndReplaceWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::FindAndReplaceWindow) {
    ui->setupUi(this);
}

FindAndReplaceWindow::~FindAndReplaceWindow() {
    delete ui;
}

/** Cancel button clicked: close window */
void FindAndReplaceWindow::on_pushButton_2_clicked() {
    delete this;
}

/** Apply button clicked: emit signal to request search and replace, then close window */
void FindAndReplaceWindow::on_ApplyButton_clicked() {
    emit find(
        ui->FindLineExit->text(),
        ui->ReplaceLineEdit->text(),
        ui->SearchTextsCheckBox->isChecked(),
        ui->SearchTitlesCheckBox->isChecked(),
        ui->SearchNamesCheckBox->isChecked()
    );
    delete this;
}
