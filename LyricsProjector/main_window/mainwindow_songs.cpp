/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "imagelibrarybutton.h"
#include "itemlistbutton.h"
#include "mainwindow.h"
#include "song_editor/songeditor.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QMouseEvent>
#include <QProcess>
#include <QThread>

 /** Loads all songs from ~/.LyricsProjector/Songs/ directory */
void MainWindow::loadSongs() {
    QDir directory(QDir::homePath() + "/.LyricsProjector/Songs/");
    QStringList lngSongs;
    if (directory.exists()) {          // if song library exists
        QStringList songsLib = directory.entryList(QStringList() << "*", QDir::Files);
        foreach(QString filename, songsLib) {          // for each file in ~/.LyricsProjector/Songs/
            if (!filename.contains(".lng.")) {          // skip translations
                Song* song = new Song();
                addSongToLib(song);
                song->setFilename(directory.path() + "/" + filename);
            } else {
                lngSongs.append(directory.path() + "/" + filename);
            }
        }
    } else {
        directory.mkdir(QDir::homePath() + "/.LyricsProjector/Songs/");
    }

    QThread* thread = new QThread();
    connect(thread, &QThread::started, [this, lngSongs]() {
        foreach(Song * song, librarySongs) {
            QFile file(song->filename());
            if (file.open(QFile::ReadOnly | QFile::Text)) {
                // QTextStream in(&file);
                //  create new song, read it from XML file, set filename and add to library
                song->readFromXML(QString::fromUtf8(file.readAll()));
                // check if song has translation files
                foreach(QString lngFilename, lngSongs) {
                    if (lngFilename.split(".lng.").at(0) == song->filename()) {
                        // if translation fits the song, open file and add to song
                        QFile fileL(lngFilename);
                        if (fileL.open(QFile::ReadOnly | QFile::Text)) {
                            // QTextStream in(&fileL);
                            song->readLngFromXML(lngFilename.split(".lng.").at(1), QString::fromUtf8(fileL.readAll()));
                        }
                    }
                }
            } else {
                qDebug() << "W: Can't open lirics file: " + song->filename();
                librarySongs.removeOne(song);
            }
        }
    });
    thread->start();
}

/** Add song to library
 * @param song song to add
 */
void MainWindow::addSongToLib(Song* song) {
    // add to song library list
    librarySongs.append(song);
    // add song button (created automatically) to song list
    ui->songsLibLayout->addWidget(song->libButton());

    // when song requests to be added to plan - execute lambda expression - add song to plan
    connect(song, &Song::addToPlan, [this, song]() {
        addSongToPlan(song);
    });
    // when song requests to be opened - execute lambda expression - open song
    connect(song, &Song::openSong, [this, song]() {
        openSong(song);
    });
    // when song requests to removed - execute lambda expression - remove song from library
    connect(song, &Song::removeSong, [this, song]() {
        removeSong(song);
    });
    // when song requests to be edited - execute lambda expression - open song editors
    connect(song, &Song::edit, [song]() {
        SongEditor* e = new SongEditor(song);
        e->show();
    });
}

void MainWindow::addSongToPlan(Song* song) {
    // create list item for plan list and plan elemet (context menu and lambda expressions)
    QListWidgetItem* songListItem = new QListWidgetItem(QIcon::fromTheme("music-note-16th"), song->title());
#ifndef Q_OS_LINUX
    songListItem->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/music-note-16th.png")));
#endif
    PlanElement* songPlanElement = new PlanElement();

    // when plan element is triggered (list item double clicked) - execute lambda expression - open song
    connect(songPlanElement, &PlanElement::triggered, [this, song, songPlanElement]() {
        openSong(song);
        history.append(songPlanElement);
    });

    // setup RMB context menu
    QMenu* rmbMenu = new QMenu;

    rmbMenu->addAction(song->editAction());

    QAction* remove = new QAction("Usuń z planu");
#ifndef Q_OS_LINUX
    remove->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png")));
#else
    remove->setIcon(QIcon::fromTheme("edit-delete"));
#endif
    rmbMenu->addAction(remove);

    // when remove action is triggered - execute lambda expression - remove element from plan
    connect(remove, &QAction::triggered, [this, songPlanElement]() {
        removeFromPlan(songPlanElement);
    });

    // set element menu
    songPlanElement->setRmbMenu(rmbMenu);

    // pair list item with plan elemet (context menu and lambda expressions) and add list item to plan list
    planElementList.insert(songListItem, songPlanElement);
    ui->planListWidget->addItem(songListItem);
}

/** Open song
 * @param song song to open
 */
void MainWindow::openSong(Song* song) {
    current = nullptr;
    // setup ui - show alert frame and set it's text if needed, and set title
    ui->ElementTitle->setText(song->title().left(18));
    ui->WarningText->setText(song->warningMessage());
    ui->alertFrame->setVisible(song->hasWarning());

    // show edit button and disconnect it from previously opened song
    ui->EditOpenedButton->show();
    ui->EditOpenedButton->disconnect();
    // when edit button is triggered - execute lambda expression - have song emit edit event (connected to another lambda upon song creation)
    connect(ui->EditOpenedButton, &QToolButton::clicked, [song]() {
        emit song->edit();
    });

    // clear item list (middle)
    QLayoutItem* item;
    while ((item = ui->ItemsListLayout->takeAt(0)) != nullptr) {
        if (item->widget())
            delete item->widget();
        delete item;
    }

    ItemListButton* prew = nullptr;

    // shortcuts related flags
    bool firstBridgeAsigned = false;
    bool firstChorusAsigned = false;
    bool firstPreChorusAsigned = false;
    bool firstVerseAsigned[10] = { false };

    // fill item list with verses
    foreach(SongItem item, song->items) {
        // create new item button, setup name, text and set type to song
        jump = nullptr;
        ItemListButton* itemButton = new ItemListButton();
        itemButton->setItemName(item.name());
        itemButton->setItemText(item.text());
        itemButton->setForSong();
        itemButton->setCurrent(&current);
        itemButton->setJump(&jump);

        // connect button to previous one in relation previous - next (unless there is no prevoius)
        itemButton->setPrew(prew);
        if (prew != nullptr) prew->setNext(itemButton);
        prew = itemButton;

        // setup button shortcut
        if (!firstBridgeAsigned && item.name().toLower().at(0) == 'b') {
            itemButton->setShortcut("B");
            firstBridgeAsigned = true;
        } else if (!firstChorusAsigned && item.name().toLower().at(0) == 'c') {
            itemButton->setShortcut("C");
            firstChorusAsigned = true;
        } else if (!firstChorusAsigned && item.name().toLower().at(0) == 'r') {
            itemButton->setShortcut("R");
            firstChorusAsigned = true;
        } else if (!firstChorusAsigned && item.name().toLower().at(0) == 'p') {
            itemButton->setShortcut("P");
            firstPreChorusAsigned = true;
        } else if (item.name().length() > 1 && item.name().at(1).isDigit() && item.name().toLower().at(0) == 'z' && !firstVerseAsigned[QString(item.name().at(1)).toInt()]) {
            itemButton->setShortcut(QString(item.name().at(1)));
            firstVerseAsigned[QString(item.name().at(1)).toInt()] = true;
        } else if (item.name().length() > 1 && item.name().at(1).isDigit() && item.name().toLower().at(0) == 'v' && !firstVerseAsigned[QString(item.name().at(1)).toInt()]) {
            itemButton->setShortcut(QString(item.name().at(1)));
            firstVerseAsigned[QString(item.name().at(1)).toInt()] = true;
        }

        // when item button is triggered - execute lambda expression - display text on screen
        connect(itemButton, &ItemListButton::triggered, [this, song, item, itemButton]() {
            // set current item button, display text on screen, and update preview
#ifdef Q_OS_MAC
            ItemListButton* old = current;
#endif
            if (current == itemButton) return;
            current = itemButton;
            network->sentSong(song, item);
            ui->VideoControlsFrame->hide();
#ifdef Q_OS_MAC
            if (old != nullptr) old->repaint();
#endif
            if (current->next() != nullptr)
                network->sentSongNext(current->next()->itemText());
            else
                network->sentSongNext("");

            outputWindow->displaySong(item);
            updatePreview();
            ui->scrollArea_2->ensureWidgetVisible(current);
            ui->scrollArea_2->repaint();
        });

        // add button to item list
        ui->ItemsListLayout->addWidget(itemButton);
    }
}

/** Remove song from library
 * @param song song to remove
 */
void MainWindow::removeSong(Song* song) {
    // Song file will be removed - ask for confirmation
    if (QMessageBox::question(this, "Usuń pieśń", "Czy na pweno chcesz usunąć pieśń:\n" + song->title() + "\n\nTej operacji nie można cofnąć.") == QMessageBox::Yes) {
        // remove song from library, delete song button
        // !DO NOT delete song - that could potentially cause crash later on (song can still be on plan, so user might still use it).
        librarySongs.removeOne(song);
        delete song->libButton();
        QFile file(song->filename());
        file.remove();
    }
}

/** Song search bar text changed: show/hide songs in library view according to filter */
void MainWindow::on_searchSongs_textChanged(const QString& arg1) {
    int c = 0;
    foreach(Song * song, librarySongs) {
        song->libButton()->setVisible(song->search(arg1));
        c++;
        if (c > 100) {
            c = 0;
            QCoreApplication::processEvents();
            if (ui->searchSongs->text() != arg1) {
                return;
            }
        }
    }
}
