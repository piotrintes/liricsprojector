/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "imagelibrarybutton.h"
#include "itemlistbutton.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QMouseEvent>
#include <QProcess>

 /** Add pdf to library
  * @param filename path to pdf file
  * @param noNotUpdateLibFile if true, library file will not be updated (used for autoloaded pdfs)
  * @param autoloaded if true, pdf button will have autoloaded icon
  */
void MainWindow::addPdfToLib(QString filename, bool noNotUpdateLibFile, bool autoloaded) {
    QFile file(filename);
    if (!file.exists()) return;          // if pdf file does not exist function can't continue

    // setup dir for pdf cache
    QDir dir(QDir::homePath() + "/.LyricsProjector/pdfCache/");
    if (!dir.exists()) dir.mkdir(QDir::homePath() + "/.LyricsProjector/pdfCache/");

    // use mutool to render pdf into png images
    QProcess* process = new QProcess();

#ifdef Q_OS_WIN
    process->start("mutool.exe draw -F png -r 180 -o \"" + QDir::homePath() + "/.LyricsProjector/pdfCache/" + filename.split("/").last() + "%d.png\" \"" + filename + "\"");
#endif
#ifdef Q_OS_LINUX
    process->start("mutool draw -F png -r 180 -o \"" + QDir::homePath() + "/.LyricsProjector/pdfCache/" + filename.split("/").last() + "%d.png\" \"" + filename + "\"");
#endif
#ifdef Q_OS_MAC
    process->start(QApplication::applicationDirPath() + "/mutool draw -F png -r 180 -o \"" + QDir::homePath() + "/.LyricsProjector/pdfCache/" + filename.split("/").last() + "%d.png\" \"" + filename + "\"");
#endif

    // when process can't run - execute lambda expression - display error message
    connect(process, &QProcess::errorOccurred, [](int err) {
        if (err == 0)          // mutool is not installed
#ifdef Q_OS_LINUX
            QMessageBox::critical(nullptr, "Dodawanie pliku pdf nie powiodło się.",
                "Nie można uruchomić programu mutool, niezbędnego do przetwarzania plików PDF.\n\n"
                "Upewnij się, że pakiet mupdf-tools jest zainstalowany. Pakiet ten można zainstalować poleceniem:\n"
                "  sudo apt install mupdf-tools\n"
                "Instalacja wymaga uprawnień administratora.");
#else
            QMessageBox::critical(nullptr, "Dodawanie pliku pdf nie powiodło się.",
                "Nie można uruchomić programu mutool, niezbędnego do przetwarzania plików PDF.\n\n"
                "Upewnij się, że pakiet mupdf-tools jest zainstalowany. Instrukcja instalacji znajduje się tutaj:\n"
                "  http://macappstore.org/mupdf-tools/\n"
                "Instalacja wymaga uprawnień administratora.");

        // http://macappstore.org/mupdf-tools/
#endif
    });

    // when process is finnished - execute lambda expression - process result
    connect(process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [this, filename, noNotUpdateLibFile, autoloaded, process](int exitCode, QProcess::ExitStatus exitStatus) {
        if (!exitCode) {          // exitCode = 0 - add pdf to plan

            // create pdf object and button to represent it in library
            Pdf* pdf = new Pdf(filename);
            ImageLibraryButton* pdfButton = new ImageLibraryButton(QDir::homePath() + "/.LyricsProjector/pdfCache/" + filename.split("/").last() + "1.png", filename.split("/").last());
            pdfButton->setAutoloaded(autoloaded);

            // add button to pdf list, set pdf button, and set whether it was autoloaded or part of static library
            ui->pdfLibLayout->addWidget(pdfButton);
            pdf->setButton(pdfButton);
            pdf->addReference();

            // when pdf button is selected - execute lambda expression - open pdf item
            connect(pdfButton, &ImageLibraryButton::selected, [this, pdf]() {
                openPdf(pdf);
            });

            // when pdf button sends addClicked signal - execute lambda expression - add pdf to plan
            connect(pdfButton, &ImageLibraryButton::addClicked, [this, pdf]() {
                addPdfToPlan(pdf);
            });

            // when pdf button sends removeClicked signal - execute lambda expression - remove pdf from library
            connect(pdfButton, &ImageLibraryButton::removeClicked, [this, pdf]() {
                removePdf(pdf);
            });

            // add pdf to pdf library list
            if (autoloaded) libraryAutoloadPDFs.append(pdf);
            else libraryPDFs.append(pdf);
            if (!noNotUpdateLibFile) saveLibraryFiles();

            // auto add to plan if selected
            if (cfg->autoAddPDFs()->contains(filename) || (cfg->autoAddSrcPDFs() && autoloaded))
                emit pdfButton->addClicked();

        } else {
            // something went wrong and mutool returned no-zero - display error message
            QMessageBox::critical(this, "Dodawanie pliku pdf nie powiodło się.", "Plik " + filename + " nie został odczytany.\n"
                "Upewnij się, że wskazany plik jest poprawnym plikiem PDF.");
        }
        // close connection to mutool process
        process->close();
    });
}

/** Add pdf to plan
 * @param pdf pdf object
 */
void MainWindow::addPdfToPlan(Pdf* pdf) {
    // create list item for plan list and plan elemet (context menu and lambda expressions)
    QListWidgetItem* pdfListItem = new QListWidgetItem(QIcon::fromTheme("application-pdf"), pdf->title());
#ifndef Q_OS_LINUX
    pdfListItem->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/application-pdf.png")));
#endif
    PlanElement* pdfPlanElement = new PlanElement();

    // when plan element is triggered (list item double clicked) - execute lambda expression - open pdf
    connect(pdfPlanElement, &PlanElement::triggered, [this, pdf, pdfPlanElement]() {
        history.append(pdfPlanElement);
        openPdf(pdf);
    });

    // setup RMB context menu
    QMenu* rmbMenu = new QMenu;
    QAction* remove = new QAction("Usuń z planu");
#ifndef Q_OS_LINUX
    remove->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png")));
#else
    remove->setIcon(QIcon::fromTheme("edit-delete"));
#endif
    rmbMenu->addAction(remove);

    // when remove action is triggered - execute lambda expression - remove element from plan
    connect(remove, &QAction::triggered, [this, pdfPlanElement, pdf]() {
        removeFromPlan(pdfPlanElement);
        pdf->removeReference();
    });

    // set element menu
    pdfPlanElement->setRmbMenu(rmbMenu);

    // pair list item with plan elemet (context menu and lambda expressions) and add list item to plan list
    planElementList.insert(pdfListItem, pdfPlanElement);
    ui->planListWidget->addItem(pdfListItem);
    pdf->addReference();
}

/** Open pdf
 * @param pdf pdf object
 */
void MainWindow::openPdf(Pdf* pdf) {
    current = nullptr;
    // setup ui - hide alert frame and edit button, set title
    ui->ElementTitle->setText(pdf->title().left(20));
    ui->alertFrame->hide();
    ui->EditOpenedButton->hide();
    pdf->addReference();

    // clear item list (middle)
    QLayoutItem* item;
    while ((item = ui->ItemsListLayout->takeAt(0)) != nullptr) {
        if (item->widget())
            delete item->widget();
        delete item;
    }

    ItemListButton* prew = nullptr;

    bool first = true;
    // fill item list with pdf slides
    foreach(QPixmap * item, pdf->slides) {
        // create new item button, setup name, preview image and set type for image
        ItemListButton* itemButton = new ItemListButton();
        itemButton->setItemName(QString::number(pdf->slides.indexOf(item) + 1));
        itemButton->setPreview(item);
        itemButton->setForImage();
        itemButton->setCurrent(&current);

        // connect button to previous one in relation previous - next (unless there is no prevoius)
        itemButton->setPrew(prew);
        if (prew != nullptr) prew->setNext(itemButton);
        prew = itemButton;

        // when item button is triggered - execute lambda expression - display image on screen
        connect(itemButton, &ItemListButton::triggered, [this, item, itemButton]() {
            // set current item button, display image (pdf slide) on screen, and update preview
#ifdef Q_OS_MAC
            ItemListButton* old = current;
#endif
            current = itemButton;
            network->sentImage(item);
            ui->VideoControlsFrame->hide();
#ifdef Q_OS_MAC
            if (old != nullptr) old->repaint();
#endif
            if (current->next() != nullptr)
                network->sentImageNext(current->next()->preview());
            else
                network->sentImageNext(nullptr);

            outputWindow->displayImage(item);
            updatePreview();
        });

        if (first) {
            first = false;
            connect(itemButton, &ItemListButton::destroyed, [pdf]() {
                pdf->removeReference();
            });
        }

        // add button to item list
        ui->ItemsListLayout->addWidget(itemButton);
    }
}

/** Remove pdf from library
 * @param pdf pdf object
 */
void MainWindow::removePdf(Pdf* pdf) {
    // remove pdf from library, delete pdf button, and save new library
    // !DO NOT delete pdf - that could potentially cause crash later on (pdf can still be on plan, so user might still use it).
    // use pdf->removeReference() instead - it will delete pdf if reference counter is 0
    libraryPDFs.removeOne(pdf);
    pdf->removeReference();
    delete pdf->button();
    saveLibraryFiles();
}

/** Add pdf button clicked: get pdf path and add it to library */
void MainWindow::on_addPdfButton_clicked() {
    QString url = QFileDialog::getOpenFileName(nullptr, "Dodaj prezentację PDF do biblioteki", QString(), "PDF (*.pdf)");
    if (url != "") addPdfToLib(url);
}
