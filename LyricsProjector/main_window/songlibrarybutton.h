/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef SONGLIBRARYBUTTON_H
#define SONGLIBRARYBUTTON_H

/****************************************************************************
 ** This class represents library button displaying song's title and part
 ** of first verse, as well as warning icon if needed.
 ** It is used to graphically represent songs on library list.
****************************************************************************/

#include "library/song.h"

#include <QPushButton>

class SongLibraryButton: public QPushButton         // this class uses QPushButton as it's base class providing basic button's functionality
{
    Q_OBJECT
private:
    Song *song_;                                    // song that button represent
    QIcon addToListIcon;                            // icon with right-pointing arrow that will be displayed when mouse hoveres over button
    QIcon warningIcon;                              // warning icon, displayed should song have a warning flag
public:
    SongLibraryButton(Song *song, QWidget* parent = Q_NULLPTR);
    // constructor - requires song

    Song *song() const;                             // get song_

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseDoubleClickEvent(QMouseEvent *event);

signals:
    void selected();                                // emited when button is selected
    void addClicked();                              // emited then add button is clicked (either via arrow icon, context menu or when double-clicked)
};

#endif // SONGLIBRARYBUTTON_H
