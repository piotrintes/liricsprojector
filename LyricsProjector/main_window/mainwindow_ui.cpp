/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#ifdef Q_OS_WIN
#include <QtWinExtras>
#endif

#include <QDebug>
#include <QDir>
#include <QShortcut>

 /** Initialize UI */
void MainWindow::initUI() {
    // setup default theme
    Theme* t = new Theme();
    t->setBcgColor(QColor(100, 200, 255));
    t->setBcgType("color");
    t->setTextColor(QColor(255, 255, 255));
    t->setBibleBcgColor(QColor(100, 200, 255));
    t->setBibleTextColor(QColor(255, 255, 255));
    t->setTextSize(50);
    t->setBibleTextSize(50);

    // system specyfic adjustments (windows UI tweaks)
#ifdef Q_OS_WIN
    ui->toolBar->layout()->setMargin(8);
#endif

    initMainMenu(t);
    initSongMenu();

    // init toolbars
    ui->toolBar->addWidget(ui->LibraryToolbarWidget);
    ui->toolBar->addWidget(ui->MiddleToolbarWidget);
    ui->toolBar->addWidget(ui->MainToolbarWidget);
    ui->PdfToolbarWidget->hide();
    ui->ImageToolbarWidget->hide();
    ui->VideoToolbarWidget->hide();

    // set screen controls button icons
#ifdef Q_OS_LINUX
    ui->setScreenNormal->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/screenDisplay.svg")));
    ui->setScreenBlack->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/screenBlack.svg")));
    ui->setScreenEmpty->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/screenTheme.svg")));
#else
    ui->setScreenNormal->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/screenDisplay.png")));
    ui->setScreenBlack->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/screenBlack.png")));
    ui->setScreenEmpty->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/customIcons/screenTheme.png")));
#endif

#ifndef Q_OS_LINUX
    ui->videoPlayStopButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/media-playback-start.png")));
#endif

#ifdef Q_OS_MAC
    QShortcut* down = new QShortcut(this);
    down->setKey(Qt::Key_Down);
    connect(down, &QShortcut::activated, [this]() {
        nextSlide();
    });
    QShortcut* right = new QShortcut(this);
    right->setKey(Qt::Key_Right);
    connect(right, &QShortcut::activated, [this]() {
        nextSlide();
    });
    QShortcut* up = new QShortcut(this);
    up->setKey(Qt::Key_Up);
    connect(up, &QShortcut::activated, [this]() {
        prevSlide();
    });
    QShortcut* left = new QShortcut(this);
    left->setKey(Qt::Key_Left);
    connect(left, &QShortcut::activated, [this]() {
        prevSlide();
    });
#endif

    // setup output window
    outputWindow->setTheme(t);
    outputWindow->show();
    outputWindow->setToScreen(QGuiApplication::screens().last());
    outputWindow->restore();

    connect(outputWindow, &OutputWindow::playerUpdate, [this](qint64 position, qint64 duration) {
        updatePreview();
        // ui->videoPlayStopButton->setIcon(QIcon::fromTheme("media-playback-pause"));
        ui->videoPosFull->setText(QString::number(duration / 60) + ":" + QString::number(duration % 60));
        ui->videoPosLabel->setText(QString::number(position / 60) + ":" + QString::number(position % 60));
        ui->videoPosSlider->setMaximum(int(duration));
        ui->videoPosSlider->setValue(int(position));
        network->updateVideoTime(position);
    });

    ui->VideoControlsFrame->hide();

    ui->libraryTabWidget->setCurrentIndex(0);
    ui->LibraryToolbarWidget->setCurrentIndex(0);

    setWindowTitle("Lyrics Projector " + QString(APP_VERSION) + " (" + QSysInfo::productType() + " " + QSysInfo::currentCpuArchitecture() + ")");
}

/** Save config to ~/.LyricsProjector/config.cfg */
void MainWindow::saveConfig() {
    QFile file(QDir::homePath() + "/.LyricsProjector/config.cfg");
    if (file.exists()) file.remove();

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out << "screen: " << cfg->outScreenIndex();
        foreach(QString img, *cfg->autoAddImages())
            out << "\nautostart-image: " << img;
        foreach(QString pdf, *cfg->autoAddPDFs())
            out << "\nautostart-pdf: " << pdf;
        foreach(QString vid, *cfg->autoAddVideos())
            out << "\nautostart-video: " << vid;
        out << "\nautostart-sources-images: " << cfg->autoAddSrcImages();
        out << "\nautostart-sources-pdfs: " << cfg->autoAddSrcPDFs();
        out << "\nautostart-sources-videos: " << cfg->autoAddSrcVideos();
        out << "\nnetwork-remote-port: " << network->remoteRecieverPort();
        out << "\nnetwork-control-1-port: " << network->slideControlPort();
        out << "\nnetwork-control-2-port: " << network->slideContentsPort();
        out << "\nnetwork-control-3-port: " << network->slideNextPort();
        out << "\nnetwork-http-server-port: " << network->httpServerPort();
        file.close();
    }
}

/** Load config from ~/.LyricsProjector/config.cfg */
void MainWindow::loadConfig() {
    QFile file(QDir::homePath() + "/.LyricsProjector/config.cfg");

    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QString f = file.readAll();
        file.close();
        QStringList lines = f.split("\n");
        foreach(QString line, lines) {
            if (line == "") continue;
            QString var = line.split(": ").at(0);
            QString val = line.split(": ").at(1);
            if (var == "screen") {
                cfg->setOutScreenIndex(val.toInt());
                if (cfg->outScreenIndex() == -1 || cfg->outScreenIndex() >= QGuiApplication::screens().size())
                    outputWindow->setToScreen(nullptr);
                else
                    outputWindow->setToScreen(QGuiApplication::screens().at(cfg->outScreenIndex()));
            }
            if (var == "autostart-image") cfg->autoAddImages()->append(val);
            if (var == "autostart-pdf") cfg->autoAddPDFs()->append(val);
            if (var == "autostart-video") cfg->autoAddVideos()->append(val);
            if (var == "autostart-sources-images") cfg->setAutoAddSrcImages(val == "1");
            if (var == "autostart-sources-pdfs") cfg->setAutoAddSrcPDFs(val == "1");
            if (var == "autostart-sources-videos") cfg->setAutoAddSrcVideos(val == "1");
            if (var == "network-remote-port") network->setRemoteRecieverPort(val.toInt());
            if (var == "network-control-1-port") network->setSlideControlPort(val.toInt());
            if (var == "network-control-2-port") network->setSlideContentsPort(val.toInt());
            if (var == "network-control-3-port") network->setSlideNextPort(val.toInt());
            if (var == "network-http-server-port") network->setHttpServerPort(val.toInt());
        }
    }
    network->start();
}

/** Update preview image (right bottom corner on the main window)*/
void MainWindow::updatePreview() {
    if (!outputWindow->usePreview()) return;

    if (ui->preview->width() < ui->preview->height())
        ui->preview->setPixmap(outputWindow->preview().scaledToWidth(ui->preview->width(), Qt::TransformationMode::SmoothTransformation));
    else
        ui->preview->setPixmap(outputWindow->preview().scaledToHeight(ui->preview->height(), Qt::TransformationMode::SmoothTransformation));

    /*if(outputWindow->videoPlaying()){
        QPixmap px = outputWindow->preview();
        network->sentImage(&px);
    }*/
}

/** Update toolbars sizes (system specyfic adjustments) */
void MainWindow::updateToolbar() {
    // update toolbars sizes

#ifdef Q_OS_WIN
    ui->MainToolbarWidget->setMaximumWidth(width() - ui->frame->geometry().x() - 5);
    ui->MainToolbarWidget->setMinimumWidth(width() - ui->frame->geometry().x() - 5);
    ui->LibraryToolbarWidget->setMaximumWidth(ui->libraryTabWidget->geometry().width() - 5);
    ui->LibraryToolbarWidget->setMinimumWidth(ui->libraryTabWidget->geometry().width() - 5);
#else
    int x, y, w, h;
    ui->toolBar->layout()->getContentsMargins(&x, &y, &w, &h);
    if (x == 2) x = -2;          // fix for only-the-Lord-wknows-what

    ui->MainToolbarWidget->setMaximumWidth(width() - ui->frame->geometry().x() - x / 2);
    ui->MainToolbarWidget->setMinimumWidth(width() - ui->frame->geometry().x() - x / 2);
    ui->LibraryToolbarWidget->setMaximumWidth(ui->libraryTabWidget->geometry().width() - x / 2);
    ui->LibraryToolbarWidget->setMinimumWidth(ui->libraryTabWidget->geometry().width() - x / 2);
#endif
}

/** Open context menu for plan element on plan list widget */
void MainWindow::on_planListWidget_customContextMenuRequested(const QPoint& pos) {
    if (ui->planListWidget->currentItem() == nullptr) return;
    // list item is rightclicked - popup menu held within plan element paired with current list item
    planElementList[ui->planListWidget->currentItem()]->rmbMenu()->popup(ui->planListWidget->mapToGlobal(pos));
}

/** Trigger plan element when list item is doubleclicked */
void MainWindow::on_planListWidget_itemDoubleClicked(QListWidgetItem* item) {
    // item is doubleclicked - trigger plan element paired with provided item
    planElementList[item]->trigger();
}

/** Set screen mode to normal (restore) */
void MainWindow::on_setScreenNormal_clicked() {
    ui->setScreenNormal->setChecked(true);
    ui->setScreenBlack->setChecked(false);
    ui->setScreenEmpty->setChecked(false);
    network->sentRestore();
    outputWindow->restore();
    updatePreview();
}

/** Set screen mode to black */
void MainWindow::on_setScreenBlack_clicked(bool checked) {
    black(checked);
}

/** Set screen mode to empty */
void MainWindow::on_setScreenEmpty_clicked(bool checked) {
    ui->setScreenNormal->setChecked(false);
    ui->setScreenBlack->setChecked(false);
    if (checked) {
        network->sentColor();
        outputWindow->setEmpty();
    } else {
        ui->setScreenNormal->setChecked(true);
        network->sentRestore();
        outputWindow->restore();
    }
    updatePreview();
}

/** Previous item button clicked */
void MainWindow::on_prewButton_clicked() {
    prevSlide();
}

/** Next item button clicked */
void MainWindow::on_nextButton_clicked() {
    nextSlide();
}

/** Go back button clicked */
void MainWindow::on_historyGoBack_clicked() {
    if (history.isEmpty()) return;

    history.pop()->trigger();
}
