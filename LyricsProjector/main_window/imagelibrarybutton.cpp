/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "imagelibrarybutton.h"

#include <QApplication>
#include <QMouseEvent>
#include <QPainter>
#include <QThread>
#include <QtMultimedia>

/** Get image. */
QPixmap* ImageLibraryButton::image() const {
    return image_;
}

/** Get name. */
QString ImageLibraryButton::name() const {
    return name_;
}

/** Get filename. */
QString ImageLibraryButton::filename() const {
    return filename_;
}

/** Set autoloaded.
 * @param autoloaded if true, the item will be marked as autoloaded
 */
void ImageLibraryButton::setAutoloaded(bool autoloaded) {
    isAutoloaded_ = autoloaded;
}

/** Set video length.
 * @param newVidLength video length in seconds
 */
void ImageLibraryButton::setVidLength(const int& newVidLength) {
    int sec = newVidLength % 60;
    int min = newVidLength / 60;

    if (sec > 9)
        length_ = QString::number(min) + ":" + QString::number(sec);
    else
        length_ = QString::number(min) + ":0" + QString::number(sec);
}

/** Set slide number.
 * @param newSlideNumber number of slides
 */
void ImageLibraryButton::setSlideNumber(const int& newSlideNumber) {
    if (newSlideNumber == 1)
        length_ = QString::number(newSlideNumber) + " slajd";
    else
        length_ = QString::number(newSlideNumber) + " slajdów";
}

/** Set image.
 * @param newImage image
 */
void ImageLibraryButton::setImage(QPixmap* newImage) {
    image_ = newImage;
}

void ImageLibraryButton::setArrowOnMouseOver(bool arrowOnMouseOver) {
    this->arrowOnMouseOver = arrowOnMouseOver;
}

void ImageLibraryButton::setVideoPreview(QString filename) {

    QMediaPlayer* player = new QMediaPlayer;

#ifdef Q_OS_WINDOWS
    QVideoWidget* videoWidget = new QVideoWidget;
    player->setVideoOutput(videoWidget);
#endif

    player->setMedia(QUrl::fromLocalFile(filename));
#ifdef Q_OS_WINDOWS
    player->play();
#endif

    connect(player, &QMediaPlayer::mediaStatusChanged, [this, player]() {
        setVidLength(player->duration() / 1000);
        repaint();
#ifndef Q_OS_WINDOWS
        player->deleteLater();
#endif
    });

    QVideoProbe* videoProbe = new QVideoProbe();

    if (videoProbe->setSource(player)) {
#ifdef Q_OS_WINDOWS
        connect(videoProbe, &QVideoProbe::videoFrameProbed, [this, videoProbe, videoWidget, player](QVideoFrame frame) {
#else
        connect(videoProbe, &QVideoProbe::videoFrameProbed, [this, videoProbe](QVideoFrame frame) {
#endif
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
            QImage imgbuf = qt_imageFromVideoFrame(frame);
#else
            QImage imgbuf = frame.image();
#endif
            delete image();
            setImage(new QPixmap(QPixmap::fromImage(imgbuf)));
            repaint();
            videoProbe->deleteLater();
#ifdef Q_OS_WINDOWS
            player->deleteLater();
            videoWidget->deleteLater();
#endif
        });

    }
}

/**
 * @param imageFile path to image
 * @param name name of image
 * @param parent parent widget (inherited from QPushButton, can be nullptr)
 */
ImageLibraryButton::ImageLibraryButton(QString imageFile, QString name, QWidget * parent) : QPushButton(parent) {
    // set filename as image
    filename_ = imageFile;

    QThread* thread = new QThread();
    connect(thread, &QThread::started, [this, imageFile, name]() {
        if (imageFile != "color") {
            image_->load(imageFile);
            if (image_->isNull()) {
                // if image loading failed (provided file is not an image, or filename is empty), set icon for file types (this would be video file)
                if (name.split(".").last() == "flv")
                    image_ = new QPixmap(QIcon::fromTheme("video-x-flv").pixmap(80, 50));
                else
                    image_ = new QPixmap(QIcon::fromTheme("video-" + name.split(".").last()).pixmap(80, 50));
            }
        }

        //load arrow icon
#ifndef Q_OS_LINUX
        addToListIcon = QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/arrow-add.png"));
        isAutoloadedIcon = QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/document-open-folder.png"));
#else
        addToListIcon = QIcon::fromTheme("arrow-right");
        isAutoloadedIcon = QIcon::fromTheme("document-open-folder");
#endif

    });
    thread->start();



    // If provided name is empty create name from filename, else set name to provided one
    if (name == "")
        name_ = imageFile.split("/").last();
    else
        name_ = name;

    //set button size and margin, to make it looke nice on the list
    setMinimumSize(100, 70);
    setStyleSheet("padding-bottom:10px");

    //setup RMB context menu
    QAction* add = new QAction("Dodaj do planu");
#ifndef Q_OS_LINUX
    add->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/arrow-right.png").scaledToWidth(24)));
#else
    add->setIcon(QIcon::fromTheme("arrow-right"));
#endif

    QAction* remove = new QAction("Usuń");
#ifndef Q_OS_LINUX
    remove->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png").scaledToWidth(24)));
#else
    remove->setIcon(QIcon::fromTheme("edit-delete"));
#endif


    // connect context menu actions with signals
    connect(add, &QAction::triggered, this, &ImageLibraryButton::addClicked);
    connect(remove, &QAction::triggered, this, &ImageLibraryButton::removeClicked);

    rmbMenu = new QMenu();
    rmbMenu->addAction(add);
    rmbMenu->addAction(remove);
}

ImageLibraryButton::~ImageLibraryButton() {
    delete image_;
    delete rmbMenu;
}

/** Override paint event.
 * @param event paint event
 */
void ImageLibraryButton::paintEvent(QPaintEvent * event) {
    // setup painter
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // if button is selected it should have it's background colored, and text color should be set to system's highlightedText color
    if (hasFocus()) {
        painter.setPen(QPen(palette().highlight(), 0));
        painter.setBrush(QBrush(palette().highlight()));
        painter.drawRect(0, 0, width(), height());
        painter.setPen(QPen(palette().highlightedText(), 0));
    }

    // setup font (bold text)
    QFont f(font());

    f.setBold(true);
    painter.setFont(f);

    // draw text and picture
    painter.drawPixmap(10, 10, 80, 50, image()->scaled(80, 50, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    if (length_ != "") {
        painter.drawText(100, 30, name());
        f.setBold(false);
        f.setItalic(true);
        painter.setFont(f);
        painter.drawText(100, 50, length_);
    } else {
        painter.drawText(100, 40, name());
    }

    // draw loaded from source icon if needed
    if (isAutoloaded_) {
        painter.setBrush(QBrush(palette().base()));
        painter.setPen(QPen(palette().base(), 0));
        painter.drawEllipse(4, 4, 20, 20);
        painter.drawPixmap(6, 6, 16, 16, isAutoloadedIcon.pixmap(16, 16));
    }

    // when button is under mouse, display arrow on the right
    if (underMouse() && arrowOnMouseOver) painter.drawPixmap(width() - 50, height() / 2 - 16, 32, 32, addToListIcon.pixmap(32, 32));

}

/** Override mouse release event.
 * @param event mouse release event
 */
void ImageLibraryButton::mouseReleaseEvent(QMouseEvent * event) {
    // mouse click event:
    if (event->button() == Qt::RightButton)
        rmbMenu->popup(event->globalPos());     // if right button, display context menu
    else {
        if (event->x() > width() - 50)
            emit addClicked();                  // mouse click ended on the right side of the button (or outside button) emit addClicked signal
        else
            emit selected();                    // otherwise emit selected signal
    }
}

/** Override mouse double click event.
 * @param event mouse double click event
 */
void ImageLibraryButton::mouseDoubleClickEvent(QMouseEvent * event) {
    // double click event - emit addClicked signal
    emit addClicked();
}
