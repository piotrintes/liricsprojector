/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

 /****************************************************************************
  ** This class represents main window, including lists, and library management.
  ** It's methods are implemented in following cpp files:
  ** mainwindow.cpp              // main functions and library management
  ** mainwindow_ui.cpp           // ui methods and slots
  ** mainwindow_menus.cpp        // menus management
  ** mainwindow_autostart.cpp    // start automation methods
  ** mainwindow_images.cpp       // images management
  ** mainwindow_pdf.cpp          // pdf management
  ** mainwindow_songs.cpp        // songs management
  ** mainwindow_videos.cpp       // videos management
  ** mainwindow_bible.cpp        // bible management
  ****************************************************************************/

#include "config/config.h"
#include "library/pdf.h"
#include "library/planelement.h"
#include "library/song.h"
#include "library/bcg.h"
#include "main_window/imagelibrarybutton.h"
#include "main_window/itemlistbutton.h"
#include "network/network.h"
#include "output_window/outputwindow.h"

#include <QListWidgetItem>
#include <QMainWindow>
#include <QStack>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
private:
    QList<Song*> librarySongs;                                                                              // list of songs in library
    QList<QString> libraryImages;                                                                           // list of images in library (filenames)
    QList<Pdf*> libraryPDFs;                                                                                // list of PDFs in library
    QList<Pdf*> libraryAutoloadPDFs;                                                                        // list of autoloaded PDFs
    QList<QString> libraryVideos;                                                                           // list of videos in library (filenames)
    QList<Bcg*> libraryBCGs;                                                                                // list of background images in library
    QList<Bcg*> libraryAutoBCGs;                                                                            // list of autoloaded background images
    QHash<QString, QList<QList<QString>>> bible;                                                            // this awesome object can store an entire bible :-)
    //
    Config* cfg = new Config();                                                                             // program configuration
    Network* network;                                                                                       // network interface
    //
    QHash<QListWidgetItem*, PlanElement*> planElementList;                                                  // hash table pairing plan list items with item-specyfic actions
    QStack<PlanElement*> history;                                                                           // history - work in progress
    OutputWindow* outputWindow = new OutputWindow;                                                          // output window class
    //
    ItemListButton* current = nullptr;                                                                      // item currently displayed on screen
    ItemListButton* jump = nullptr;                                                                         // item to which should jump at the end of the block
    //
    void initUI();                                                                                          // ininiate ui                              - mainwindow_ui.cpp
    void initMainMenu(Theme* t);                                                                            // build app main menu                      - mainwindow_menus.cpp
    void initSongMenu();                                                                                    // build song menu                          - mainwindow_menus.cpp
    void saveConfig();                                                                                      // save configuration (work in progress)    - mainwindow_ui.cpp
    void loadConfig();                                                                                      // load configuration (work in progress)    - mainwindow_ui.cpp
    void saveAutoloadDirs();                                                                                // save list of autloaded source dirs       - mainwindow.cpp
    void loadAutoloadDirs();                                                                                // load list of autloaded source dirs       - mainwindow.cpp
    void setupNetworking();                                                                                 // setup network interface                  - mainwindow.cpp
    //
    void saveLibraryFiles();                                                                                // save library to ~/.LyricsProjector       - mainwindow.cpp
    void loadLibraryFiles();                                                                                // load library from ~/.LyricsProjector     - mainwindow.cpp
    void loadFromSourceDir(QString dir);                                                                    // load library from custom source          - mainwindow.cpp
    //
    void loadBible(QString filename);                                                                       // load bible from xml file
    //
    void loadSongs();                                                                                       // load songs from ~/.LyricsProjector/Songs - mainwindow_songs.cpp
    void addSongToLib(Song* song);                                                                          // add song to library                      - mainwindow_songs.cpp
    void addSongToPlan(Song* song);                                                                         // add song to plan                         - mainwindow_songs.cpp
    void openSong(Song* song);                                                                              // open song                                - mainwindow_songs.cpp
    void removeSong(Song* song);                                                                             // remove song from library                 - mainwindow_songs.cpp
    //
    void addImageToLib(QString filename, bool noNotUpdateLibFile = false, bool autoloaded = false);         // add image to library - mainwindow_images.cpp
    void addImageToPlan(QPixmap* image, QString name);                                                      // add image to plan                        - mainwindow_images.cpp
    void removeImage(ImageLibraryButton* button);                                                           // remove image from library                - mainwindow_images.cpp
    //
    void addVideoToLib(QString filename, bool noNotUpdateLibFile = false, bool autoloaded = false);         // add video to library - mainwindow_videos.cpp
    void addVideoToPlan(QString filename, QString name);                                                    // add video to plan                        - mainwindow_videos.cpp
    void removeVideo(ImageLibraryButton* button, QString filename);                                         // remove video from library                - mainwindow_videos.cpp
    //
    void addPdfToLib(QString filename, bool noNotUpdateLibFile = false, bool autoloaded = false);           // add pdf to library   - mainwindow_pdf.cpp
    void addPdfToPlan(Pdf* pdf);                                                                            // add pdf to plan                          - mainwindow_pdf.cpp
    void openPdf(Pdf* pdf);                                                                                 // open pdf                                 - mainwindow_pdf.cpp
    void removePdf(Pdf* pdf);                                                                               // remove pdf from library                  - mainwindow_pdf.cpp
    //
    void addBcgToLib(QString source, bool noNotUpdateLibFile = false, bool autoloaded = false);             // add background to library                - mainwindow_bcg.cpp
    void updateAutoBcgs();                                                                                  // update automatically created backgrounds - mainwindow_bcg.cpp
    void removeFromPlan(PlanElement* element);                                                              // remove element from plan                 - mainwindow.cpp

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

    void nextSlide();
    void prevSlide();
    void black(int setBlack);

    QList<Pdf*> getPdfList();
    QList<Pdf*> getAutoloadPDFList();

private slots:
    void updatePreview();               // update screen preview                    - mainwindow_ui.cpp
    void updateToolbar();               // update toolbar                           - mainwindow_ui.cpp
    void updateBiblePreview();          // update bible preview                     - mainwindow_bible.cpp

    // ui widgets events - all are in mainwindow_ui.cpp
    void on_searchSongs_textChanged(const QString& arg1);

    void on_planListWidget_customContextMenuRequested(const QPoint& pos);
    void on_planListWidget_itemDoubleClicked(QListWidgetItem* item);

    void on_setScreenNormal_clicked();
    void on_setScreenBlack_clicked(bool checked);
    void on_setScreenEmpty_clicked(bool checked);

    void on_addImageButton_clicked();
    void on_addPdfButton_clicked();

    void on_addVideoButton_clicked();

    void on_prewButton_clicked();
    void on_nextButton_clicked();

    void on_historyGoBack_clicked();

    void on_BibleBookComboBox_currentIndexChanged(const QString& book);
    void on_BibleChapterComboBox_currentIndexChanged(int chapter);
    void on_BibleVerseFromComboBox_currentIndexChanged(int startVerse);
    void on_BibleVerseToComboBox_currentIndexChanged(int endVerse);
    void on_BibleAddToPlan_clicked();
    void on_BibleAddXML_clicked();

    void on_videoPlayStopButton_clicked();
    void on_videoPosSlider_sliderMoved(int position);

    void on_libraryTabWidget_currentChanged(int index);

protected:
    virtual void showEvent(QShowEvent* event);
    virtual void resizeEvent(QResizeEvent* event);
    virtual void dragEnterEvent(QDragEnterEvent* e);
    virtual void dropEvent(QDropEvent* e);

private:
    Ui::MainWindow* ui;

    void closeEvent(QCloseEvent* event);
};

#endif          // MAINWINDOW_H
