/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "imagelibrarybutton.h"
#include "itemlistbutton.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QMouseEvent>
#include <QProcess>

 /** Add image to library
  * @param filename path to image file
  * @param noNotUpdateLibFile if true, library file will not be updated (used for autoloaded images)
  * @param autoloaded if true, image button will have autoloaded icon
  */
void MainWindow::addImageToLib(QString filename, bool noNotUpdateLibFile, bool autoloaded) {
    QFile file(filename);
    if (!file.exists()) return;          // if image file does not exist function can't continue

    // create library button for image. This will be only object needed for image (it contains image itself) and add buttom on image lib list
    ImageLibraryButton* imgButton = new ImageLibraryButton(filename);

    imgButton->setAutoloaded(autoloaded);
    ui->imagesLibLayout->addWidget(imgButton);

    // when image button sends addClicked signal - execute lambda expression - add image to plan
    connect(imgButton, &ImageLibraryButton::addClicked, [this, imgButton]() {
        addImageToPlan(imgButton->image(), imgButton->name());
    });

    // when image button sends removeClicked signal - execute lambda expression - remove image from library
    connect(imgButton, &ImageLibraryButton::removeClicked, [this, imgButton]() {
        removeImage(imgButton);
    });

    // add image to image library list
    if (!autoloaded) libraryImages.append(filename);
    if (!noNotUpdateLibFile) saveLibraryFiles();

    // auto add to plan if selected
    if (cfg->autoAddImages()->contains(filename) || (cfg->autoAddSrcImages() && autoloaded))
        emit imgButton->addClicked();
}

/** Add image to plan
 * @param image image to add
 * @param name name of image file
 */
void MainWindow::addImageToPlan(QPixmap* image, QString name) {
    // create list item for plan list and plan elemet (context menu and lambda expressions)
    QListWidgetItem* imageListItem = new QListWidgetItem(QIcon::fromTheme("image-x-generic"), name);
#ifndef Q_OS_LINUX
    imageListItem->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/image-x-generic.png").scaledToWidth(24)));
#endif
    PlanElement* imagePlanElement = new PlanElement();

    // when plan element is triggered (list item double clicked) - execute lambda expression - display image on screen
    connect(imagePlanElement, &PlanElement::triggered, [this, image, name, imagePlanElement]() {
        // if video is played - ask for confirmation
        if (outputWindow->videoPlaying()) {
            if (QMessageBox::question(this, "Wymagane potwierdzenie.", "Odtwarzanie filmu zostanie przerwane. Czy na pewno chcesz kontynuować?") == QMessageBox::No) {
                return;
            }
        }

        // history - work in progress
        history.append(imagePlanElement);

        // next/previous buttons won't work for image - set current to nullptr
        current = nullptr;

        // setup ui - hide alert frame and edit button, set title
        ui->alertFrame->hide();
        ui->EditOpenedButton->hide();
        ui->ElementTitle->setText(name);

        // clear item list (middle)
        QLayoutItem* item;
        while ((item = ui->ItemsListLayout->takeAt(0)) != nullptr) {
            if (item->widget())
                delete item->widget();
            delete item;
        }

        // display image on screen and update preview
        network->sentImage(image);
        network->sentImageNext(nullptr);
        ui->VideoControlsFrame->hide();
        outputWindow->displayImage(image);
        updatePreview();
    });

    // setup RMB context menu
    QMenu* rmbMenu = new QMenu;

    QAction* remove = new QAction("Usuń z planu");
#ifndef Q_OS_LINUX
    remove->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png").scaledToWidth(24)));
#else
    remove->setIcon(QIcon::fromTheme("edit-delete"));
#endif
    rmbMenu->addAction(remove);

    // when remove action is triggered - execute lambda expression - remove element from plan
    connect(remove, &QAction::triggered, [this, imagePlanElement]() {
        removeFromPlan(imagePlanElement);
    });

    // set element menu
    imagePlanElement->setRmbMenu(rmbMenu);

    // pair list item with plan elemet (context menu and lambda expressions) and add list item to plan list
    planElementList.insert(imageListItem, imagePlanElement);
    ui->planListWidget->addItem(imageListItem);
}

/** Remove image from library
 * @param button button to remove
 */
void MainWindow::removeImage(ImageLibraryButton* button) {
    // remove image from list, then delete button and save new library
    libraryImages.removeOne(button->filename());
    delete button;
    saveLibraryFiles();
}

/** Add image button clicked: get image path and add it to library */
void MainWindow::on_addImageButton_clicked() {
    QString url = QFileDialog::getOpenFileName(nullptr, "Dodaj obraz do biblioteki", QString(), "Images (*.bmp *.jpg *.png *.xpm *.svg)");
    if (url != "") addImageToLib(url);
}
