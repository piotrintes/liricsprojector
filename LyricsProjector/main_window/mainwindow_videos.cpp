/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "imagelibrarybutton.h"
#include "itemlistbutton.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QMouseEvent>
#include <QFileDialog>
#include <QProcess>

#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
extern QImage qt_imageFromVideoFrame(const QVideoFrame& f);
#endif

/** Add video to library.
 * @param filename video file path
 * @param noNotUpdateLibFile if true, library file will not be updated (used for autoloaded videos)
 * @param autoloaded if true, video button will have autoloaded icon
 */
void MainWindow::addVideoToLib(QString filename, bool noNotUpdateLibFile, bool autoloaded) {
    QFile file(filename);
    if (!file.exists()) return;              // if video file does not exist function can't continue

    // create library button for video. This will be only object needed for video and add buttom on video lib list
    ImageLibraryButton* vidButton = new ImageLibraryButton("", filename.split("/").last());
    vidButton->setAutoloaded(autoloaded);
    ui->videosLibLayout->addWidget(vidButton);

    // get video length and image
    vidButton->setVideoPreview(filename);

    // when video button sends addClicked signal - execute lambda expression - add video to plan
    connect(vidButton, &ImageLibraryButton::addClicked, [this, filename]() {
        addVideoToPlan(filename, filename.split("/").last());
    });

    // when video button sends removeClicked signal - execute lambda expression - remove video from library
    connect(vidButton, &ImageLibraryButton::removeClicked, [this, vidButton, filename]() {
        removeVideo(vidButton, filename);
    });

    // add video to video library list
    if (!autoloaded) libraryVideos.append(filename);
    if (!noNotUpdateLibFile) saveLibraryFiles();

    // auto add to plan if selected
    if (cfg->autoAddVideos()->contains(filename) || (cfg->autoAddSrcVideos() && autoloaded))
        emit vidButton->addClicked();
}

/** Add video to plan
 * @param filename video file path
 * @param name video name
 */
void MainWindow::addVideoToPlan(QString filename, QString name) {
    // create list item for plan list and plan elemet (context menu and lambda expressions)
    QListWidgetItem* videoListItem = new QListWidgetItem(QIcon::fromTheme("video-x-generic"), name);
#ifndef Q_OS_LINUX
    videoListItem->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/video-x-generic.png")));
#endif
    PlanElement* videoPlanListItem = new PlanElement();
    network->addPlanVideo(name, filename);

    //when plan element is triggered (list item double clicked) - execute lambda expression - display video on screen
    connect(videoPlanListItem, &PlanElement::triggered, [this, name, filename, videoPlanListItem]() {
        // if video is played - ask for confirmation
        if (outputWindow->videoPlaying()) {
            if (QMessageBox::question(this, "Wymagane potwierdzenie.", "Odtwarzanie filmu zostanie przerwane. Czy na pewno chcesz kontynuować?")
                == QMessageBox::No) {
                return;
            }
            network->stopVideo();
        }

        // history - work in progress
        history.append(videoPlanListItem);

        // next/previous buttons won't work for image - set current to nullptr
        current = nullptr;

        // setup ui - hide alert frame and edit button, set title
        ui->alertFrame->hide();
        ui->EditOpenedButton->hide();
        ui->ElementTitle->setText(name);

        // clear item list (middle)
        QLayoutItem* item;
        while ((item = ui->ItemsListLayout->takeAt(0)) != nullptr) {
            if (item->widget())
                delete item->widget();
            delete item;
        }

        // display video on screen and update preview
        network->playVideo(filename, 0);
        outputWindow->displayVideo(filename);
        ui->VideoControlsFrame->show();
        updatePreview();
        network->sentImageNext(nullptr);
    });

    // setup RMB context menu
    QMenu* rmbMenu = new QMenu;

    QAction* remove = new QAction("Usuń z planu");
#ifndef Q_OS_LINUX
    remove->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-delete.png")));
#else
    remove->setIcon(QIcon::fromTheme("edit-delete"));
#endif
    rmbMenu->addAction(remove);

    // when remove action is triggered - execute lambda expression - remove element from plan
    connect(remove, &QAction::triggered, [this, videoPlanListItem, name]() {
        removeFromPlan(videoPlanListItem);
        network->removePlanElement(name);
    });

    // set element menu
    videoPlanListItem->setRmbMenu(rmbMenu);

    // pair list item with plan elemet (context menu and lambda expressions) and add list item to plan list
    planElementList.insert(videoListItem, videoPlanListItem);
    ui->planListWidget->addItem(videoListItem);
}

/** Remove video from library
 * @param button video button
 * @param filename video file path
 */
void MainWindow::removeVideo(ImageLibraryButton* button, QString filename) {
    // remove image from list, then delete button and save new library
    libraryVideos.removeOne(filename);
    delete button;
    saveLibraryFiles();
}

/** Add video button clicked: open file dialog and add video */
void MainWindow::on_addVideoButton_clicked() {
    QString url = QFileDialog::getOpenFileName(nullptr, "Dodaj video do biblioteki");
    if (url != "") addVideoToLib(url);
}

/** Video play button clicked: play/pause video */
void MainWindow::on_videoPlayStopButton_clicked() {
    outputWindow->videoPlayStop();
    //ui->videoPlayStopButton->setIcon(QIcon::fromTheme("media-playback-start"));
    if (outputWindow->videoPlaying()) {
        network->resumeVideo();
    } else {
        network->stopVideo();
    }
}

/** Video position slider moved: update video position */
void MainWindow::on_videoPosSlider_sliderMoved(int position) {
    network->updateVideoTime(position);
    outputWindow->setVideoPos(position);
    if (outputWindow->videoPlaying()) network->resumeVideo();
}
