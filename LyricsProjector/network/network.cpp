/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "network.h"
#include "qthread.h"
#include "../main_window/mainwindow.h"

#include <QBuffer>
#include <QDateTime>
#include <QNetworkDatagram>
#include <QPixmap>
#include <QTcpSocket>
#include <QWebSocket>

const QStringList& Network::getAddrList() const {
    return addrList;
}

int Network::slideControlPort() const {
    return slideControlPort_;
}

void Network::setSlideControlPort(int newSlideControlPort) {
    slideControlPort_ = newSlideControlPort;
}

int Network::slideContentsPort() const {
    return slideContentsPort_;
}

void Network::setSlideContentsPort(int newSlideContentsPort) {
    slideContentsPort_ = newSlideContentsPort;
}

quint16 Network::remoteRecieverPort() const {
    return remoteRecieverPort_;
}

void Network::setRemoteRecieverPort(quint16 newRemoteRecieverPort) {
    remoteRecieverPort_ = newRemoteRecieverPort;
}

int Network::slideNextPort() const {
    return slideNextPort_;
}

void Network::setSlideNextPort(int newSlideNextPort) {
    slideNextPort_ = newSlideNextPort;
}

void Network::sendContentsData(QByteArray data) {
    foreach(QTcpSocket * socket, socketListContents)
        send(socket, data + "eot");
}

void Network::sendNextData(QByteArray data) {
    foreach(QTcpSocket * socket, socketListNext)
        send(socket, data + "eot");
}

void Network::send(QTcpSocket* socket, QByteArray data) {
    // if socket is not connected for some reason (network error or such) disconnect from client
    if (socket->state() == QTcpSocket::ConnectedState) {
        socket->write(data);
        socket->flush();
        /*if(!socket->waitForBytesWritten(1000)){
            // error while sending data, disconnect from client
            socket->disconnect();
        }*/
    } else {
        socket->disconnect();
    }
}

void Network::send(QWebSocket* socket, QString data) {
    // if socket is not connected for some reason (network error or such) disconnect from client
    if (socket->state() == QAbstractSocket::ConnectedState) {
        socket->sendTextMessage(data);
        socket->flush();
    } else {
        socket->disconnect();
    }
}

int Network::httpServerPort() const {
    return httpServerPort_;
}

void Network::setHttpServerPort(int httpServerPort) {
    httpServerPort_ = httpServerPort;
}

void Network::sendControlData(QByteArray data) {
    foreach(QTcpSocket * socket, socketListControl)
        send(socket, data);

    foreach(QWebSocket * socket, websocketList) {
        send(socket, data);
    }
}

Network::Network(MainWindow* mainWindow) {
    serverWebSck = new QWebSocketServer("lp-ctl", QWebSocketServer::NonSecureMode);
    window = mainWindow;
}

void Network::start() {
    serverNext->listen(QHostAddress::Any, slideNextPort_);
    serverContents->listen(QHostAddress::Any, slideContentsPort_);
    serverControl->listen(QHostAddress::Any, slideControlPort_);
    serverHttp->listen(QHostAddress::Any, httpServerPort_);
    serverWebSck->listen(QHostAddress::Any, wsServerPort_);

    connect(serverWebSck, &QWebSocketServer::newConnection, [this]() {
        QWebSocket* socket = serverWebSck->nextPendingConnection();
        websocketList.append(socket);

        connect(socket, &QWebSocket::disconnected, [this, socket]() {
            websocketList.removeOne(socket);
        });

        connect(socket, &QWebSocket::textMessageReceived, [this, socket](QString message) {
            if (message == "next") window->nextSlide();
            if (message == "prev") window->prevSlide();
            if (message == "black") window->black(-1);
        });
    });

    connect(serverHttp, &QTcpServer::newConnection, [this]() {
        QTcpSocket* socket = serverHttp->nextPendingConnection();
        connect(socket, &QTcpSocket::readyRead, [this, socket]() {
            // QThread *thread = new QThread();
            // connect(thread, &QThread::started, this, [&]() {
            httpRequest(socket, socket->readAll());
            //});
            // thread->start();
        });

        socketListHttp.append(socket);
        connect(socket, &QTcpSocket::disconnected, [this, socket]() {
            socketListHttp.removeOne(socket);
        });

        connect(socket, &QTcpSocket::errorOccurred, [this, socket](QAbstractSocket::SocketError error) {
            socketListHttp.removeOne(socket);
        });
    });

    connect(serverNext, &QTcpServer::newConnection, [this]() {
        QTcpSocket* socket = serverNext->nextPendingConnection();
        // check if this address is not alraldy listed (in case of uncontrolled reconnection)
        foreach(QTcpSocket * s, socketListNext) {
            if (s->peerAddress() == socket->peerAddress()) s->disconnect();
        }

        socketListNext.append(socket);
        connect(socket, &QTcpSocket::disconnected, [this, socket]() {
            socketListNext.removeOne(socket);
        });

        connect(socket, &QTcpSocket::errorOccurred, [this, socket](QAbstractSocket::SocketError error) {
            socketListNext.removeOne(socket);
        });
    });

    connect(serverContents, &QTcpServer::newConnection, [this]() {
        QTcpSocket* socket = serverContents->nextPendingConnection();          // check if this address is not alraldy listed (in case of uncontrolled reconnection)
        foreach(QTcpSocket * s, socketListContents) {
            if (s->peerAddress() == socket->peerAddress()) s->disconnect();
        }

        socketListContents.append(socket);
        connect(socket, &QTcpSocket::disconnected, [this, socket]() {
            socketListContents.removeOne(socket);
        });

        connect(socket, &QTcpSocket::errorOccurred, [this, socket](QAbstractSocket::SocketError error) {
            socketListControl.removeOne(socket);
        });
    });

    connect(serverControl, &QTcpServer::newConnection, [this]() {
        QTcpSocket* socket = serverControl->nextPendingConnection();          // check if this address is not alraldy listed (in case of uncontrolled reconnection)
        foreach(QTcpSocket * s, socketListControl) {
            if (s->peerAddress() == socket->peerAddress()) s->close();
        }

        socketListControl.append(socket);
        qDebug() << "add peer: " << QHostAddress(socket->peerAddress().toIPv4Address()).toString();
        addrList.append(QHostAddress(socket->peerAddress().toIPv4Address()).toString());
        // read version and answer to pings
        connect(socket, &QTcpSocket::readyRead, [this, socket]() {
            QByteArray datagram = socket->readLine();
            // qDebug() << "peer sends message: " << datagram;
            if (datagram == "ping") {
                // client requests ping answer
                socket->write("ping-response");
                socket->flush();
                // qDebug() << "responded to ping";
            } else {
                // client provided it's version
                QString addr = QHostAddress(socket->peerAddress().toIPv4Address()).toString();

                /*for(int i=0;i<addrList.length();i++) {
                    addrList.replace(i, "Client v" + datagram + " on " + addr);
                }*/
            }
        });

        connect(socket, &QTcpSocket::disconnected, [this, socket]() {
            qDebug() << "peer disconneted: " << QHostAddress(socket->peerAddress().toIPv4Address()).toString();
            socketListControl.removeOne(socket);
            addrList.removeOne(QHostAddress(socket->peerAddress().toIPv4Address()).toString());
        });

        connect(socket, &QTcpSocket::errorOccurred, [this, socket](QAbstractSocket::SocketError error) {
            qDebug() << QHostAddress(socket->peerAddress().toIPv4Address()).toString() << error;
            socketListControl.removeOne(socket);
        });
    });
}

void Network::playVideo(QString filename, long startTime) {
    lastUpdate = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    content = "video";
    lastVideo = filename;
    sendControlData("video#" + filename.split("/").last().toUtf8() + "#" + QString::number(startTime).toUtf8());

    /*foreach (QString addr, addrList) {
        qDebug() << "starting streamer for: " << addr;
        QProcess *process = new QProcess();
        process->start("ffmpeg -ss " + QString::number(startTime) + " -re -i \"" + filename
                               + "\" -f mpegts \"udp://" + addr + ":" + QString::number(vidStreamPortNumber) + "\"");
        streamers.append(process);
    }*/
}

void Network::resumeVideo() {
    playVideo(lastVideo, videoTime);
}

void Network::updateVideoTime(long videoTime) {
    this->videoTime = videoTime;
}

void Network::stopVideo() {
    sendControlData("videostop");
    /*foreach(QProcess *process, streamers) {
        process->kill();
        streamers.removeOne(process);
    }*/
}

void Network::addPlanVideo(QString id, QString filename) {
    videoList.insert(id, filename);
}

void Network::removePlanElement(QString id) {
    if (videoList.contains(id)) videoList.remove(id);
}

void Network::sentSong(Song* song, SongItem item) {
    lastUpdate = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    sendControlData("song");
    sendContentsData(item.text().toUtf8());
    currentSong = song;
    content = "song";
    if (currentItem != nullptr) delete currentItem;
    currentItem = new SongItem(item);
}

void Network::sentImage(QPixmap* image) {
    bool scaled = false;
    QPixmap scaledImage;
    if (image->width() > 1920 || image->height() > 1080) {
        scaled = true;
        scaledImage = QPixmap(*image);
        if (image->width() > 1920) {
            scaledImage = scaledImage.scaledToWidth(1920, Qt::SmoothTransformation);
        }
        if (image->height() > 1080) {
            scaledImage = scaledImage.scaledToHeight(1080, Qt::SmoothTransformation);
        }
    }

    lastUpdate = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    QByteArray bytes;
    QBuffer* buffer = new QBuffer(&bytes);
    buffer->open(QIODevice::WriteOnly);
    if (scaled)
        scaledImage.save(buffer, "JPG");
    else
        image->save(buffer, "JPG");

    content = "image";

    sendControlData("image");
    sendContentsData(bytes);

    currentImage = bytes;

    delete buffer;
}

void Network::sentSongNext(QString item) {
    sendNextData(item.toUtf8());
}

void Network::sentImageNext(QPixmap* image) {
    // scale image (experimental)
    QPixmap scaledImage;
    if (image != nullptr && (image->width() > 768 || image->height() > 432)) {
        scaledImage = QPixmap(*image);
        if (image->width() > 768) {
            scaledImage = scaledImage.scaledToWidth(768, Qt::SmoothTransformation);
        }
        if (image->height() > 432) {
            scaledImage = scaledImage.scaledToHeight(432, Qt::SmoothTransformation);
        }
    }

    QByteArray bytes;
    QBuffer* buffer = new QBuffer(&bytes);
    buffer->open(QIODevice::WriteOnly);

    if (image != nullptr)
        scaledImage.save(buffer, "JPG");
    else {
        QPixmap img(1, 1);
        img.fill(QColor(0, 0, 0));
        img.save(buffer, "JPG");
    }

    sendNextData(bytes);

    nextImage = bytes;

    delete buffer;
}

void Network::sentBlack() {
    lastUpdate = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    sendControlData("black");
    display = "black";
}

void Network::sentColor() {
    lastUpdate = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    sendControlData("color");
    display = "empty";
}

void Network::sentRestore() {
    lastUpdate = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    sendControlData("restore");
    display = "image";
}
