/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "networkmanager.h"
#include "ui_networkmanager.h"

#include <QClipboard>
#include <QMessageBox>
#include <qnetworkinterface.h>

NetworkManager::NetworkManager(Network *network, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::NetworkManager)
{
    ui->setupUi(this);

    QIcon warningIcon;
    #ifndef Q_OS_LINUX
        warningIcon = QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/dialog-warning.png"));
        ui->copy_IP_Button->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/edit-copy.png")));
        ui->toolBar->layout()->setMargin(4);
    #else
        warningIcon = QIcon::fromTheme("dialog-warning");
    #endif
    ui->WarningIcon->setPixmap(warningIcon.pixmap(48,48));

    this->network = network;

    ui->spinBox->setValue(network->remoteRecieverPort());
    ui->spinBox_2->setValue(network->slideControlPort());
    ui->spinBox_3->setValue(network->slideContentsPort());
    ui->spinBox_4->setValue(network->slideNextPort());
    ui->spinBox_5->setValue(network->httpServerPort());

    ui->IP_lineEdit->setText("");

    ui->toolBar->addWidget(ui->ip_widget);

    QList<QHostAddress> list = QNetworkInterface::allAddresses();

    for(int nIter=0; nIter<list.count(); nIter++)
        if(!list[nIter].isLoopback() && (list[nIter].protocol() == QAbstractSocket::IPv4Protocol)) {
            if (ui->IP_lineEdit->text() != "") ui->IP_lineEdit->setText(ui->IP_lineEdit->text().append(", "));
            ui->IP_lineEdit->setText(ui->IP_lineEdit->text().append(list[nIter].toString()));
        }


    loadClientList();
}

NetworkManager::~NetworkManager()
{
    delete ui;
}

void NetworkManager::loadClientList()
{
    ui->listWidget->clear();

    foreach (QString address, network->getAddrList()) {
        ui->listWidget->addItem(address);
    }
}

void NetworkManager::on_abortButton_clicked()
{
    delete this;
}


void NetworkManager::on_reloadButton_clicked()
{
    loadClientList();
}


void NetworkManager::on_saveButton_clicked()
{
    network->setRemoteRecieverPort(ui->spinBox->value());
    network->setSlideControlPort(ui->spinBox_2->value());
    network->setSlideContentsPort(ui->spinBox_3->value());
    network->setSlideNextPort(ui->spinBox_4->value());
    network->setHttpServerPort(ui->spinBox_5->value());

    emit accepted();

    QMessageBox::information(this,"Konfiguracja została zapisana","Zmiany zostaną wprowadzone przy ponownym uruchomieniu programu.");

    delete this;
}


void NetworkManager::on_copy_IP_Button_clicked()
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    clipboard->setText(ui->IP_lineEdit->text());
}
