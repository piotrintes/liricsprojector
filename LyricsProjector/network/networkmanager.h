/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

/****************************************************************************
 ** This class is a network manager window.
****************************************************************************/

#include <QMainWindow>
#include "network.h"

namespace Ui {
class NetworkManager;
}

class NetworkManager : public QMainWindow
{
    Q_OBJECT
private:
    Network *network;

public:
    explicit NetworkManager(Network *network, QWidget *parent = nullptr);
    ~NetworkManager();

    void loadClientList();

private slots:
    void on_abortButton_clicked();
    void on_reloadButton_clicked();
    void on_saveButton_clicked();

    void on_copy_IP_Button_clicked();

signals:
    void accepted();

private:
    Ui::NetworkManager *ui;
};

#endif // NETWORKMANAGER_H
