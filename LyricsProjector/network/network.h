/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#ifndef NETWORK_H
#define NETWORK_H

 /****************************************************************************
  ** This class is network interface for LP Client.
  ****************************************************************************/

#include <QList>
#include <QProcess>
#include <QString>
#include <QStringList>
#include <QTcpServer>
#include <QUdpSocket>
#include <QWebSocketServer>

#include <library/song.h>

  // declare the mainwindow class for forward references
class MainWindow;

class Network : public QObject {
    Q_OBJECT
private:
    MainWindow* window;
    QStringList addrList;                                      // list of IP addresses of all peers connected to control server
    //
    QUdpSocket* socket = new QUdpSocket();                     // UDP socket, recieve remote commands
    QTcpServer* serverControl = new QTcpServer();              // TCP server, send commands
    QTcpServer* serverContents = new QTcpServer();             // TCP server, send slide contents
    QTcpServer* serverNext = new QTcpServer();                 // TCP server, send next slide preview
    QTcpServer* serverHttp = new QTcpServer();                 // TCP server, http server
    QWebSocketServer* serverWebSck = nullptr;                  // WS server, commands for web endpoints
    QList<QTcpSocket*> socketListControl;                     // list of command TCP sockets (one for each peer)
    QList<QTcpSocket*> socketListContents;                    // list of slide contents TCP sockets (one for each peer)
    QList<QTcpSocket*> socketListNext;                        // list of next slide preview TCP sockets (one for each peer using that feature)
    QList<QTcpSocket*> socketListHttp;                        // list of http TCP sockets (one for each peer)
    QList<QWebSocket*> websocketList;                         // list of ws sockets
    //
    int slideControlPort_ = 50100;                             // TCP port used to send commands
    int slideContentsPort_ = 50200;                            // TCP port used to send slide contents
    int slideNextPort_ = 50300;                                // TCP port used to send next slide preview
    quint16 remoteRecieverPort_ = 50000;                       // UDP port used to recieve remote commands
    int httpServerPort_ = 50400;                               // TCP port used by http server
    int wsServerPort_ = 50500;                                 // TCP port used by ws server
    //
    QString lastVideo;                                         // name of last played video file
    long videoTime;                                            // time of video being played
    QHash<QString, QString> videoList;                         // list of videos in plan, used to provide urls for pre-buffering
    //
    Song* currentSong = nullptr;                               //
    SongItem* currentItem = nullptr;                           //
    QByteArray display = "image";                              //
    QByteArray content = "image";                              //
    QByteArray currentImage;                                   //
    QByteArray nextImage;                                      //
    int lastUpdate = 0;                                        //
    //
    void sendControlData(QByteArray data);                     // send command to all peers
    void sendContentsData(QByteArray data);                    // send slide contents to all pears
    void sendNextData(QByteArray data);                        // send slide preview to all peers using that feature
    void send(QTcpSocket* socket, QByteArray data);            //
    void send(QWebSocket* socket, QString data);               //
    //
public:                                                        //
    Network(MainWindow* mv);                                   //
    //
    void start();                                              // start network services
    //
    void playVideo(QString filename, long startTime);          // have peers play video
    void resumeVideo();                                        // have peers resume playing video
    void updateVideoTime(long videoTime);                      // have peers update their video playback time
    void stopVideo();                                          // have peers stop playing video
    void addPlanVideo(QString id, QString filename);           // add video to plan list
    void removePlanElement(QString id);                        // remove element from plan list (if exists)
    //
    void sentSong(Song* song, SongItem item);                  // have peers display song
    void sentImage(QPixmap* image);                            // have peers display image (used both for image and for PDF)
    //
    void sentSongNext(QString item);                           // have peers using that feature update next song slide preview
    void sentImageNext(QPixmap* image);                        // have peers using that feature update next image slide preview (next PDF slide)
    //
    void sentBlack();                                          // have peers display black screen
    void sentColor();                                          // have peers display solid color
    void sentRestore();                                        // have peers resture image (from black screen or solid color)
    //
    const QStringList& getAddrList() const;                    // returns peer IP list
    int slideControlPort() const;
    void setSlideControlPort(int newSlideControlPort);
    int slideContentsPort() const;
    void setSlideContentsPort(int newSlideContentsPort);
    int slideNextPort() const;
    void setSlideNextPort(int newSlideNextPort);
    quint16 remoteRecieverPort() const;
    void setRemoteRecieverPort(quint16 newRemoteRecieverPort);
    int httpServerPort() const;
    void setHttpServerPort(int httpServerPort);

    void httpRequest(QTcpSocket* socket, QByteArray httpRequest);
};

#endif          // NETWORK_H
