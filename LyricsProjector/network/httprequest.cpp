/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
 ****************************************************************************/
#include "network.h"
#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QTcpSocket>
#include <QBuffer>
#include "../main_window/mainwindow.h"

QString getMimeString(QString filename) {
    if (filename.right(3) == "jpg")
        return ("image/jpeg");
    else if (filename.right(3) == "png")
        return ("image/png");
    else if (filename.right(3) == "svg")
        return ("image/svg+xml");
    else if (filename.right(3) == "video")
        return ("video/mp4");
    else if (filename.right(3) == "htm")
        return ("text/html");
    else if (filename.right(3) == "html")
        return ("text/html");
    else if (filename.right(3) == "html")
        return ("text/html");
    else if (filename.right(3) == "css")
        return ("text/css");
    else if (filename.right(2) == "js")
        return ("text/javascript");
    else
        return ("text/plain");
}

QString convertDiacritics(QString url) {
    QString w = url;

    // convert polish characters back from URL
    w.replace("%C4%85", "ą");
    w.replace("%C4%87", "ć");
    w.replace("%C4%99", "ę");
    w.replace("%C5%82", "ł");
    w.replace("%C5%84", "ń");
    w.replace("%C3%B3", "ó");
    w.replace("%C5%9B", "ś");
    w.replace("%C5%BC", "ż");
    w.replace("%C5%BA", "ź");

    return w;
}

void Network::httpRequest(QTcpSocket* socket, QByteArray httpRequest) {
    QByteArray cmd = httpRequest;
    bool sent = false;

    if (cmd.left(3) == "GET") {
        QByteArray request = cmd.split(' ').at(1);
        if (request == "/") request = "/index.htm";

        qDebug() << "GET " << request;

        QByteArray body;
        if (request == "/ver") {          ////////////////////  /ver
            body = "{\"app\":\"Lyrics Projector\",\"version\":\"" + QString(APP_VERSION).toUtf8() + "\"}\r\n";

        } else if (request == "/null") {          ////////////////////  /null
            body = "\r\n";

        } else if (request == "/content") {          ////////////////////  /content
            body = "{\"display\":\"" + display +
                "\",\"content\":\"" + content +
                "\",\"update\":\"" + QString::number(lastUpdate).toUtf8() + "\"}\r\n";

        } else if (request == "/videolist") {          ////////////////////  /videolist
            body = "[";
            foreach(QString filename, videoList) {
                body += "{\"id\":\"";
                body += videoList.key(filename).toUtf8();
                body += "\",\"url\":\"";
                body += filename.toUtf8();
                body += "\"},";
            }
            body.remove(body.length() - 1, 1);
            body += "]";

        } else if (request == "/pdflist") {
            QList<Pdf*> pdfList = window->getPdfList();
            QList<Pdf*> pdfListAutoloaded = window->getAutoloadPDFList();
            foreach(Pdf * pdf, pdfListAutoloaded) {
                pdfList.append(pdf);
            }

            body = "[";
            for (int i = 0; i < pdfList.count(); i++) {
                body += "{\"id\":\"";
                body += pdfList.at(i)->id().toUtf8();
                body += "\",\"name\":\"";
                body += pdfList.at(i)->filename().toUtf8();
                body += "\",\"pages\":\"";
                body += QString::number(pdfList.at(i)->slides.count()).toUtf8();
                body += "\"},";
            }
            body.remove(body.length() - 1, 1);
            body += "]";

        } else if (request.startsWith("/pdf/")) {          ////////////////////  /pdf (image by id.page)
            QString file = request.split('/').at(2);
            QString id = file.split('.').at(0);
            int page = file.split('.').at(1).toInt();

            if (page == 0) {
                socket->write("HTTP/1.1 404 OK\r\n\n");
                socket->write("file not found");
                socket->flush();
                return;
            }

            // get pdf list (with autoloaded pdfs)
            QList<Pdf*> pdfList = window->getPdfList();
            QList<Pdf*> pdfListAutoloaded = window->getAutoloadPDFList();
            foreach(Pdf * pdf, pdfListAutoloaded) {
                pdfList.append(pdf);
            }

            Pdf* pdf = nullptr;
            for (int i = 0; i < pdfList.count(); i++) {
                if (pdfList.at(i)->id() == id) {
                    pdf = pdfList.at(i);
                    break;
                }
            }

            if (pdf == nullptr) {
                socket->write("HTTP/1.1 404 OK\r\n\n");
                socket->write("file not found");
                socket->flush();
                return;
            }

            if (page > pdf->slides.count()) {
                socket->write("HTTP/1.1 404 OK\r\n\n");
                socket->write("file not found");
                socket->flush();
                return;
            }

            //QByteArray img = pdf->slides.at(page - 1)->toImage()
            QByteArray data;
            QBuffer* buffer = new QBuffer(&data);
            buffer->open(QIODevice::WriteOnly);
            pdf->slides.at(page - 1)->toImage().save(buffer, "JPG");

            socket->write("HTTP/1.1 200 OK\r\n");
            socket->write("Access-Control-Allow-Origin: *\r\n");
            socket->write("Content-Type: image/png\r\n");
            socket->write("Content-Length: " + QString::number(data.size()).toUtf8() + "\r\n");
            socket->write("\n");

            // send file
            while (data.size() > 0) {
                QByteArray fragment = data.left(4096);
                socket->write(fragment);
                socket->flush();
                if (data.size() - 4096 > 0)
                    data = data.right(data.size() - 4096);
                else
                    data = "";
            }
            socket->flush();
            sent = true;

        } else if (request == "/video/current") {          ////////////////////  /video/current
            if (currentSong == nullptr) {
                body = "{\"display\":\"false\"}\r\n";
            } else {
                body = "{\"display\":\"true\",\"video\":\"" +
                    lastVideo.toUtf8() + "\"}\r\n";
            }

        } else if (request == "/song/current") {          ////////////////////  /song/current
            if (currentSong == nullptr) {
                body = "{\"display\":\"false\"}\r\n";
            } else {
                body = "{\"display\":\"true\",\"text\":\"" +
                    currentItem->text().replace("\n", "<br>").replace("	", " ").toUtf8() + "\"}\r\n";
            }

        } else if (request == "/song/full") {          ////////////////////  /song/full
            if (currentSong == nullptr) {
                body = "{\"display\":\"false\"}\r\n";
            } else {
                body = "{\"title\":\"" + currentSong->title().toUtf8() + "\",\"items\":[";
                for (int i = 0; i < currentSong->items.count(); i++) {
                    body += ("{\"name\":\"" + currentSong->items.at(i).name() +
                        "\",\"text\":\"" + currentSong->items.at(i).text().replace("\n", "<br>").replace("	", " "))
                        .toUtf8();
                    body += "\"}";
                    if (i != currentSong->items.count() - 1) body += ",";
                }
                body += "]}\r\n";
            }

            ////////////////////  /song/current&lng=
        } else if (request.split('&').at(0) == "/song/current" && request.split('&').at(1).split('=').at(0) == "lng") {
            QString lng = request.split('&').at(1).split('=').at(1);
            if (currentSong == nullptr) {
                body = "{\"display\":\"false\"}\r\n";
            } else {
                body = "{\"display\":\"true\",\"text\":\"" +
                    currentItem->textLng(lng).replace("\n", "<br>").replace("	", " ").toUtf8() + "\"}\r\n";
            }

        } else if (request.split('&').at(0) == "/img") {          ////////////////////  /img
            QByteArray data = currentImage;

            socket->write("HTTP/1.1 200 OK\r\n");
            socket->write("Access-Control-Allow-Origin: *\r\n");
            socket->write("Content-Type: image/jpeg\r\n");
            socket->write("Content-Length: " + QString::number(currentImage.size()).toUtf8() + "\r\n");
            socket->write("\n");

            // send file
            while (data.size() > 0) {
                QByteArray fragment = data.left(4096);
                socket->write(fragment);
                socket->flush();
                if (data.size() - 4096 > 0)
                    data = data.right(data.size() - 4096);
                else
                    data = "";
            }
            socket->flush();
            sent = true;

        } else if (request.split('&').at(0) == "/img/next") {          ////////////////////  /img/next
            QByteArray data = nextImage;

            socket->write("HTTP/1.1 200 OK\r\n");
            socket->write("Access-Control-Allow-Origin: *\r\n");
            socket->write("Content-Type: image/jpeg\r\n");
            socket->write("Content-Length: " + QString::number(nextImage.size()).toUtf8() + "\r\n");
            socket->write("\n");

            // send file
            while (data.size() > 0) {
                QByteArray fragment = data.left(4096);
                socket->write(fragment);
                socket->flush();
                if (data.size() - 4096 > 0)
                    data = data.right(data.size() - 4096);
                else
                    data = "";
            }
            socket->flush();
            sent = true;

            ////////////////////  /filelocal&url=
        } else if (request.split('&').at(0) == "/filelocal" && request.split('&').at(1).split('=').at(0) == "url") {
            QString url = request.split('&').at(1).split('=').at(1);
            url = convertDiacritics(url);

            qDebug() << "requested file: " << url;

            // cut off everything after ? mark
            request = request.split('?').first();
            // read file
            QFile file(url);
            if (file.open(QIODevice::ReadOnly)) {
                // Header
                socket->write("HTTP/1.1 200 OK\r\n");
                socket->write("Access-Control-Allow-Origin: *\r\n");
                socket->write("Content-Type: " + getMimeString(request).toUtf8() + "\r\n");
                socket->write("Content-Length: " + QString::number(file.size()).toUtf8() + "\r\n");
                socket->write("\n");
                QByteArray data = file.readAll();
                QByteArray buffer = socket->readAll();

                // send file
                while (data.size() > 0) {
                    QByteArray fragment = data.left(4096);
                    socket->write(fragment);
                    socket->flush();
                    if (data.size() - 4096 > 0)
                        data = data.right(data.size() - 4096);
                    else
                        data = "";
                }
                sent = true;
            } else {
                socket->write("HTTP/1.1 404 OK\r\n\n");
                socket->write("file not found");
                socket->flush();
                sent = true;
            }

        } else {          ////////////////////  /file url
            // cut off everything after ? mark
            request = request.split('?').first();
            // read file
            QFile file(QApplication::applicationDirPath() + "/web" + request);
            if (file.open(QIODevice::ReadOnly)) {
                // Header
                socket->write("HTTP/1.1 200 OK\r\n");
                socket->write("Access-Control-Allow-Origin: *\r\n");
                socket->write("Content-Type: " + getMimeString(request).toUtf8() + "\r\n");
                socket->write("Content-Length: " + QString::number(file.size()).toUtf8() + "\r\n");
                socket->write("\n");
                QByteArray data = file.readAll();
                QByteArray buffer = socket->readAll();

                // send file
                while (data.size() > 0) {
                    QByteArray fragment = data.left(4096);
                    socket->write(fragment);
                    socket->flush();
                    if (data.size() - 4096 > 0)
                        data = data.right(data.size() - 4096);
                    else
                        data = "";
                }
                sent = true;
            } else {
                socket->write("HTTP/1.1 404 OK\r\n\n");
                socket->write("file not found");
                socket->flush();
                sent = true;
            }
        }

        if (!sent) {
            // send json
            socket->write("HTTP/1.1 200 OK\r\n");
            socket->write("Access-Control-Allow-Origin: *\r\n");
            socket->write("Content-Type: text/json\r\n");
            socket->write("Content-Length: " + QString::number(body.size()).toUtf8() + "\r\n\n");
            socket->write(body);
        }
    }
}
