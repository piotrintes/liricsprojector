
const { networkInterfaces } = require('os');
const asyncHandler = require('express-async-handler')
const ping = require('ping');
const axios = require('axios');
//const fetch = require("node-fetch");

async function fetchWithTimeout(resource, options = { timeout: 2000 }) {
     const { timeout = 2000 } = options;

     const controller = new AbortController();
     const id = setTimeout(() => controller.abort(), timeout);

     const response = await fetch(resource, {
          ...options,
          signal: controller.signal
     });
     clearTimeout(id);

     return response;
}

function getIpRange(address, mask) {
     // make address and mask into arrays
     address = address.split('.');
     mask = mask.split('.');

     // get network address
     for (let i = 0; i < 4; i++)
          address[i] = mask[i] == '255' ? Number(address[i]) : 0;

     // create list of possible addresses for the network
     let addrList = []
     let octet = [0, 0, 0, 0];
     do {
          octet[1] = 0;
          do {
               octet[2] = 0;
               do {
                    octet[3] = 0;
                    do {
                         let addr = [
                              mask[0] == '255' ? address[0] : octet[0],
                              mask[1] == '255' ? address[1] : octet[1],
                              mask[2] == '255' ? address[2] : octet[2],
                              mask[3] == '255' ? address[3] : octet[3],
                         ];
                         addrList.push(`${addr[0]}.${addr[1]}.${addr[2]}.${addr[3]}`);
                         octet[3]++;
                    } while (octet[3] < 255 && mask[3] != '255')
                    octet[2]++;
               } while (octet[2] < 255 && mask[2] != '255')
               octet[1]++;
          } while (octet[1] < 255 && mask[1] != '255')
          octet[0]++;
     } while (octet[0] < 255 && mask[0] != '255')

     return addrList;
}

function getRange(cidr, netmask) {
     // Split the IP address and netmask into arrays of octets
     const ipParts = cidr.split('/')[0].split('.');
     const netmaskParts = netmask.split('.');

     // Calculate the network address
     const networkAddressParts = ipParts.map((part, index) => {
          const ipPart = parseInt(part);
          const netmaskPart = parseInt(netmaskParts[index]);
          return ipPart & netmaskPart;
     });

     return networkAddressParts.join('.') + '/' + cidr.split('/')[1];
}

async function findServerIP(ipRange) {
     const hosts = [];
     const promises = [];

     // Ping each IP address in the range
     ipRange.forEach(addr => {
          hosts.push(addr);
          promises.push(ping.promise.probe(addr));
     });

     const results = await Promise.all(promises);

     // Filter out the active IP addresses
     const activeHosts = results.filter(result => result.alive).map(result => result.host);

     // Check for the server by sending an HTTP request to each active IP
     const serverIP = await checkServer(activeHosts);

     return serverIP;
}

async function checkServer(hosts) {
     const promises = [];

     for (const host of hosts) {
          promises.push(axios.get(`http://${host}:50400/ver`, { timeout: 5000 }).catch(error => { }))
     }

     const results = await Promise.all(promises);

     let serverIp = null;

     results.forEach(result => {
          if (result != undefined) serverIp = result.request.host;
     });

     return serverIp;
}

/** Get current IP addresses */
exports.getIpList = asyncHandler(async (req, res, next) => {
     let addrList = [];
     let nets = networkInterfaces();

     for (const name of Object.keys(nets)) {
          for (const net of nets[name]) {
               if (net.family == 'IPv4' && net.address != '127.0.0.1') addrList.push(net.address);
          }
     }

     res.send(addrList);
});


exports.findServer = asyncHandler(async (req, res, next) => {
     let nets = networkInterfaces();
     let serverAddr = "null"

     for (const name of Object.keys(nets)) {
          for (const net of nets[name]) {
               if (net.family == 'IPv4' && net.address != '127.0.0.1') {
                    serverAddr = await findServerIP(getIpRange(net.address, net.netmask));
               }
          }
     }

     res.send(serverAddr);
});