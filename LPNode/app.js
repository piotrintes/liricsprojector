const networkControler = require("./controller/network");

const hostname = '127.0.0.1';
const port = 3000;

var express = require('express')
var app = express()

app.get('/ver', function (req, res) {
     res.send(JSON.stringify({ version: '1.0.0' }));
})
app.get('/ip-list', (req, res) => networkControler.getIpList(req, res));
app.get('/svr-ip', (req, res) => networkControler.findServer(req, res));
app.use('/', express.static(__dirname + "/front/"));



app.listen(port, hostname, () => {
     console.log(`Server running at http://${hostname}:${port}/`);
});