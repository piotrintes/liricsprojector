import Server from "./endpoints/server.js";
import Settings from "./misc/settings.js";
import ContentManager from "./screen/contentManager.js";
import Song from "./screen/song.js";
import Image from "./screen/image.js";
import Video from "./screen/video.js";
import Black from "./screen/black.js";
import WelcomeScreen from "./screen/welcome.js"
import VideoBackground from "./screen/background/video.js";

/** This is a app main class, used to set up and instantiate application. */
class App {
     constructor() {
          this.settings = new Settings('LP.Node.Settings');
          this.server = new Server();


          this.screenArea = document.createElement('div');
          this.screenArea.className = 'screen-area fullscreen';
          document.body.appendChild(this.screenArea);

          this.background = document.createElement('div');
          this.background.className = 'fill';
          this.screenArea.appendChild(this.background);

          let videoBackground = new VideoBackground(this.background);
          videoBackground.show();
          videoBackground.loadVideo("Mermaid_HD.mp4")

          this.content = document.createElement('div');
          this.content.className = 'fill';
          this.screenArea.appendChild(this.content);

          let welcomeScreen = new WelcomeScreen(this.content, this.server);
          let songScreen = new Song(this.content, this.server, this.settings);
          let imageScreen = new Image(this.content, this.server, this.settings);
          let videoScreen = new Video(this.content, this.server, this.settings);
          let blackScreen = new Black(this.content);

          let contentManager = new ContentManager(this.server, songScreen, imageScreen, videoScreen, blackScreen, welcomeScreen);
          setTimeout(() => contentManager.connect(), 4000);
     }


}


export default App;