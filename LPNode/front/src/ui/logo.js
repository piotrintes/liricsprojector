
const logoSVG = `
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->
<svg
   style="
      --circle-fill: 18;
      --opacity: 1;
      --scale: 1;
      --scale2: 1;
   "
   width="512"
   height="128"
   viewBox="0 0 135.46665 33.866668"
   version="1.1"
   id="logo-animated-svg"
   inkscape:version="1.1.2 (0a00cf5339, 2022-02-04)"
   sodipodi:docname="logo.svg"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview7"
     pagecolor="#353535"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageshadow="2"
     inkscape:pageopacity="0"
     inkscape:pagecheckerboard="true"
     inkscape:document-units="mm"
     showgrid="false"
     units="px"
     width="128px"
     inkscape:zoom="1.5287037"
     inkscape:cx="191.33858"
     inkscape:cy="52.986069"
     inkscape:window-width="1920"
     inkscape:window-height="1044"
     inkscape:window-x="0"
     inkscape:window-y="36"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <defs
     id="defs2">
    <rect
       x="149.28464"
       y="18.029891"
       width="303.95685"
       height="96.575286"
       id="rect20304" />
    <linearGradient
       inkscape:collect="always"
       id="linearGradient15434">
      <stop
         style="stop-color:#ff5a4a;stop-opacity:var(--opacity);transition: 3s;"
         offset="0"
         id="stop15430" />
      <stop
         style="stop-color:#ff8a4c;stop-opacity:var(--opacity);transition: 1s;"
         offset="1"
         id="stop15432" />
    </linearGradient>
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient11174"
       x1="26.506628"
       y1="27.655256"
       x2="4.2216492"
       y2="5.1131549"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(31.183785,-29.68128)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient12961"
       gradientUnits="userSpaceOnUse"
       x1="3.7467117"
       y1="7.3907089"
       x2="10.83133"
       y2="4.0632777"
       gradientTransform="matrix(3.1814637,0,0,3.1814637,26.741646,-3.0733481)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient12963"
       gradientUnits="userSpaceOnUse"
       x1="3.7467117"
       y1="7.3907089"
       x2="10.83133"
       y2="4.0632777"
       gradientTransform="matrix(3.1814637,0,0,3.1814637,26.741646,-3.0733481)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient15662"
       x1="31.30434"
       y1="31.483503"
       x2="31.318514"
       y2="27.976347"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(18.899411,9.789295)" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient56632"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906"
       gradientUnits="userSpaceOnUse" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient839"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient841"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient843"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient15434"
       id="linearGradient845"
       gradientUnits="userSpaceOnUse"
       x1="171.5979"
       y1="58.692097"
       x2="332.76688"
       y2="7.474906" />
  </defs>
  <g
     inkscape:label="Warstwa 1"
     inkscape:groupmode="layer"
     id="layer1">
    <circle
       style="fill:none;stroke:url(#linearGradient11174);stroke-width:1.5875;stroke-linecap:round;stroke-miterlimit:4;stroke-dasharray:100;stroke-dashoffset: var(--circle-fill); transition: 1s;stroke-opacity:1"
       id="path846"
       transform="rotate(34.72762)"
       cx="48.117115"
       cy="-12.747945"
       r="14.282" />
    <path
       d="m 38.033643,22.231159 c -0.388779,0 -0.725223,-0.118106 -1.009331,-0.354309 -0.269155,-0.249328 -0.403731,-0.544582 -0.403731,-0.885775 v -8.364942 c 0,-0.269651 0.09719,-0.496725 0.291584,-0.681223 0.194391,-0.198695 0.433639,-0.298036 0.717748,-0.298036 0.284107,0 0.523357,0.09934 0.717746,0.298036 0.209344,0.184499 0.314014,0.411572 0.314014,0.681223 v 7.813791 h 6.148781 c 0.238181,0 0.438752,0.09186 0.601716,0.275575 0.175499,0.170593 0.263247,0.380555 0.263247,0.629885 0,0.249328 -0.08775,0.459291 -0.263247,0.629885 -0.162964,0.170593 -0.363536,0.255891 -0.601716,0.255891 z"
       style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;font-size:5.21147px;line-height:1.25;font-family:'Varela Round';-inkscape-font-specification:'Varela Round Bold';letter-spacing:0px;word-spacing:0px;fill:url(#linearGradient12961);fill-opacity:1;stroke:none;stroke-width:0.415;stroke-miterlimit:4;stroke-dasharray:none;scale:var(--scale);transition:0.7s;transform-origin:center;"
       id="path922"
       sodipodi:nodetypes="scsscscscscscss" />
    <path
       d="m 48.453793,22.270235 c -0.286026,0 -0.526891,-0.101355 -0.722593,-0.304059 -0.195701,-0.202705 -0.293553,-0.45219 -0.293553,-0.748454 v -8.947147 c 0,-0.18518 0.09785,-0.341122 0.293553,-0.467825 0.195702,-0.13645 0.436567,-0.204673 0.722594,-0.204673 h 4.460986 c 1.190895,0 2.162415,0.161828 2.91456,0.48549 0.752144,0.323661 1.291181,0.73835 1.617111,1.244069 0.325928,0.50572 0.488894,1.051896 0.488894,1.63853 0,0.586634 -0.162965,1.132811 -0.488894,1.63853 -0.325931,0.505719 -0.864968,0.920409 -1.617112,1.244069 -0.752144,0.323661 -1.723653,0.485491 -2.91456,0.485491 h -3.422257 v 2.883466 c 0,0.296264 -0.10538,0.545748 -0.316136,0.748454 -0.195702,0.202707 -0.436566,0.304059 -0.722593,0.304059 z M 52.745547,17.0295 c 1.278645,0 2.174951,-0.197228 2.688916,-0.591691 0.526501,-0.394461 0.789752,-0.885009 0.789752,-1.471643 0,-0.586634 -0.26325,-1.077182 -0.789752,-1.471642 -0.513965,-0.394461 -1.410271,-0.591692 -2.688916,-0.591692 H 49.492522 V 17.0295 Z"
       style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;font-size:5.21147px;line-height:1.25;font-family:'Varela Round';-inkscape-font-specification:'Varela Round Bold';letter-spacing:0px;word-spacing:0px;fill:url(#linearGradient12963);fill-opacity:1;stroke:none;stroke-width:0.415;stroke-miterlimit:4;stroke-dasharray:none;scale:var(--scale);transition:0.7s;transform-origin:center;"
       id="path924"
       sodipodi:nodetypes="sssscsssssssscscsscscsccs" />
    <circle
       style="fill:url(#linearGradient15662);fill-opacity:1;stroke:none;stroke-width:0.414999;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       id="path15538"
       cx="50.202858"
       cy="39.701466"
       r="1.6562569"
       transform="rotate(-18.527532)" />
    <g
       aria-label="node"
       transform="matrix(0.300912,0,0,0.300912,20.729436,2.2878499)"
       id="text20302">
      <path
         d="m 155.89849,65.627054 q -1.01334,0 -1.70667,-0.693333 -0.69333,-0.693333 -0.69333,-1.706666 V 40.347069 q 0,-1.013333 0.69333,-1.706666 0.69333,-0.746666 1.70667,-0.746666 1.01333,0 1.70666,0.746666 0.74667,0.693333 0.74667,1.706666 v 2.186665 q 2.02666,-2.239999 4.37333,-3.573331 2.34666,-1.333333 5.54666,-1.333333 2.93333,0 5.06667,1.386666 2.18666,1.386666 3.30666,3.786665 1.17333,2.399998 1.17333,5.279996 v 15.146658 q 0,1.013333 -0.74666,1.706666 -0.69334,0.693333 -1.70667,0.693333 -1.01333,0 -1.70666,-0.693333 -0.69334,-0.693333 -0.69334,-1.706666 V 48.61373 q 0,-6.506662 -5.86666,-6.506662 -2.66667,0 -4.8,1.386666 -2.08,1.386665 -3.94666,3.733331 v 15.99999 q 0,1.013333 -0.74667,1.706666 -0.69333,0.693333 -1.70666,0.693333 z"
         id="path836"
         style="fill:url(#linearGradient839);scale:var(--scale2);transition:1.5s;transform-origin:100% 150%;" />
      <path
         d="m 197.97842,65.89372 q -4.10666,0 -7.19999,-1.759999 -3.09333,-1.759999 -4.8,-4.959997 -1.65333,-3.253331 -1.65333,-7.413329 0,-4.159997 1.65333,-7.359995 1.70667,-3.253332 4.8,-5.013331 3.09333,-1.759999 7.19999,-1.759999 4.05333,0 7.14666,1.759999 3.09334,1.759999 4.8,5.013331 1.70667,3.199998 1.70667,7.359995 0,4.159998 -1.70667,7.413329 -1.70666,3.199998 -4.8,4.959997 -3.09333,1.759999 -7.14666,1.759999 z m 0,-4.426664 q 2.18667,0 4.16,-1.013333 2.02667,-1.013332 3.30666,-3.199998 1.33334,-2.186665 1.33334,-5.546663 0,-3.306665 -1.33334,-5.49333 -1.27999,-2.186665 -3.30666,-3.146665 -1.97333,-1.013333 -4.16,-1.013333 -2.24,0 -4.26666,1.013333 -1.97333,0.96 -3.25333,3.146665 -1.28,2.186665 -1.28,5.49333 0,3.359998 1.28,5.546663 1.28,2.186666 3.25333,3.199998 2.02666,1.013333 4.26666,1.013333 z"
         id="path838"
         style="fill:url(#linearGradient841);scale:var(--scale2);transition:1.8s;transform-origin:100% 150%;" />
      <path
         d="m 229.65839,65.89372 q -3.30666,0 -6.29333,-1.653332 -2.93333,-1.706666 -4.79999,-4.906664 -1.81334,-3.253331 -1.81334,-7.573329 0,-4.319997 1.81334,-7.519995 1.86666,-3.253332 4.79999,-4.906664 2.98667,-1.706666 6.29333,-1.706666 5.22667,0 9.12,4.053331 V 28.613742 q 0,-1.066666 0.69333,-1.759999 0.74667,-0.693332 1.76,-0.693332 1.01333,0 1.70666,0.746666 0.69334,0.693333 0.69334,1.706665 v 34.613313 q 0,1.013333 -0.69334,1.706666 -0.69333,0.693333 -1.70666,0.693333 -1.01333,0 -1.76,-0.693333 -0.69333,-0.693333 -0.69333,-1.706666 v -1.386666 q -3.89333,4.053331 -9.12,4.053331 z m 0.69333,-4.53333 q 2.45334,0 4.58667,-1.066666 2.18666,-1.12 3.84,-2.933332 V 46.160399 q -1.65334,-1.813333 -3.84,-2.879999 -2.13333,-1.119999 -4.58667,-1.119999 -3.94666,0 -6.34666,2.506665 -2.4,2.506665 -2.4,7.093329 0,4.586664 2.4,7.093329 2.4,2.506666 6.34666,2.506666 z"
         id="path840"
         style="fill:url(#linearGradient843);scale:var(--scale2);transition:2.1s;transform-origin:100% 150%;" />
      <path
         d="m 264.21829,65.89372 q -6.50666,0 -10.18666,-3.679998 -3.62666,-3.733331 -3.62666,-10.453327 0,-3.786664 1.38667,-6.986662 1.38666,-3.199998 4.26666,-5.17333 2.88,-1.973333 7.09333,-1.973333 3.94666,0 6.82666,1.866666 2.88,1.813332 4.37333,4.906664 1.49333,3.039998 1.49333,6.666662 0,1.013333 -0.69333,1.706666 -0.64,0.693333 -1.76,0.693333 h -18.07999 q 0.42667,3.733331 2.77333,5.81333 2.34667,2.079999 6.56,2.079999 2.24,0 3.89333,-0.426667 1.70667,-0.426666 3.30667,-1.226666 0.42666,-0.213333 0.96,-0.213333 0.85333,0 1.49333,0.586666 0.64,0.586667 0.64,1.493333 0,1.173332 -1.38667,1.973332 -2.08,1.173333 -4.10666,1.759999 -2.02667,0.586666 -5.22667,0.586666 z m 6.82667,-16.373323 q -0.16,-2.613332 -1.38667,-4.373331 -1.17333,-1.759999 -2.93333,-2.559999 -1.76,-0.853332 -3.57333,-0.853332 -1.81333,0 -3.57333,0.853332 -1.76,0.8 -2.93333,2.559999 -1.17334,1.759999 -1.33334,4.373331 z"
         id="path842"
         style="fill:url(#linearGradient845);scale:var(--scale2);transition:2.4s;transform-origin:100% 150%;" />
    </g>
  </g>
</svg>
`

export default logoSVG;