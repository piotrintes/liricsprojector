import Screen from "../screen.js";

class VideoBackground extends Screen {
     constructor(screenArea) {
          super(screenArea);

          this.player = document.createElement('video');
          this.player.preload = 'auto'
          this.player.autoplay = true;
          this.player.muted = true;
          this.player.loop = true;
          this.screen.appendChild(this.player);
     }

     async loadVideo(src) {
          this.player.src = await this.proloadVideo('mbcg/' + src);
          this.player.play();
          let playInterval = setInterval(() => {
               if (this.player.paused)
                    this.player.play();
               else
                    clearInterval(playInterval);
          }, 1000);
     }

     async proloadVideo(src) {
          const res = await fetch(src);
          const blob = await res.blob();
          return URL.createObjectURL(blob);
     }
}

export default VideoBackground;