/** This class manages the content screens, swithing between them and calling updates. */
class ContentManager {

     /**
      * @param server Endpoint for Lyrics Projector web server
      * @param song Song screen
      * @param image Image screen
      * @param video Video screen
      * @param black Black screen
      */
     constructor(server, song, image, video, black, welcome) {
          this.server = server;
          this.song = song;
          this.image = image;
          this.video = video;
          this.black = black;
          this.welcome = welcome;

          this.lastUpdate;
          this.update_ = this.update.bind(this);
          //this.connect();
     }

     async connect() {
          await this.server.updateConnectionState();
          if (this.server.getConnected()) {
               //this.interval = setInterval(this.update_, 25);
               //console.log("setting interval: ", this.interval)
               this.server.addWebSocketCallback((event) => {
                    this.update(event.data);
               })
               this.welcome.update();
               //this.update();
          } else
               setTimeout(() => this.connect(), 5000);
     }

     async disconnect() {
          //if (!this.interval) return;
          //console.log("removing interval: ", this.interval)
          //clearInterval(this.interval);
          //this.interval = undefined;
          await this.server.updateConnectionState();
          this.welcome.update();
          this.welcome.show();
          setTimeout(() => this.connect(), 5000);
     }

     /** Updates the contetns by switching and updating screens. This is called automatically by 10ms interval. */
     update(message) {
          try {
               if (message == 'videostop') this.video.update(message);
               this.server.get('content').then(json => {
                    if (!json) {
                         this.disconnect();
                         return;
                    }

                    if (this.lastUpdate == json.update) return;

                    this.lastUpdate = json.update;
                    if (json.display == 'empty') {
                         this.song.hide();
                         this.image.hide();
                         this.video.hide();
                         this.black.hide();
                    } else if (json.display == 'black') {
                         this.black.show();
                    } else if (json.display == 'image' && json.content == 'song') {
                         this.song.update();
                         this.song.show();
                    } else if (json.display == 'image' && json.content == 'image') {
                         this.image.update();
                         this.image.show();
                    } else if (json.display == 'image' && json.content == 'video') {
                         this.video.update(message);
                         this.video.show();
                    }
               });
          } catch (e) { this.disconnect(); }
     }
}

export default ContentManager;