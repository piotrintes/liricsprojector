/** This is a base class representing a screen - content container to be displayed within provided div element */
class Screen {
     /**
      * @param screenArea container the content should be displayed within.
      */
     constructor(screenArea) {
          this.screen = document.createElement("div");
          this.screen.className = 'screen';
          this.screen.show = this.show.bind(this);
          this.screen.hide = this.hide.bind(this);
          screenArea.appendChild(this.screen);

          this.screenArea = screenArea;
          this.isVisible = false;
     }

     /** Returns currently active screen from within the same parent container */
     getCurrentScreen() {
          for (const element of this.screenArea.children) {
               if (element.style.opacity == '1') return element;
          };
     }

     /** Show this screen (and hide the current one) */
     show() {
          this.getCurrentScreen()?.hide();
          this.screen.style.opacity = '1';
          this.isVisible = true;
     }

     /** Hide this screen */
     hide() {
          this.screen.style.opacity = '0';
          this.isVisible = false;
     }
}

export default Screen;