/** This class provides connection with Lyric Projector web server */
class Server {
     constructor() {
          this.address = null;
          this.port = 50400;
          this.wsport = 50500;
          this.connected = false;
          this.websocket = undefined;

          this.updateConnectionState();
     }

     /** Update server address */
     async updateServerAddress() {
          if (this.address == null) {
               // search for server
               let serverIp = await fetch('svr-ip');
               this.address = await serverIp.text();
          }
     }

     /** Check connection with server app */
     async updateConnectionState() {

          await this.updateServerAddress();

          if (this.address == null) return;

          let json = await fetch(`http://${this.address}:${this.port}/ver`).then(res => res.json()).catch(() => {
               this.connected = false;
               console.log(`No connection to ${this.address}:${this.port}`);
               this.address = null;
          })

          if (this.connected) return;

          if (json) {
               console.log(json)
               this.connected = true;
               console.log("Connected to Lyrics Projector HTTP server");
               this.app = json.app;
               this.version = json.version;

               this.websocket = new WebSocket(`ws://${this.address}:${this.wsport}`);
               this.websocket.addEventListener("message", (event) => {
                    console.log("Message from server ", event.data);
               });
          }
     }

     /** Returns connection state (based on last manual check) */
     getConnected() {
          return this.connected;
     }

     /** Returns server version */
     getVersion() {
          return this.version;
     }

     /** Returns server url */
     getUrl() {
          return `http://${this.address}:${this.port}`;
     }

     /** Sends GET request to server and returns the response
      * @param request Requset to be sent to server
      * @returns promise of response
      */
     get(request) {
          if (this.getConnected()) {
               // if connected, try to fetch the request
               return fetch(`http://${this.address}:${this.port}/${request}`)
                    .then(res => res.json())
                    .catch(() => { });
          } else {
               return undefined
          }
     }

     getVideoUrl(filename) {
          return "videosrc/" + filename;
     }

     addWebSocketCallback(callback) {
          this.websocket.addEventListener("message", callback);
     }
}

export default Server;
