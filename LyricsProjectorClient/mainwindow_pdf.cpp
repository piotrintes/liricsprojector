#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>


void MainWindow::on_loadPdfButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(nullptr,"Otwórz plik z notatkami",QString(),"PDF (*.pdf)");
    if(filename != ""){
        ui->pdfProgressBar->show();
        ui->pdfProgressBar->setValue(0);
        // setup dir for pdf cache
        QDir dir(QApplication::applicationDirPath() + "/pdfCache/");
        if(!dir.exists()) dir.mkdir(QApplication::applicationDirPath() + "/pdfCache/");
        QStringList files = dir.entryList(QStringList() << "*.png",QDir::Files);
        foreach(QString file, files) {
            QFile::remove(QApplication::applicationDirPath() + "/pdfCache/" + file);
        }
        ui->pdfProgressBar->setValue(5);

        // use mutool to render pdf into png images
        QProcess *process = new QProcess();

        #ifdef Q_OS_WIN
            process->start(QApplication::applicationDirPath() + "/mutool.exe draw -F png -r 180 -o \"" + QApplication::applicationDirPath() + "/pdfCache/"
                      + "%d.png\" \"" + filename + "\"");
        #endif
        #ifdef Q_OS_LINUX
            process->start("mutool draw -F png -r 180 -o \"" + QApplication::applicationDirPath() + "/pdfCache/"
                      + "%d.png\" \"" + filename + "\"");
        #endif
        #ifdef Q_OS_MAC
            process->start(QApplication::applicationDirPath() + "/mutool draw -F png -r 180 -o \"" + QApplication::applicationDirPath() + "/pdfCache/"
                      + "%d.png\" \"" + filename + "\"");
        #endif

            // when process can't run - execute lambda expression - display error message
            connect(process, &QProcess::errorOccurred,[this](int err){
                if (err == 0)   //mutool is not installed
        #ifdef Q_OS_LINUX
                    QMessageBox::critical(nullptr,"Dodawanie pliku pdf nie powiodło się.",
                                          "Nie można uruchomić programu mutool, niezbędnego do przetwarzania plików PDF.\n\n"
                                          "Upewnij się, że pakiet mupdf-tools jest zainstalowany. Pakiet ten można zainstalować poleceniem:\n"
                                          "  sudo apt install mupdf-tools\n"
                                          "Instalacja wymaga uprawnień administratora.");
        #else
                    QMessageBox::critical(nullptr,"Dodawanie pliku pdf nie powiodło się.",
                                          "Nie można uruchomić programu mutool, niezbędnego do przetwarzania plików PDF.\n\n"
                                          "Upewnij się, że pakiet mupdf-tools jest zainstalowany. Instrukcja instalacji znajduje się tutaj:\n"
                                          "  http://macappstore.org/mupdf-tools/\n"
                                          "Instalacja wymaga uprawnień administratora.");

                //http://macappstore.org/mupdf-tools/
        #endif
                ui->pdfProgressBar->hide();
        });


        // when process is finnished - execute lambda expression - process result
        connect(process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [this, filename, process](int exitCode, QProcess::ExitStatus exitStatus){

            if (!exitCode) { // exitCode = 0 - process pdf
                ui->pdfProgressBar->setValue(50);
                //load all pages
                QVector<QPixmap> pages;
                int width = 0;
                int len = 0;
                while(QFile::exists(QApplication::applicationDirPath() + "/pdfCache/" + QString::number(pages.count() + 1) + ".png")) {
                    pages.append(QPixmap(QApplication::applicationDirPath() + "/pdfCache/" + QString::number(pages.count() + 1) + ".png"));
                    len += pages.last().height();
                    if(width < pages.last().width()) width = pages.last().width();
                }

                QPixmap page;
                QPixmap pageSmall;

                ui->pdfProgressBar->setValue(70);

                //merge pages into single pixmap
                int y = 0;
                page = QPixmap(width,len);
                page.fill();
                QPainter p(&page);
                foreach(QPixmap page, pages) {
                    p.drawPixmap(0,y,page.width(),page.height(),page);
                    y += page.height();
                }
                p.end();

                ui->pdfProgressBar->setValue(90);

                int w = this->width() * 0.4;
                int h = page.width() * page.height();
                pageSmall = page.scaled(w,h,Qt::KeepAspectRatio,Qt::SmoothTransformation);
                w = this->width() * 0.75;
                page = page.scaled(w,h,Qt::KeepAspectRatio,Qt::SmoothTransformation);

                //add pdf to list and assign lambda to button
                Notes *newItem = new Notes(filename.split("/").last(),page,pageSmall);
                notesList.append(newItem);

                ui->verticalLayout_5->addWidget(newItem->button());

                connect(notesList.last()->button(),&QPushButton::clicked,this,[this, newItem](){
                    notes = newItem;
                    updateUI();
                });

                ui->pdfProgressBar->setValue(100);

                updateUI();
            } else {
                // something went wrong and mutool returned no-zero - display error message
                QMessageBox::critical(this,"Dodawanie pliku pdf nie powiodło się.","Plik " + filename + " nie został odczytany.\n"
                                           "Upewnij się, że wskazany plik jest poprawnym plikiem PDF.");
            }
            // close connection to mutool process
            process->close();
            ui->pdfProgressBar->hide();
        });
    }
}
