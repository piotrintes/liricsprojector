#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QMediaPlayer>
#include <QVideoWidget>
#include "theme.h"
#include "screenmanager.h"
#include "notes.h"
#include "connectioncontrol.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    QUdpSocket *socket = new QUdpSocket();
    QTcpSocket *tcpNextSocket = new QTcpSocket();
    QTcpSocket *tcpContentsSocket = new QTcpSocket();
    QTcpSocket *tcpControlSocket = new QTcpSocket();
    ConnectionControl *connectionCtrl;

    QString serverAddress = "192.168.2.105";
    quint16 slideControlPort = 50100;
    quint16 slideContentsPort = 50200;
    quint16 slideNextPort = 50300;
    //int vidStreamPortNumber = 50200;
    quint16 slideRemotePort = 50000;
    QByteArray data = "";
    QByteArray dataN = "";
    bool cleanupNeeded = false;

    QWidget *lastScreen;
    Theme *theme = new Theme();

    //QProcess *player = nullptr;
    QMediaPlayer *player = new QMediaPlayer;                // media player
    QVideoWidget *videoWidget = new QVideoWidget;           // video widget (canvas for media player)

    bool showSlideControls = false;
    double notes2preview = 0;
    bool showNextSlide = false;

    ScreenManager scrM;
    bool useGraphicalOutput = true;

    QPixmap lastImage;

    QVector<Notes*> notesList;
    Notes *notes = nullptr;


public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void sentUDPData(QByteArray data, quint16 port);

    void updateTheme();
    void setupUI();
    void updateUI();

    void loadConfig();
    void saveConfig();

protected:
    virtual void resizeEvent(QResizeEvent * event);

private slots:
    void on_nextButton_clicked();
    void on_previousButton_clicked();
    void on_blackButton_clicked();
    void on_changeNotes2PreviewButton_clicked();
    void on_showNotesButton_clicked();
    void on_loadPdfButton_clicked();

    void waitForConnection(bool connectionLost = true);
    void readContents();
    void readNext();
    void readControl();
    void sendPing();

    //void endPlayer();



    void on_closeNotesButton_clicked();

private:
    Ui::MainWindow *ui;

    void closeEvent (QCloseEvent *event);
};
#endif // MAINWINDOW_H
