#include "widgetarea.h"

#include <QGraphicsDropShadowEffect>
#include <QPainter>
#include <QMouseEvent>
#include <QTime>
#include <QTimer>
#include <QCoreApplication>
#include <QDebug>
#include <QProcess>

const QColor &WidgetArea::backgroundColor() const
{
    return backgroundColor_;
}

void WidgetArea::setBackgroundColor(const QColor &newBackgroundColor)
{
    backgroundColor_ = newBackgroundColor;
}

WidgetArea::WidgetArea(QWidget *parent) : QWidget(parent)
{
    // setup shadow effect
    QGraphicsDropShadowEffect* sh = new QGraphicsDropShadowEffect();
    sh->setColor(QColor(0,0,0,150));
    sh->setOffset(0,1);
    sh->setBlurRadius(20);

    this->setGraphicsEffect(sh);

    // setup font
    QFont f("arial",height()/1.1,1,false);
    setFont(f);

    //setup update
    QTimer *timer = new QTimer();
    connect(timer, &QTimer::timeout, this, [this]{
        if(isVisible()){
            updateStoper();
            updateBattery();
            update();
        }
    });
    timer->start(1000);

    // setup battery reader
    initBatery();
    updateBattery();
}

void WidgetArea::updateStoper()
{
    if (stoperActive) {
        stoperSeconds++;
        if (stoperSeconds == 60) {
            stoperSeconds = 0;
            stoperMinutes++;
        }
        if (stoperMinutes == 60) {
            stoperMinutes = 0;
            stoperHour++;
        }
    }

    if(stoperHour < 10) stoperOutput = "0" + QString::number(stoperHour); else stoperOutput = QString::number(stoperHour);
    if(stoperMinutes < 10) stoperOutput += ":0" + QString::number(stoperMinutes); else stoperOutput += ":" + QString::number(stoperMinutes);
}

void WidgetArea::updateBattery()
{
    if (batteryDevice == "") return;
    if (actTab != 2) return;

    QProcess process;
    //process.start("upower -i $(upower -e | grep 'bat') | grep -E \"state|time\\ to|percentage\"");
    process.start("upower -i " + batteryDevice);
    process.waitForFinished(100);
    QStringList lines = QString(process.readAllStandardOutput()).split("\n");
    batteryTime = "";
    foreach (QString line, lines) {
        QStringList val = line.split(":");
        val[0].remove(" ");
        if(val.at(0) == "percentage") batteryLevel = val[1].remove(" ").left(val.at(1).length()-1).toUInt();
        if(val.at(0) == "state") batteryCharging = val[1].remove(" ") != "discharging";
        if(val.at(0) == "timetoempty") batteryTime = val[1].remove("  ");
        if(val.at(0) == "timetofull") batteryTime = val[1].remove("  ");
    }
}

void WidgetArea::initBatery()
{
    QProcess process;
    process.start("upower -e");
    process.waitForFinished(1000);
    QStringList output = QString(process.readAllStandardOutput()).split("\n");
    foreach (QString device, output) {
        if (device.contains("bat")) batteryDevice = device;
    }
}

void WidgetArea::paintClock(QPainter *painter, int offset)
{
    painter->setBrush(painter->pen().color());
    painter->drawEllipse(width()-height()/3*2,height()/2-3-12,6,6);

    painter->drawText(height(),height()/1.35 + height()*offset/100,QTime::currentTime().toString("hh:mm"));
}

void WidgetArea::paintStoper(QPainter *painter, int offset)
{
    painter->setBrush(painter->pen().color());
    painter->drawEllipse(width()-height()/3*2,height()/2-3,6,6);

    //if(!stoperActive) painter->setPen(QPen(QColor(255,50,50),1));
    painter->drawText(height()/3*2,height()/1.35 + height()*offset/100,stoperOutput);

    QFont f = painter->font();
    f.setPixelSize(height()/2);
    painter->setFont(f);

    QString sec = QString::number(stoperSeconds);
    if (stoperSeconds < 10) sec = "0" + sec;

    painter->drawText(height()*2.4,height()/1.35 + height()*offset/100,sec);
}

void WidgetArea::paintBattery(QPainter *painter, int offset)
{
    QColor c = painter->pen().color();
    if (batteryDevice == "") {
        painter->setPen(QPen(QColor(255,50,50),1));
        QFont f = painter->font();
        f.setPixelSize(height()/4);
        painter->setFont(f);
        painter->drawText(height()*2,height()*0.45 + height()*offset/100, "No battery");

        painter->setPen(QPen(c,1));
        painter->setBrush(QBrush(QColor(0,0,0,0)));
        painter->drawRoundedRect(height(),height()/3 + height()*offset/100,height()/3*2,height()/3,3,3);
        return;
    }

    painter->setBrush(c);
    painter->drawEllipse(width()-height()/3*2,height()/2-3+12,6,6);

    QFont f = painter->font();
    f.setPixelSize(height()/4);
    painter->setFont(f);
    painter->drawText(height()*2,height()*0.45 + height()*offset/100, QString::number(batteryLevel) + "%");
    painter->drawText(height()*2,height()/1.35 + height()*offset/100, batteryTime);

    painter->setPen(QPen(QColor(0,0,0,0),0));
    if (batteryCharging)
        painter->setBrush(QBrush(QColor(50,180,200)));
    else
        if (batteryLevel < 25) painter->setBrush(QBrush(QColor(255,50,50))); else painter->setBrush(QBrush(QColor(50,200,50)));

    painter->drawRoundedRect(height(),height()/3 + height()*offset/100,height()/3*2*batteryLevel/100,height()/3,3,3);

    painter->setPen(QPen(c,1));
    painter->setBrush(QBrush(QColor(0,0,0,0)));
    painter->drawRoundedRect(height(),height()/3 + height()*offset/100,height()/3*2,height()/3,3,3);
}

void WidgetArea::paintEvent(QPaintEvent *event)
{
    // setup painter
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(QPen(QColor(0,0,0,0),0));
    painter.setBrush(QBrush(backgroundColor_));

    painter.drawEllipse(0,0,height(),height());
    painter.drawEllipse(width()-height(),0,height(),height());
    painter.drawRect(height()/2,0,width()-height(),height());

    if(backgroundColor_.value()<128)
        painter.setPen(QPen(QColor(255,255,255),1));
    else
        painter.setPen(QPen(QColor(0,0,0),1));

    // draw button / indicator
    painter.setBrush(QBrush(QColor(0,0,0,0)));
    painter.drawEllipse(width()-height()/3*2,height()/2-3-12,6,6);
    painter.drawEllipse(width()-height()/3*2,height()/2-3,6,6);
    painter.drawEllipse(width()-height()/3*2,height()/2-3+12,6,6);


    // draw contents + animation
    if (actTab == 0) paintClock(&painter,transition);
    else if (actTab == 1) paintStoper(&painter,transition);
    else if (actTab == 2) paintBattery(&painter,transition);

    if (transition != 0) {
        unsigned short nextTab = actTab + 1;
        if (nextTab > tabCount-1) nextTab = 0;

        if (nextTab == 0) paintClock(&painter,100+transition);
        else if (nextTab == 1) paintStoper(&painter,100+transition);
        else if (nextTab == 2) paintBattery(&painter,100+transition);
    }
}

void WidgetArea::mousePressEvent(QMouseEvent * event)
{
    //QPushButton::mousePressEvent(event);     // this will do base class content
    //isClicked=true;
    update();
}

void WidgetArea::mouseReleaseEvent(QMouseEvent * event)
{
    //QPushButton::mouseReleaseEvent(event);
    //isClicked=false;
    if (event->x() > width()-height()) {

        QTime startTime = QTime::currentTime();
        while (transition > -100) {
            transition = QTime::currentTime().msecsTo(startTime);
            QCoreApplication::processEvents();
            update();
        }
        actTab++;
        if (actTab > tabCount-1) actTab = 0;
        transition = 0;
    } else {
        if (actTab == 1) {
            if (stoperActive) {
                stoperActive = false;
            } else {
                stoperHour = 0;
                stoperMinutes = 0;
                stoperSeconds = 0;
                stoperActive = true;
                stoperOutput = "00:00";
            }
        }
    }
    update();
}
