/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "theme.h"

#include <QTextStream>
#include <QDebug>

QList<int> Theme::bibleMargins() const
{
    return bibleMargins_;
}

void Theme::setBibleMargins(const QList<int> &bibleMargins)
{
    bibleMargins_ = bibleMargins;
    emit updated();
}

QColor Theme::bibleBcgColor() const
{
    return bibleBcgColor_;
}

void Theme::setBibleBcgColor(const QColor &bibleBcgColor)
{
    bibleBcgColor_ = bibleBcgColor;
    emit updated();
}

QColor Theme::bibleTextColor() const
{
    return bibleTextColor_;
}

void Theme::setBibleTextColor(const QColor &bibleTextColor)
{
    bibleTextColor_ = bibleTextColor;
    emit updated();
}

int Theme::bibleTextSize() const
{
    return bibleTextSize_;
}

void Theme::setBibleTextSize(int bibleTextSize)
{
    bibleTextSize_ = bibleTextSize;
    emit updated();
}

Theme::Theme()
{}

QFont Theme::Font() const
{
    return Font_;
}

void Theme::setFont(const QFont &Font)
{
    Font_ = Font;
    emit updated();
}

int Theme::textSize() const
{
    return textSize_;
}

void Theme::setTextSize(int textSize)
{
    textSize_ = textSize;
    emit updated();
}

QColor Theme::textColor() const
{
    return textColor_;
}

void Theme::setTextColor(const QColor &textColor)
{
    textColor_ = textColor;
    emit updated();
}

QColor Theme::bcgColor() const
{
    return bcgColor_;
}

void Theme::setBcgColor(const QColor &bcgColor)
{
    bcgColor_ = bcgColor;
    emit updated();
}

QString Theme::name() const
{
    return name_;
}

void Theme::setName(const QString &name)
{
    name_ = name;
}

Theme::Theme(Theme *theme)
{
    setTheme(theme);
}

void Theme::setTheme(Theme *theme)
{
    name_ = theme->name();

    Font_ = theme->Font();
    textSize_ = theme->textSize();
    textColor_ = theme->textColor();
    bcgColor_ = theme->bcgColor();
    bibleTextSize_ = theme->bibleTextSize();
    bibleTextColor_ = theme->bibleTextColor();
    bibleBcgColor_ = theme->bibleBcgColor();
    bibleMargins_ = theme->bibleMargins();
    emit updated();
}

void Theme::loadTheme(QFile *file)
{
    QString cfg;
    if (file->open(QFile::ReadOnly | QFile::Text)){
        QTextStream in(file);
        cfg = in.readAll();
    } else {
        return;
    }

    QStringList lines = cfg.split("\n");

    foreach (QString line, lines) {
        if(line != "") {
            QString var = line.split(":").at(0);
            QString val = line.split(":").at(1);
            if(var == "bcgColor") setBcgColor(QColor(val));
            else if (var == "textColor") setTextColor(QColor(val));
            else if (var == "textSize") setTextSize(val.toInt());
            else if (var == "textFont") {
                QFont font;
                font.fromString(val);
                setFont(font);
            }
            else if (var == "bibleBcgColor") setBibleBcgColor(QColor(val));
            else if (var == "bibleTextColor") setBibleTextColor(QColor(val));
            else if (var == "bibleTextSize") setBibleTextSize(val.toInt());
            else if (var == "bibleMarginsLeft") bibleMargins_[0] = val.toInt();
            else if (var == "bibleMarginsRight") bibleMargins_[1] = val.toInt();
            else if (var == "bibleMarginsTop") bibleMargins_[2] = val.toInt();
            else if (var == "bibleMarginsBottom") bibleMargins_[3] = val.toInt();
        }
    }
}

void Theme::saveTheme(QFile *file)
{
    if(file->exists()) file->remove();

    if (file->open(QIODevice::WriteOnly)) {
        QTextStream out(file);
        out << "bcgColor:" << bcgColor().name() << "\n";
        out << "textColor:" << textColor().name() << "\n";
        out << "textSize:" << textSize() << "\n";
        out << "textFont:" << Font().toString() << "\n";
        out << "bibleBcgColor:" << bibleBcgColor().name() << "\n";
        out << "bibleTextColor:" << bibleTextColor().name() << "\n";
        out << "bibleTextSize:" << bibleTextSize() << "\n";
        out << "bibleMarginsLeft:" << bibleMargins().at(0) << "\n";
        out << "bibleMarginsRight:" << bibleMargins().at(1) << "\n";
        out << "bibleMarginsTop:" << bibleMargins().at(2) << "\n";
        out << "bibleMarginsBottom:" << bibleMargins().at(3) << "\n";
        file->close();
    }
}
