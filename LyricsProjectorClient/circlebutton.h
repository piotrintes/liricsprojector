#ifndef CIRCLEBUTTON_H
#define CIRCLEBUTTON_H

#include <QPushButton>

class CircleButton : public QPushButton
{
private:
    bool isClicked = false;
    QColor buttonColor_ = QColor(255,255,255);

public:
    CircleButton(QWidget* parent = Q_NULLPTR);

    const QColor &buttonColor() const;
    void setButtonColor(const QColor &newButtonColor);

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent * event);          // Overloaded mouse press event (set isClicked = true)
    virtual void mouseReleaseEvent(QMouseEvent * event);        // Overloaded mouse relese event (set isClicked = false)

};

#endif // CIRCLEBUTTON_H
