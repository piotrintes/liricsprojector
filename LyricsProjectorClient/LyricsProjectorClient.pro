QT       += core gui multimedia multimediawidgets network

VERSION = 0.2
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    circlebutton.cpp \
    connectioncontrol.cpp \
    lebeledimage.cpp \
    main.cpp \
    mainwindow.cpp \
    mainwindow_pdf.cpp \
    mainwindow_ui.cpp \
    notes.cpp \
    noteslistbutton.cpp \
    screenmanager.cpp \
    theme.cpp \
    widgetarea.cpp

HEADERS += \
    circlebutton.h \
    connectioncontrol.h \
    lebeledimage.h \
    mainwindow.h \
    notes.h \
    noteslistbutton.h \
    screenmanager.h \
    theme.h \
    widgetarea.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
