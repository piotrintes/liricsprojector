#ifndef LEBELEDIMAGE_H
#define LEBELEDIMAGE_H

#include <QLabel>

class LebeledImage : public QLabel
{
private:
    QString label_;
    bool displayLabel = true;
    QColor backgroundColor_ = QColor(255,255,255);
public:
    LebeledImage(QWidget* parent = Q_NULLPTR);

    const QString &label() const;
    void setLabel(const QString &newLabel);
    const QColor &backgroundColor() const;
    void setBackgroundColor(const QColor &newBackgroundColor);
    void setDisplayLabel(bool newDisplayLabel);

protected:
    virtual void paintEvent(QPaintEvent *event);
};

#endif // LEBELEDIMAGE_H
