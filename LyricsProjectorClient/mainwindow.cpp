#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QScreen>
#include <QNetworkInterface>
#include <QNetworkDatagram>
#include <QDebug>
#include <QDir>
#include <QPixmap>
#include <QThread>
#include <QGraphicsVideoItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QScroller>
#include <QScrollBar>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // if theme config file exists load theme from file
    QDir dir(QApplication::applicationDirPath() + "/cfg/");
    if(dir.exists()) {
        QFile file(QApplication::applicationDirPath() + "/cfg/theme.cfg");
        theme->loadTheme(&file);
        loadConfig();
    } else {
        dir.mkdir(QApplication::applicationDirPath() +  + "/cfg/");
    }

    ui->notesScrollArea->horizontalScrollBar()->setEnabled(false);
    QScroller::grabGesture(ui->notesScrollArea, QScroller::LeftMouseButtonGesture);

    updateTheme();
    setupUI();

    qDebug() << "attempting connection with " << serverAddress << ":" << slideControlPort;

    // open connection to server app
    tcpContentsSocket->connectToHost(serverAddress, slideContentsPort);
    tcpControlSocket->connectToHost(serverAddress, slideControlPort);
    tcpNextSocket->connectToHost(serverAddress, slideNextPort);

    connect(tcpNextSocket, SIGNAL(readyRead()), this, SLOT(readNext()));
    connect(tcpContentsSocket, SIGNAL(readyRead()), this, SLOT(readContents()));
    connect(tcpControlSocket, SIGNAL(readyRead()), this, SLOT(readControl()));
    connect(tcpNextSocket, SIGNAL(disconnected()), this, SLOT(waitForConnection()));
    connect(tcpContentsSocket, SIGNAL(disconnected()), this, SLOT(waitForConnection()));
    connect(tcpControlSocket, SIGNAL(disconnected()), this, SLOT(waitForConnection()));

    // connection control
    /*connectionCtrl = new ConnectionControl(tcpControlSocket);
    connect(connectionCtrl, SIGNAL(sendPing()), this, SLOT(sendPing()));
    connect(connectionCtrl, SIGNAL(disconnect()), this, SLOT(waitForConnection()));
    connectionCtrl->start();*/

    // setup media player
    player->setVideoOutput(videoWidget);
    ui->video->layout()->addWidget(videoWidget);

    ui->MainScreen->layout()->addWidget(scrM.getGraphicsView());

    show();

    waitForConnection(false);

    //player->setMedia(QUrl::fromLocalFile(QApplication::applicationDirPath() + "/video/Przejściówka.mp4"));
    //player->play();
    if(useGraphicalOutput) ui->stackedWidget->setCurrentWidget(ui->MainScreen);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::loadConfig()
{
    QFile file(QApplication::applicationDirPath() + "/cfg/config.cfg");

    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QString f = file.readAll();
        file.close();
        QStringList lines = f.split("\n");
        foreach (QString line, lines) {
            if (line == "") continue;
            QString var = line.split(": ").at(0);
            QString val = line.split(": ").at(1);
            if(var == "server-ip") serverAddress = val;
            if(var == "network-remote-port") slideRemotePort = val.toInt();
            if(var == "network-control-1-port") slideControlPort = val.toInt();
            if(var == "network-control-2-port") slideContentsPort = val.toInt();
            if(var == "network-control-3-port") slideNextPort = val.toInt();
            if(var == "use-graphical-output") useGraphicalOutput = val.toUInt();
            if(var == "show-slide-controls") showSlideControls = val.toUInt();
            if(var == "show-next-slide") showNextSlide = val.toUInt();
            if(var == "hide-mouse" && val == "1") setCursor(Qt::BlankCursor);
        }
    }
}

void MainWindow::saveConfig()
{
    QFile file(QApplication::applicationDirPath() + "/cfg/config.cfg");
    if(file.exists()) file.remove();

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out << "server-ip: " << serverAddress;
        out << "\n" << "network-remote-port: " << slideRemotePort;
        out << "\n" << "network-control-1-port: " << slideControlPort;
        out << "\n" << "network-control-2-port: " << slideContentsPort;
        out << "\n" << "network-control-3-port: " << slideNextPort;
        out << "\n" << "use-graphical-output: " << useGraphicalOutput;
        out << "\n" << "show-slide-controls: " << showSlideControls;
        out << "\n" << "show-next-slide: " << showNextSlide;
        file.close();
    }
}

void MainWindow::sentUDPData(QByteArray data, quint16 port)
{
    QNetworkDatagram datagram;
    datagram.setData(data);
    datagram.setDestination(QHostAddress(serverAddress),port);

    socket->writeDatagram(datagram);
    socket->flush();
}

void MainWindow::waitForConnection(bool connectionLost)
{
    lastScreen = ui->splash;
    ui->stackedWidget->setCurrentWidget(ui->splash);

    QPalette pal = palette();
    pal.setColor(QPalette::WindowText,QColor(255,50,50));
    ui->connectionStatusLabel->setPalette(pal);

    QCoreApplication::processEvents();

    if(connectionLost) {
        ui->connectionStatusLabel->setText("Connection lost. Attempting to reconnect.");
        QCoreApplication::processEvents();
        QThread::sleep(3);
        ui->connectionStatusLabel->setText("Reconnecting.  ");
        cleanupNeeded = true;
    }

    QCoreApplication::processEvents();

    while(!tcpContentsSocket->waitForConnected(1000)) {
        tcpContentsSocket->connectToHost(serverAddress, slideContentsPort);
        if(ui->connectionStatusLabel->text() == "Reconnecting.  ") ui->connectionStatusLabel->setText("Reconnecting . ");
        else if(ui->connectionStatusLabel->text() == "Reconnecting . ") ui->connectionStatusLabel->setText("Reconnecting  .");
        else if(ui->connectionStatusLabel->text() == "Reconnecting  .") ui->connectionStatusLabel->setText("Reconnecting.  ");
        else if(ui->connectionStatusLabel->text() == "Connecting.  ") ui->connectionStatusLabel->setText("Connecting . ");
        else if(ui->connectionStatusLabel->text() == "Connecting . ") ui->connectionStatusLabel->setText("Connecting  .");
        else if(ui->connectionStatusLabel->text() == "Connecting  .") ui->connectionStatusLabel->setText("Connecting.  ");
        else ui->connectionStatusLabel->setText("Connecting.  ");
        QCoreApplication::processEvents();
        QThread::sleep(1);
    }
    while(!tcpControlSocket->waitForConnected(10)) {
        tcpControlSocket->connectToHost(serverAddress, slideControlPort);
        QCoreApplication::processEvents();
    }
    if(showNextSlide){
        while(!tcpNextSocket->waitForConnected(10)) {
            tcpNextSocket->connectToHost(serverAddress, slideNextPort);
            QCoreApplication::processEvents();
        }
    }

    pal.setColor(QPalette::WindowText,QColor(50,200,50));
    ui->connectionStatusLabel->setPalette(pal);
    ui->connectionStatusLabel->setText("Connected to server");
    connectionCtrl->setPingReturned(true);
    qDebug() << "sending version";
    tcpControlSocket->write(APP_VERSION);
    tcpControlSocket->flush();

    QThread::sleep(1);
    if(useGraphicalOutput) ui->stackedWidget->setCurrentWidget(ui->MainScreen);
}

bool compareColors(QColor color1, QColor color2)
{
    if (int(color1.red()*10) != int(color2.red()*10)) return false;
    if (int(color1.green()*10) != int(color2.green()*10)) return false;
    if (int(color1.blue()*10) != int(color2.blue()*10)) return false;
    return true;
}

void MainWindow::readContents()
{
    if(cleanupNeeded) {
        tcpContentsSocket->readAll();
        tcpControlSocket->readAll();
        tcpNextSocket->readAll();
        cleanupNeeded = false;
        return;
    }

    QByteArray datagram = tcpContentsSocket->readAll();
    data += datagram;

    if(datagram.right(3) == "eot"){
        data = data.left(data.length()-3);


        if(useGraphicalOutput) {
            QPixmap image;
            image.loadFromData(data,"JPG");
            if(!image.isNull()) {
                scrM.setImagePixmap(image);
            } else scrM.setSongText(data);
        } else {
            ui->SlideText->setText(data);

            QPixmap image;
            image.loadFromData(data,"JPG");
            if(!image.isNull()) {
                lastImage = image;
                ui->SlideImage->setPixmap(image.scaled(ui->SlideImage->width(),ui->SlideImage->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

                // setup background color
                QPalette pal = ui->image->palette();

                if(compareColors(image.toImage().pixelColor(0,0), image.toImage().pixelColor(0,image.height()-2)) &&
                        compareColors(image.toImage().pixelColor(image.width()-2,0), image.toImage().pixelColor(image.width()-2,image.height()-2)) &&
                        compareColors(image.toImage().pixelColor(0,0), image.toImage().pixelColor(image.width()-2,image.height()-2))
                        ) {
                    pal.setColor(QPalette::Window, image.toImage().pixelColor(0,0));
                } else {
                    pal.setColor(QPalette::Window,QColor(0,0,0));
                }

                ui->image->setPalette(pal);
            }
        }

        data = "";
    }
}

void MainWindow::readNext()
{
    if(cleanupNeeded) {
        tcpContentsSocket->readAll();
        tcpControlSocket->readAll();
        tcpNextSocket->readAll();
        cleanupNeeded = false;
        return;
    }

    QByteArray datagram = tcpNextSocket->readAll();
    dataN += datagram;

    if(datagram.right(3) == "eot"){
        dataN = dataN.left(dataN.length()-3);

        ui->NextPreview->setText(dataN);
        ui->NextPreview->setPalette(palette());

        QPixmap image;
        image.loadFromData(dataN,"JPG");
        if(!image.isNull()) {
            ui->NextPreview->setPixmap(image.scaled(ui->NextPreview->width(),ui->NextPreview->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

            // setup background color
            QPalette pal = ui->NextPreview->palette();

            if(compareColors(image.toImage().pixelColor(0,0), image.toImage().pixelColor(0,image.height()-2)) &&
                    compareColors(image.toImage().pixelColor(image.width()-2,0), image.toImage().pixelColor(image.width()-2,image.height()-2)) &&
                    compareColors(image.toImage().pixelColor(0,0), image.toImage().pixelColor(image.width()-2,image.height()-2))
                    ) {
                pal.setColor(QPalette::Window, image.toImage().pixelColor(0,0));
            } else {
                pal.setColor(QPalette::Window,QColor(0,0,0));
            }

            ui->NextPreview->setPalette(pal);
        }
        dataN = "";
    }
}

void MainWindow::readControl()
{
    if(cleanupNeeded) {
        tcpContentsSocket->readAll();
        tcpControlSocket->readAll();
        tcpNextSocket->readAll();
        cleanupNeeded = false;
        return;
    }

    QByteArray datagram = tcpControlSocket->readLine();

    if(datagram == "ping-response") {
        qDebug() << "ping responded";
        connectionCtrl->setPingReturned(true);
        return;
    }

    if(useGraphicalOutput) {
        ui->stackedWidget->setCurrentWidget(ui->MainScreen);
        if(datagram == "black") {
            scrM.black();
        }else if(datagram == "color") {
            scrM.empty();
        }else if(datagram == "restore") {
            scrM.restore();
        }else if(datagram == "song") {
            scrM.setSong();
        }else if(datagram == "image") {
            scrM.setImage();
        }else if(datagram == "videostop") {
            scrM.stopVideo();
        }else if(datagram.left(5) == "video") {
            scrM.playVideo(QApplication::applicationDirPath() + "/video/" + QString(datagram).split("#").at(1),
                    QString(datagram).split("#").at(2).toInt() * 1000);
        }

    } else {
        //endPlayer();
        if(datagram == "black") {
            ui->stackedWidget->setCurrentWidget(ui->black);
            player->stop();
        }else if(datagram == "color") {
            ui->stackedWidget->setCurrentWidget(ui->color);
            player->stop();
        }else if(datagram == "restore") {
            ui->stackedWidget->setCurrentWidget(lastScreen);
            if(lastScreen == ui->video) player->play();
        }else if(datagram == "song") {
            if(ui->stackedWidget->currentWidget() != ui->black) ui->stackedWidget->setCurrentWidget(ui->song);
            lastScreen = ui->song;
            player->stop();
        }else if(datagram == "image") {
            if(ui->stackedWidget->currentWidget() != ui->black) ui->stackedWidget->setCurrentWidget(ui->image);
            lastScreen = ui->image;
            player->stop();
        }else if(datagram == "videostop") {
            player->stop();
        }else if(datagram.left(5) == "video") {
            ui->stackedWidget->setCurrentWidget(ui->video);
            lastScreen = ui->video;
            player->setMedia(QUrl::fromLocalFile(QApplication::applicationDirPath() + "/video/" + QString(datagram).split("#").at(1)));
            player->setPosition(QString(datagram).split("#").at(2).toInt() * 1000);
            player->play();
            //player = new QProcess();
            //player->start("smplayer -fullscreen rtp://" + serverAddress + ":" + QString::number(vidStreamPortNumber));
        }
    }
}

void MainWindow::sendPing()
{
    tcpControlSocket->write("ping");
    tcpControlSocket->flush();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    delete this;
}

/*void MainWindow::endPlayer()
{
    if(player == nullptr) return;
    qDebug() << "ending player";

    player->terminate();
    //player->kill();
    player->waitForFinished();
    delete player;
    player = nullptr;

    QProcess killer;
    killer.execute("killall mpv");
}*/

void MainWindow::on_closeNotesButton_clicked()
{
    notes = nullptr;
    ui->pdf->setPixmap(QPixmap(0,0));
    updateUI();
}

