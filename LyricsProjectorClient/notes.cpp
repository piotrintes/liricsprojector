#include "notes.h"

#include <QApplication>

const QString &Notes::filename() const
{
    return filename_;
}

const QPixmap &Notes::image() const
{
    return image_;
}

const QPixmap &Notes::imageSmall() const
{
    return imageSmall_;
}

QPushButton *Notes::button() const
{
    return button_;
}

Notes::Notes(QString filename, QPixmap image, QPixmap imageSmall)
{
    filename_ = filename;
    image_ = image;
    imageSmall_ = imageSmall;

    button_ = new NotesListButton();
    button_->setText(filename);
    button_->setMinimumHeight(64);
    button_->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/pdf.png")));
}
