#include "noteslistbutton.h"

#include <QPainter>

NotesListButton::NotesListButton(QWidget *parent): QPushButton(parent)
{}

void NotesListButton::paintEvent(QPaintEvent *event)
{
    // setup painter
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(QPen(QColor(0,0,0,0),0));

    //QColor c = palette().window().color();
    QColor c = palette().light().color();
    if(isClicked){
        if(c.value()>128)
            c.setHsv(c.hue(),c.saturation(),c.value() - 30);
        else
            c.setHsv(c.hue(),c.saturation(),c.value() + 30);
    }

    painter.setBrush(QBrush(c));
    //painter.drawEllipse(0,0,width(),height());
    painter.drawRoundedRect(128,0,width()-140,height(),5,5);
    painter.drawPixmap(128+16,16,height()-32,height()-32,icon().pixmap(width()-32,height()-32));
    QFont f = painter.font();
    //f.setPixelSize(16);
    f.setBold(!visited);
    painter.setFont(f);
    painter.setPen(QPen(palette().text().color(),1));
    painter.drawText(128,0,width()-140,height(), Qt::AlignmentFlag::AlignCenter, text());
}

void NotesListButton::mousePressEvent(QMouseEvent * event)
{
    QPushButton::mousePressEvent(event);     // this will do base class content
    isClicked=true;
    update();
}

void NotesListButton::mouseReleaseEvent(QMouseEvent * event)
{
    QPushButton::mouseReleaseEvent(event);
    isClicked=false;
    visited = true;
    update();
}
