#include "screenmanager.h"

#include <QApplication>
#include <QDateTime>
#include <QGraphicsDropShadowEffect>
#include <QScreen>
#include <QThread>

ScreenManager::ScreenManager()
{
    graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicsView->setFrameStyle(0);

    player->setVideoOutput(videoItem);
    videoItem->setSize(QSize(width,height));

    connect(player, &QMediaPlayer::positionChanged, [this]() {
        if (player->mediaStatus() == QMediaPlayer::MediaStatus::BufferedMedia && player->duration() > 0
                && player->position() >= player->duration() -1000) videoEnd();
    });


    blackRect->setRect(0,0,width,height);
    blackRect->setBrush(QBrush(QColor(0,0,0,0)));
    blackRect->setPen(QPen(QColor(0,0,0,0),0));

    imageBcg->setRect(0,0,width,height);
    imageBcg->setBrush(QBrush(QColor(0,0,0,255)));
    imageBcg->setPen(QPen(QColor(0,0,0,0),0));
    imageBcg->setVisible(false);
    image->setVisible(false);

    scene->addItem(videoItem);
    scene->addItem(songText);
    scene->addItem(imageBcg);
    scene->addItem(image);
    scene->addItem(blackRect);

    player->setMedia(QUrl::fromLocalFile(backgroundVideo));
    player->play();
}

QGraphicsView *ScreenManager::getGraphicsView() const
{
    return graphicsView;
}

void ScreenManager::renderSongText()
{
    songText->setVisible(true);

    QFontMetrics fm(songTextFont);

    QPixmap px(fm.width(text),fm.height()*4);
    px.fill(QColor(0,0,0,0));
    QPainter painter(&px);

    painter.setPen(songTextColor);
    painter.setFont(songTextFont);
    painter.drawText(0, 0, fm.width(text), fm.height()*4, Qt::AlignCenter, text);
    painter.end();

    if(true) {
        // setup shadow effect
        QGraphicsDropShadowEffect* sh = new QGraphicsDropShadowEffect();
        sh->setColor(QColor(0,0,0,100));
        sh->setOffset(0,2);
        sh->setBlurRadius(30);

        // apply shadow
        QGraphicsPixmapItem *pxi = new QGraphicsPixmapItem(px);
        pxi->setGraphicsEffect(sh);
        pxi->update();

        QGraphicsScene sc(0,0,px.width(),px.height());
        sc.addItem(pxi);
        sc.addPixmap(px);

        px.fill(QColor(0,0,0,0));
        QPainter p(&px);
        sc.render(&p);
    }

    songText->setPixmap(px);

    QRectF bR = songText->sceneBoundingRect();
    songText->setPos(width/2 - bR.width()/2, height/2 - bR.height()/2);
}

void ScreenManager::fadeToBlack()
{
    if(blackRect->brush().color().alpha() == 255) return;
    QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

    QTime timeLeft = QTime::currentTime();
    while(timeLeft.msecsTo(fadeEndTime)>0) {
        timeLeft = QTime::currentTime();
        int val = 255 - (255*1000/transition)*timeLeft.msecsTo(fadeEndTime)/1000;
        if (val > 255) val = 255;
        blackRect->setBrush(QBrush(QColor(0,0,0,val)));
        QCoreApplication::processEvents();
    }
    blackRect->setBrush(QBrush(QColor(0,0,0,255)));
}

void ScreenManager::fadeFromBlack()
{
    if(blackRect->brush().color().alpha() == 0) return;

    QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

    QTime timeLeft = QTime::currentTime();
    while(timeLeft.msecsTo(fadeEndTime)>0) {
        timeLeft = QTime::currentTime();
        int val = (255*1000/transition)*timeLeft.msecsTo(fadeEndTime)/1000;
        if (val < 0) val = 0;
        blackRect->setBrush(QBrush(QColor(0,0,0,val)));
        QCoreApplication::processEvents();
    }
}

void ScreenManager::setupSongFont(QColor color, QFont font, int size)
{
    font.setPixelSize(height * size / 1000);
    color.setAlpha(0);
    //songText->setBrush(QBrush(color));
    //songText->setFont(font);
    songTextColor = color;
    songTextFont = font;
}

void ScreenManager::black()
{
    fadeToBlack();

    songTextColor.setAlpha(0);
    songText->setVisible(false);
    blackRect->setBrush(QBrush(QColor(0,0,0,255)));
    QCoreApplication::processEvents();
}

void ScreenManager::restore()
{
    isEmpty = false;

    if(lastScene == "song") setSong();

    fadeFromBlack();
}

void ScreenManager::empty()
{
    isEmpty = true;
    if(songTextColor.alpha() == 255) {
        QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

        QTime timeLeft = QTime::currentTime();
        while(timeLeft.msecsTo(fadeEndTime)>0) {
            timeLeft = QTime::currentTime();
            int val = (255*1000/transition)*timeLeft.msecsTo(fadeEndTime)/1000;
            if (val < 0) val = 0;
            songTextColor.setAlpha(val);

            renderSongText();
            QCoreApplication::processEvents();
        }
    }

    fadeFromBlack();
}

void ScreenManager::setSong()
{
    if(isEmpty) return;

    if(player->media().canonicalUrl()!=QUrl::fromLocalFile(backgroundVideo)) player->setMedia(QUrl::fromLocalFile(backgroundVideo));
    player->play();

    if(lastScene == "image" || lastScene == "video") {
        fadeToBlack();
        if(lastScene == "video") {
            player->setMedia(QUrl::fromLocalFile(backgroundVideo));
            player->play();
            QThread::msleep(300);
        }
        songTextColor.setAlpha(255);
        imageBcg->setVisible(false);
        image->setVisible(false);
        renderSongText();
        qDebug() << "fade start";
        fadeFromBlack();
        qDebug() << "fade end";
    } else {
        if(songTextColor.alpha() == 255) return;
        QTime fadeEndTime = QTime::currentTime().addMSecs(transition);

        QTime timeLeft = QTime::currentTime();
        while(timeLeft.msecsTo(fadeEndTime)>0) {
            timeLeft = QTime::currentTime();
            int val = 255 - (255*1000/transition)*timeLeft.msecsTo(fadeEndTime)/1000;
            if (val > 255) val = 255;
            songTextColor.setAlpha(val);

            renderSongText();
            QCoreApplication::processEvents();
        }
        songTextColor.setAlpha(255);
    }
    lastScene = "song";
}

void ScreenManager::setSongText(QString text)
{
    this->text = text;
    if (lastScene == "song" or lastScene == "video") renderSongText();
}

void ScreenManager::setImage()
{
    if(lastScene == "song" || lastScene == "video" || isEmpty ) fadeToBlack();

    if(lastScene == "video") {
        player->setMedia(QUrl::fromLocalFile(backgroundVideo));
        player->play();
    }

    imageBcg->setVisible(true);
    image->setVisible(true);
    lastScene = "image";
    fadeFromBlack();
}

bool ScreenManager::compareColors(QColor color1, QColor color2) {
    if (int(color1.red()*10) != int(color2.red()*10)) return false;
    if (int(color1.green()*10) != int(color2.green()*10)) return false;
    if (int(color1.blue()*10) != int(color2.blue()*10)) return false;
    return true;
}

void ScreenManager::videoEnd()
{
    if(player->media().canonicalUrl().toString().remove("file://") == backgroundVideo) {
        player->setPosition(0);
    } else {
        if (player->position() >= player->duration() -1){
            player->pause();
            QApplication::processEvents();
            fadeToBlack();
        }
    }
}

void ScreenManager::setImagePixmap(QPixmap px)
{
    image->setPixmap(px.scaled(width,height,Qt::KeepAspectRatio,Qt::SmoothTransformation));

    if(compareColors(px.toImage().pixelColor(0,0), px.toImage().pixelColor(0,px.height()-2)) &&
            compareColors(px.toImage().pixelColor(px.width()-2,0), px.toImage().pixelColor(px.width()-2,px.height()-2)) &&
            compareColors(px.toImage().pixelColor(0,0), px.toImage().pixelColor(px.width()-2,px.height()-2))
            ) {
        imageBcg->setBrush(QBrush(px.toImage().pixelColor(0,0)));
    } else {
        imageBcg->setBrush(QBrush(QColor(0,0,0,255)));
    }

    QRectF bR = image->sceneBoundingRect();
    image->setPos(width/2 - bR.width()/2, height/2 - bR.height()/2);
}

void ScreenManager::playVideo(QString filename, int position)
{
    fadeToBlack();
    imageBcg->setVisible(false);
    image->setVisible(false);
    songText->setVisible(false);

    lastScene = "video";

    player->setMedia(QUrl::fromLocalFile(filename));
    if(position > 0) player->setPosition(position);
    player->play();
    QThread::msleep(300);

    fadeFromBlack();
}

void ScreenManager::stopVideo()
{
    player->pause();
    fadeToBlack();
    player->stop();
}

