#ifndef NOTESLISTBUTTON_H
#define NOTESLISTBUTTON_H

#include <QPushButton>

class NotesListButton : public QPushButton
{
private:
    bool isClicked = false;
    bool visited = false;

public:
    NotesListButton(QWidget* parent = Q_NULLPTR);

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent * event);          // Overloaded mouse press event (set isClicked = true)
    virtual void mouseReleaseEvent(QMouseEvent * event);        // Overloaded mouse relese event (set isClicked = false)
};

#endif // NOTESLISTBUTTON_H
