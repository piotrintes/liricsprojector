#ifndef WIDGETAREA_H
#define WIDGETAREA_H

#include <QWidget>

class WidgetArea : public QWidget
{
    Q_OBJECT
private:
    bool isClicked = false;
    QColor backgroundColor_ = QColor(255,255,255);

    unsigned short actTab = 0;
    unsigned short tabCount = 3;
    int transition = 0;

    unsigned stoperHour=0;
    unsigned stoperMinutes=0;
    unsigned stoperSeconds=0;
    QString stoperOutput;
    bool stoperActive = false;

    unsigned batteryLevel = 0;
    bool batteryCharging = true;
    QString batteryTime;
    QString batteryDevice;

    void updateStoper();

    void updateBattery();
    void initBatery();

    void paintClock(QPainter *painter, int offset = 0);
    void paintStoper(QPainter *painter, int offset = 0);
    void paintBattery(QPainter *painter, int offset = 0);
public:
    explicit WidgetArea(QWidget *parent = nullptr);

    const QColor &backgroundColor() const;
    void setBackgroundColor(const QColor &newBackgroundColor);

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent * event);          // Overloaded mouse press event (set isClicked = true)
    virtual void mouseReleaseEvent(QMouseEvent * event);        // Overloaded mouse relese event (set isClicked = false)
};

#endif // WIDGETAREA_H
