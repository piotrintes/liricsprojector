#ifndef CONNECTIONCONTROL_H
#define CONNECTIONCONTROL_H

#include <QTcpSocket>
#include <QThread>

class ConnectionControl : public QThread
{
    Q_OBJECT
private:
    QTcpSocket *socket;
    bool pingReturned;
public:
    ConnectionControl(QTcpSocket *socket);
    void run() override;

    void setPingReturned(bool value);

signals:
    void sendPing();
    void disconnect();
    void disconnected();
};

#endif // CONNECTIONCONTROL_H
