#include "circlebutton.h"

#include <QGraphicsDropShadowEffect>
#include <QPainter>

const QColor &CircleButton::buttonColor() const
{
    return buttonColor_;
}

void CircleButton::setButtonColor(const QColor &newButtonColor)
{
    buttonColor_ = newButtonColor;
}

CircleButton::CircleButton(QWidget *parent): QPushButton(parent)
{
    // setup shadow effect
    QGraphicsDropShadowEffect* sh = new QGraphicsDropShadowEffect();
    sh->setColor(QColor(0,0,0,150));
    sh->setOffset(0,1);
    sh->setBlurRadius(20);

    this->setGraphicsEffect(sh);
}

void CircleButton::paintEvent(QPaintEvent *event)
{
    // setup painter
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(QPen(QColor(0,0,0,0),0));

    QColor c = buttonColor_;
    if(isClicked){
        if(c.value()>128)
            c.setHsv(c.hue(),c.saturation(),c.value() - 30);
        else
            c.setHsv(c.hue(),c.saturation(),c.value() + 30);
    }

    painter.setBrush(QBrush(c));
    painter.drawEllipse(0,0,width(),height());
    painter.drawPixmap(10,10,width()-20,height()-20,icon().pixmap(width()-20,height()-20));
}

void CircleButton::mousePressEvent(QMouseEvent * event)
{
    QPushButton::mousePressEvent(event);     // this will do base class content
    isClicked=true;
    update();
}

void CircleButton::mouseReleaseEvent(QMouseEvent * event)
{
    QPushButton::mouseReleaseEvent(event);
    isClicked=false;
    update();
}
