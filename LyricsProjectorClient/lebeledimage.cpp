#include "lebeledimage.h"

#include <QPainter>

const QString &LebeledImage::label() const
{
    return label_;
}

void LebeledImage::setLabel(const QString &newLabel)
{
    label_ = newLabel;
}

const QColor &LebeledImage::backgroundColor() const
{
    return backgroundColor_;
}

void LebeledImage::setBackgroundColor(const QColor &newBackgroundColor)
{
    backgroundColor_ = newBackgroundColor;
}

void LebeledImage::setDisplayLabel(bool newDisplayLabel)
{
    displayLabel = newDisplayLabel;
}

LebeledImage::LebeledImage(QWidget *parent): QLabel(parent)
{}

void LebeledImage::paintEvent(QPaintEvent *event)
{
    QLabel::paintEvent(event);

    if(!displayLabel || label_ == "") return;

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(QPen(QColor(0,0,0,0),0));
    painter.setBrush(QBrush(backgroundColor_));

    painter.drawEllipse(width()-32-5,5,32,32);
    painter.drawEllipse(width()-64-64-5,5,32,32);
    painter.drawRect(width()-64-48-5,5,64+32,32);

    if(backgroundColor_.value()<128)
        painter.setPen(QPen(QColor(255,255,255),1));
    else
        painter.setPen(QPen(QColor(0,0,0),1));

    QFont f = painter.font();
    f.setPixelSize(16);
    f.setBold(true);
    painter.setFont(f);
    painter.drawText(width()-64-48-5,5,64+32,32, Qt::AlignmentFlag::AlignCenter, label_);

}
