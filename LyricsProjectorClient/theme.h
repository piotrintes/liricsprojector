/****************************************************************************
 **
 ** Copyright 2021 Piotr Grosiak     Polska
 **
 ** This file is part of Lyrics Projector.
 **
 **  Lyrics Projector is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Lyrics Projector is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef THEME_H
#define THEME_H

/****************************************************************************
 ** This class represents theme - colors and font for output window
****************************************************************************/

#include <QColor>
#include <QFile>
#include <QFont>

class Theme: public QObject
{
    Q_OBJECT
private:
    QString name_;                                          // theme name (for when you can have multiple themes, if that frature is ever needed)

    QFont Font_;                                            // font
    int textSize_;                                          // text size (‰ of screen height)
    QColor textColor_;                                      // text color
    QColor bcgColor_;                                       // background color

    int bibleTextSize_;                                     // text size for bible (‰ of screen height)
    QColor bibleTextColor_;                                 // text color for bible
    QColor bibleBcgColor_;                                  // background color for bible
    QList<int> bibleMargins_ = {10,10,10,10};               // margins for bible (left,right,top,bottom)

public:
    Theme();                                                // constructor for empty theme
    Theme(Theme *theme);                                    // constructor to build new theme from another one
    void setTheme(Theme *theme);                            // owerwrite this theme with another one

    void loadTheme(QFile *file);                            // load theme from file
    void saveTheme(QFile *file);                            // save theme to file

    QFont Font() const;                                     // return font_
    void setFont(const QFont &Font);                        // set font_
    int textSize() const;                                   // return textSize_
    void setTextSize(int textSize);                         // set textSize_
    QColor textColor() const;                               // return textColor_
    void setTextColor(const QColor &textColor);             // set textColor_
    QColor bcgColor() const;                                // return bcgColor_
    void setBcgColor(const QColor &bcgColor);               // set bcgColor_
    QString name() const;                                   // return name_
    void setName(const QString &name);                      // set name_
    QList<int> bibleMargins() const;
    void setBibleMargins(const QList<int> &bibleMargins);
    QColor bibleBcgColor() const;
    void setBibleBcgColor(const QColor &bibleBcgColor);
    QColor bibleTextColor() const;
    void setBibleTextColor(const QColor &bibleTextColor);
    int bibleTextSize() const;
    void setBibleTextSize(int bibleTextSize);

signals:
    void updated();                                         // emited when theme is modified
};

#endif // THEME_H
