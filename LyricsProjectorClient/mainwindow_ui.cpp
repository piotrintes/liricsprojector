#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QScreen>
#include <QNetworkInterface>
#include <QNetworkDatagram>
#include <QDebug>
#include <QDir>
#include <QPixmap>
#include <QThread>
#include <QGraphicsVideoItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QScrollBar>


void MainWindow::updateTheme()
{
    // update color palette
    QPalette pal;
    pal.setColor(QPalette::Window,theme->bcgColor());
    pal.setColor(QPalette::WindowText,theme->textColor());

    ui->stackedWidget->setPalette(pal);

    if(useGraphicalOutput) {
        scrM.setupSongFont(theme->textColor(),theme->Font(),theme->textSize());

    } else {

        // update font
        QFont f(theme->Font());
        f.setPixelSize(ui->stackedWidget->width() * theme->textSize() / 1000);
        ui->SlideText->setFont(f);

        // update bible color palette
        pal.setColor(QPalette::Window,theme->bibleBcgColor());
        pal.setColor(QPalette::WindowText,theme->bibleTextColor());

        /*
        // update bible font
        ui->bible->setPalette(pal);
        f.setPixelSize(height() * theme_->bibleTextSize() / 1000);
        ui->BibleSlideText->setFont(f);

        // update bible margins
        ui->bible->setContentsMargins(width() * theme_->bibleMargins().at(0) / 100,
                                      height() * theme_->bibleMargins().at(2) / 100,
                                      width() * theme_->bibleMargins().at(1) / 100,
                                      height() * theme_->bibleMargins().at(3) / 100);
                                      */
    }

}

void MainWindow::setupUI()
{
    setWindowState(Qt::WindowFullScreen);
    setWindowFlag(Qt::WindowStaysOnTopHint,true);
#ifdef Q_OS_WIN
    setWindowFlag(Qt::FramelessWindowHint,true);
#endif

    if(showSlideControls) {
        ui->nextButton->setButtonColor(QColor(0,200,83));
        ui->nextButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/next.png")));
        ui->nextButton->show();

        ui->previousButton->setButtonColor(QColor(80,80,80));
        ui->previousButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/prev.png")));
        ui->previousButton->show();

        ui->blackButton->setButtonColor(QColor(80,80,80));
        ui->blackButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/black.png")));
        ui->blackButton->show();

        ui->widgetArea->setBackgroundColor(QColor(80,80,80));
        ui->widgetArea->show();

        ui->showNotesButton->setButtonColor(QColor(80,80,80));
        ui->showNotesButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/notes.png")));
        ui->showNotesButton->show();

        ui->closeNotesButton->setButtonColor(QColor(200,80,80));
        ui->closeNotesButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/close.png")));
        ui->closeNotesButton->hide();

        ui->changeNotes2PreviewButton->setButtonColor(QColor(80,80,80));
        ui->changeNotes2PreviewButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/resize.png")));
        ui->changeNotes2PreviewButton->hide();

        ui->loadPdfButton->setButtonColor(QColor(80,80,80));
        ui->loadPdfButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/open.png")));
        ui->loadPdfButton->hide();
    } else {
        ui->nextButton->hide();
        ui->previousButton->hide();
        ui->blackButton->hide();
        ui->showNotesButton->hide();
        ui->closeNotesButton->hide();
        ui->changeNotes2PreviewButton->hide();
        ui->loadPdfButton->hide();
        ui->widgetArea->hide();
        ui->NextPreview->hide();
    }
    ui->pdfProgressBar->hide();

    ui->NextPreview->setVisible(showNextSlide);

    if(showNextSlide) {
        ui->NextPreview->setBackgroundColor(QColor(80,80,80));
        ui->SlideImage->setBackgroundColor(QColor(80,80,80));
        ui->SlideText->setBackgroundColor(QColor(80,80,80));

        ui->NextPreview->setLabel("Następny");
        ui->SlideImage->setLabel("Teraz");
        ui->SlideText->setLabel("Teraz");
    }

    ui->IpAddress->setText("");

    QList<QHostAddress> list = QNetworkInterface::allAddresses();

    for(int nIter=0; nIter<list.count(); nIter++)
        if(!list[nIter].isLoopback() && (list[nIter].protocol() == QAbstractSocket::IPv4Protocol)) {
            if (ui->IpAddress->text() != "") ui->IpAddress->setText(ui->IpAddress->text().append("\n"));
            ui->IpAddress->setText(ui->IpAddress->text().append(list[nIter].toString()));
        }

    ui->label_version->setText("Client v" + QString(APP_VERSION) + " for Lyrics Projector v2.2.1");
}

void MainWindow::updateUI()
{
    // setup ui
    ui->stackedWidget->setGeometry(0,0,width(),height());
    ui->nextButton->setGeometry(width()-60-80,height()-40-80,80,80);
    ui->previousButton->setGeometry(width()-60-60-80-30,height()-50-60,60,60);
    ui->blackButton->setGeometry(width()-30-60,30,60,60);
    ui->showNotesButton->setGeometry(30,30,60,60);
    ui->closeNotesButton->setGeometry(30,110,60,60);
    //ui->closeNotesButton->setGeometry(110,30,60,60);

    ui->widgetArea->setGeometry(width()/2-130,30,260,60);
    ui->widgetArea->setGeometry(width()-260-60-80,30,260,60);
    //ui->widgetArea->setGeometry(width()/2-130,height()-50-60,260,60);

    ui->notesScrollArea->setGeometry(0,0,width()*notes2preview,height());
    ui->stackedWidget->setGeometry(width()*notes2preview,height()*(notes2preview/3),width()*(1-notes2preview),height()*(1-notes2preview));
    ui->changeNotes2PreviewButton->setGeometry(width()*notes2preview-30,height()/2-30,60,60);

    if(notes2preview != 0 && notes == nullptr) {
        ui->loadPdfButton->show();
        ui->loadPdfButton->setGeometry((width()*notes2preview)-90,height()-90,60,60);
        foreach (Notes *n, notesList) {
            n->button()->show();
        }
        ui->closeNotesButton->hide();
    } else {
        ui->loadPdfButton->hide();
        foreach (Notes *n, notesList) {
            n->button()->hide();
        }
        ui->closeNotesButton->show();
    }

    if (notes2preview == 0) ui->closeNotesButton->hide();


    if(notes2preview != 0 && notes != nullptr) {
        int scrollPos = ui->notesScrollArea->verticalScrollBar()->value();

        int startH = 1;
        if (ui->pdf->pixmap() != nullptr) startH = ui->pdf->pixmap()->height();

        if(notes2preview == 0.4) ui->pdf->setPixmap(notes->imageSmall()); else ui->pdf->setPixmap(notes->image());

        double asp = double(ui->pdf->pixmap()->height())/startH;

        ui->notesScrollArea->verticalScrollBar()->setMaximum(scrollPos*asp);    // we need to ensure the value we set will be accepted
        ui->notesScrollArea->verticalScrollBar()->setValue(scrollPos*asp);
    }

    if(showNextSlide){
        ui->NextPreview->setGeometry(width()*notes2preview,height()*3/4,width()/4,height()/4);
        if(notes2preview == 0.75) ui->NextPreview->setGeometry(width()*notes2preview,height()*3/5,width()*(1-notes2preview),height()*(1-notes2preview));

        ui->SlideImage->setDisplayLabel(notes2preview > 0);
        ui->SlideText->setDisplayLabel(notes2preview > 0);
    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    updateUI();
    updateTheme();
}

void MainWindow::on_changeNotes2PreviewButton_clicked()
{
    if(notes2preview == 0.4) notes2preview = 0.75; else notes2preview = 0.4;
    updateUI();
    updateTheme();
    if(ui->SlideImage->pixmap() != nullptr)
        ui->SlideImage->setPixmap(lastImage.scaled(ui->SlideImage->width(),ui->SlideImage->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
}


void MainWindow::on_showNotesButton_clicked()
{
    if(notes2preview == 0) {
        notes2preview = 0.4;
        ui->changeNotes2PreviewButton->show();
        //ui->showNotesButton->setButtonColor(QColor(200,80,80));
        ui->showNotesButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/closeNotes.png")));
    } else {
        notes2preview = 0;
        ui->changeNotes2PreviewButton->hide();
        //ui->showNotesButton->setButtonColor(QColor(80,80,80));
        ui->showNotesButton->setIcon(QIcon(QPixmap(QApplication::applicationDirPath() + "/icons/notes.png")));
    }
    updateUI();
    updateTheme();
    if(ui->SlideImage->pixmap() != nullptr)
        ui->SlideImage->setPixmap(lastImage.scaled(ui->SlideImage->width(),ui->SlideImage->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

void MainWindow::on_nextButton_clicked()
{
    sentUDPData("1",slideRemotePort);
}


void MainWindow::on_previousButton_clicked()
{
    sentUDPData("-1",slideRemotePort);
}

void MainWindow::on_blackButton_clicked()
{
    sentUDPData("0",slideRemotePort);
}
