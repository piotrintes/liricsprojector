#include "connectioncontrol.h"

#include <QHostAddress>

void ConnectionControl::setPingReturned(bool value)
{
    pingReturned = value;
}

ConnectionControl::ConnectionControl(QTcpSocket *socket)
{
    this->socket = socket;
}

void ConnectionControl::run()
{
    while(1) {
        qDebug() << "ping attempt";
        if(socket->state() == QAbstractSocket::ConnectedState) {
            qDebug() << "ping";
            if(!pingReturned){
                qDebug() << "disconnecting";
                emit disconnect();
            } else {
                emit sendPing();
                pingReturned = false;
            }
        }
        sleep(5);
    }
}
