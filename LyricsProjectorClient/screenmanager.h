#ifndef SCREENMANAGER_H
#define SCREENMANAGER_H

#include <QApplication>
#include <QScreen>
#include <QGraphicsScene>
#include <QGraphicsVideoItem>
#include <QGraphicsView>
#include <QMediaPlayer>
#include <QTime>

class ScreenManager : public QObject
{
    Q_OBJECT
private:
    QGraphicsScene *scene = new QGraphicsScene();
    QGraphicsView *graphicsView = new QGraphicsView(scene);
    QMediaPlayer *player = new QMediaPlayer;

    int width = QApplication::primaryScreen()->size().width();
    int height = QApplication::primaryScreen()->size().height();
    int transition = 500;

    QColor songTextColor;
    QFont songTextFont;
    QString text = "tekst";
    QString lastScene = "";
    bool isEmpty = false;
    QString backgroundVideo = QApplication::applicationDirPath() + "/bcg/yellow_dots.mp4";

    QGraphicsVideoItem *videoItem = new QGraphicsVideoItem();
    QGraphicsRectItem *blackRect = new QGraphicsRectItem();
    QGraphicsPixmapItem *songText = new QGraphicsPixmapItem();
    QGraphicsRectItem *imageBcg = new QGraphicsRectItem();
    QGraphicsPixmapItem *image = new QGraphicsPixmapItem();

    void renderSongText();
    void fadeToBlack();
    void fadeFromBlack();
    bool compareColors(QColor color1, QColor color2);
    void videoEnd();
public:
    ScreenManager();
    QGraphicsView *getGraphicsView() const;

    void setupSongFont(QColor color, QFont font, int size);

    void black();
    void restore();
    void empty();
    void setSong();
    void setSongText(QString text);
    void setImage();
    void setImagePixmap(QPixmap px);
    void playVideo(QString filename, int position = 0);
    void stopVideo();
};

#endif // SCREENMANAGER_H
