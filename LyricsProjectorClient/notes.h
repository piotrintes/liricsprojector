#ifndef NOTES_H
#define NOTES_H

#include <QPixmap>
#include "noteslistbutton.h"

class Notes
{
    QString filename_;
    QPixmap image_;
    QPixmap imageSmall_;
    NotesListButton *button_;
public:
    Notes(QString filename, QPixmap image, QPixmap imageSmall);
    const QString &filename() const;
    const QPixmap &image() const;
    const QPixmap &imageSmall() const;
    QPushButton *button() const;
};

#endif // NOTES_H
